﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using cje.georgia.efiling;

namespace ExampleEFilingService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace = "https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/WSDL/FilingReviewMDE", ConfigurationName = "FilingReviewMDE")]
    public interface FilingReviewMDE
    {

        // CODEGEN: Generating message contract since the operation ReviewFiling is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action = "https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/WSDL/FilingReviewMDE/Rev" +
            "iewFiling", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(AugmentationType))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MetadataType))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(AssociationType))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(ServeProcessRequestType))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(ServeFilingResponseType))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(ServeFilingRequestType))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(NotifyFilingReviewCompleteResponseType))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(NotifyFilingReviewCompleteRequestType))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(ObjectType))]
        ReviewFilingResponse ReviewFiling(ReviewFilingRequest request);
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface FilingReviewMDEChannel : FilingReviewMDE, System.ServiceModel.IClientChannel
    {
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
    public partial class ReviewFilingRequest
    {

        [System.ServiceModel.MessageHeaderAttribute(Namespace = "http://cje.georgia.gov/extension/header/1.0")]
        public CJEExchangeHeaderType CJEExchangeHeader;

        [System.ServiceModel.MessageBodyMemberAttribute(Name = "ReviewFilingRequest", Namespace = "https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/MessageWrappers", Order = 0)]
        public ReviewFilingRequestType ReviewFilingRequest1;

        public ReviewFilingRequest()
        {
        }

        public ReviewFilingRequest(CJEExchangeHeaderType CJEExchangeHeader,ReviewFilingRequestType ReviewFilingRequest1)
        {
            this.CJEExchangeHeader = CJEExchangeHeader;
            this.ReviewFilingRequest1 = ReviewFilingRequest1;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
    public partial class ReviewFilingResponse
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Name = "ReviewFilingResponse", Namespace = "https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/MessageWrappers", Order = 0)]
        public ReviewFilingResponseType ReviewFilingResponse1;

        public ReviewFilingResponse()
        {
        }

        public ReviewFilingResponse(ReviewFilingResponseType ReviewFilingResponse1)
        {
            this.ReviewFilingResponse1 = ReviewFilingResponse1;
        }
    }
}
