﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using cje.georgia.efiling;

namespace ExampleEFilingService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : FilingReviewMDE
    {
        public ReviewFilingResponse ReviewFiling(ReviewFilingRequest request)
        {
            CJEExchangeHeaderType hdr = request.CJEExchangeHeader;
            ReviewFilingRequestType body = request.ReviewFilingRequest1;
            
            ReviewFilingResponse resp = new ReviewFilingResponse(ReviewFilingResponseType.createSuccessResponse(hdr.MessageID.Value));
            return resp;
        }
    }
}
