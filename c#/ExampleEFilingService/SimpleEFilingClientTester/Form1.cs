﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using cje.georgia.efiling;
using SimpleEFilingClientTester.EFilingServiceClient2;

namespace SimpleEFilingClientTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                this.xmlTextBox.Text = openFileDialog1.FileName;
            }
        }

        private void runButton_Click(object sender, EventArgs e)
        {
            try
            {
                // 1. Open the XML message template and unmarshall it
                ReviewFilingRequestType req = ReviewFilingRequestType.GetInstanceFromFile(openFileDialog1.FileName);

                FilingMessageType fmsg = req.FilingMessage;

                // 2. Open the attachment (inefficiently for now)
                byte[] attachmentByteArray = File.ReadAllBytes(openFileDialog2.FileName);

                // 3. Build the attachment and add it to the ECF DocumentRendition element
                String format = "application/pdf";
                BinaryType attachment = new BinaryType.BinaryBuilder(idTextBox.Text, format)
                        .binaryDescriptionText("MTOM Attachment")
                        .binaryObject(attachmentByteArray)
                        .binarySize(attachmentByteArray.Length)
                        .build();

                DocumentRenditionType rendition = DocumentRenditionType.buildDocumentRendition(attachment);

                // 4. Add the rendition to the ECF augmentation
                DocumentAugmentationType ecfAug =
                        new DocumentAugmentationType.ECFDocumentationBuilder(rendition, "1", docTypeTextBox.Text)
                        .specialHandlingInstructions("Do this")
                        .build();

                // 5. Create the GA augmentation
                DocumentAugmentationType1 gaAug =
                        new DocumentAugmentationType1.GADocumentAugmentationBuilder(true).build();

                // 6. Build the lead document
                DocumentType leadDocument = new FilingLeadDocumentHelper(catTextBox.Text, "123456", ecfAug)
                    .gaDocumentationAugmentation(gaAug)
                    .build();

                fmsg.FilingLeadDocument = leadDocument;

                msgTextBox.Text = "Built Filing Request message";

                // 7. Build the header component to the SOAP invocation
                OrganizationType msgSender = new OrganizationType.OrganizationBuilder().category("PROS")
                        .county("100")
                        .build();
                OrganizationType msgReceiver = new OrganizationType.OrganizationBuilder().category("CT")
                        .county("100")
                        .build();
                CJEExchangeHeaderType header = CJEExchangeHeaderType.buildHeader(
                        "1",            // Transaction ID
                        "123456789",    // Message ID
                        DateTime.Now,
                        EntityType.buildEntityOrganization(msgSender),
                        EntityType.buildEntityOrganization(msgReceiver));

                msgTextBox.AppendText("Built Header and Sending message\n");

                // 8. Invoke the service
                // this.eFilingServiceClient.reviewFiling(reviewMsg, header);
                FilingReviewMDEClient client = new FilingReviewMDEClient();
                //GlobalProxySelection.Select = new WebProxy("127.0.0.1", 8888);
                ReviewFilingResponseType response = client.ReviewFiling(header, req);

                if (response != null
                    && response.MessageStatus != null
                    && response.MessageStatus.MessageStatusCode != null)
                {
                    if (response.MessageStatus.MessageStatusCode.Value == MessageStatusCodeSimpleType.Success)
                    {
                        msgTextBox.AppendText("Success response received.\n");
                    }
                    else
                    {
                        msgTextBox.AppendText("Error response received.\n");
                    }
                }
                else
                {
                    msgTextBox.AppendText("NULL response or Null Message Status in response received.\n");
                }                
            }
            catch (IOException ie)
            {
                msgTextBox.AppendText("IO Exception:");
                msgTextBox.AppendText(ie.ToString());
            }
            catch (Exception ex)
            {
                msgTextBox.AppendText("Inexpected Exception:");
                msgTextBox.AppendText(ex.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (this.openFileDialog2.ShowDialog() == DialogResult.OK)
            {
                this.attachmentTextBox.Text = openFileDialog2.FileName;
            }
        }
    }
}