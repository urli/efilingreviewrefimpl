package com.urlintegration.io;

import org.apache.commons.io.filefilter.NameFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileFilter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by JMierwa on 3/18/2018.
 */
public class MessageCache {

    private String directory = null;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    private static final Logger LOG = LoggerFactory.getLogger(MessageCache.class);

    public MessageCache (String baseDirectory) {
        this.directory = baseDirectory;
    }

    public String getDirectory(String messageID) {
        // SimpleDateFormat lDateFormat = new SimpleDateFormat("yyyyMMdd");
        // String lstrDate = lDateFormat.format(Calendar.getInstance().getTime());
        // String lstrYear = lstrDate.substring(0, 4); // get the year
        String lstrDirectory = buildPayloadDirectoryPath(this.directory, null, messageID);
        File dir = new File(lstrDirectory);
        dir.setReadable(true, false);

        // This is needed to change to the dir and list dir contents, on linux, by a user other than
        // the app server
        // user, where the user is in a qualified group.
        dir.setExecutable(true, false);

        if (!dir.exists() && !dir.mkdirs())
            LOG.error("Error creating data folder: " + dir.getAbsolutePath());

        return lstrDirectory;
    }

    public  List<File> findDirectory(String messageID, File root) {
//        File dir = new File(this.directory);
//        FileFilter fileFilter = new NameFileFilter(messageID);
//        File[] files = dir.listFiles(fileFilter);
//        if (files.length != 0) {
//            return files[0].getAbsolutePath();
//        } else {
//            return "";
//        }
        List<File> result = new ArrayList<File>();
        for (File file : root.listFiles()) {
            if (file.isDirectory()) {
                if (file.getName().equals(messageID)) {
                    result.add(file);
                }
                result.addAll(findDirectory(messageID, file));
            }
        }
        return result;
    }

    public static String buildPayloadDirectoryPath(String basePayloadLoggingPath, String dateStr, String messageID) {
        String absolutePayloadDirPath;
        SimpleDateFormat lDateFormat = new SimpleDateFormat("yyyyMMdd");
        String lstrDate = dateStr;
        if (StringUtils.isBlank(lstrDate)) {
            lstrDate = lDateFormat.format(Calendar.getInstance().getTime());
        }
        String lstrYear = lstrDate.substring(0, 4); // get the year
        absolutePayloadDirPath = basePayloadLoggingPath + File.separator + lstrYear + File.separator + lstrDate + File.separator + messageID;
        return absolutePayloadDirPath;
    }
}
