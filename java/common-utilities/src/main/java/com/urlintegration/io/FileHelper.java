/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.urlintegration.io;

import java.io.*;

/**
 *
 * @author JMierwa
 */
public class FileHelper {
    public static String getName(File inFile) {
        String[] split = inFile.getName().split("\\.");
        return split[0];
    }
    
    public static String getExtension(File inFile) {
        String[] split = inFile.getName().split("\\.");
        return split[split.length - 1];
    }

    public static void inputStreamToFile(InputStream is, String outFile) throws IOException {
        byte[] buffer = new byte[is.available()];
        is.read(buffer);

        File targetFile = new File(outFile);
        OutputStream outStream = new FileOutputStream(targetFile);
        outStream.write(buffer);
        outStream.flush();
        outStream.close();
    }
}
