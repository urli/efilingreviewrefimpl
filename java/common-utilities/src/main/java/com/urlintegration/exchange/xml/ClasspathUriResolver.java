/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.urlintegration.exchange.xml;

import java.io.InputStream;
import javax.xml.transform.Source;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamSource;
import org.apache.commons.lang3.StringUtils;

/**
 * A resolver class called by an XSLT processor to turn a URI used in document(), xsl:import, or xsl:include into a Source object.
 * @author JMierwa
 */
public class ClasspathUriResolver implements URIResolver {

    private String relativePath = "";

    /**
     * Base constructor
     * @param relativePath a relative path from which to physically locate a resource.
     */
    public ClasspathUriResolver(String relativePath) {
        if (relativePath != null) {
            this.relativePath = relativePath;
        }
    }

    /**
     * Called by the processor when it encounters an xsl:include, xsl:import, or document() function in an XSLT document and
     * returns it as {@link javax.xml.transform.Source}.
     * @param href An href attribute, which may be relative or absolute, that reference a resource like an image.
     * @param base The base URI against which the first argument will be made absolute if the absolute URI is required.
     * @return {@link javax.xml.transform.Source}
     */
    public Source resolve(String href, String base) {
        String fullPath;
        if (StringUtils.isNotBlank(this.relativePath)) {
            fullPath = this.relativePath + "/" + href;
        } else {
            fullPath = href;
        }
                
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(fullPath);
        if (is != null) {
            return new StreamSource(is);
        }
        return null;
    }
}
