/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.urlintegration.exchange.xml;


import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * XML Date creation utility.
 * @author JMierwa
 */
public final class XMLGregorianCalendarHelper {

    private static final Logger LOG = LoggerFactory.getLogger(XMLGregorianCalendarHelper.class.getName());

    /**
     * Utility constructor preventing anyone from being able to access it.
     */
    private XMLGregorianCalendarHelper() {
    
    }
    /**
     * Returns an XMLGregorianCalendar object based on the provided input.
     *
     * @param hour number to be used for the hour
     * @param minute number to be used for minutes
     * @param second number to be used for seconds
     * @param tzOffset number (in minutes) for time zone offset (+/-)
     * @return XMLGregorianCalendar object based on the provided input or NULL if
     * there was a problem during setup.
     */
    public static XMLGregorianCalendar getXMLGregCalTime(int hour, int minute, int second, int tzOffset) {

        DatatypeFactory dtf;
        try {
            dtf = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException ex) {
            LOG.error("Failed to create a DatatypeFactory", ex);
            return null;
        }
        XMLGregorianCalendar cal = dtf.newXMLGregorianCalendar();
        cal.setTime(hour, minute, second);
        cal.setTimezone(tzOffset);
        return cal;
    }

    /**
     * Returns an XMLGregorianCalendar object based on the provided input.
     *
     * @param hour number to be used for the hour
     * @param minute number to be used for minutes
     * @param second number to be used for seconds
     * @return XMLGregorianCalendar object based on the provided input or NULL if
     * there was a problem during setup.
     */
    public static XMLGregorianCalendar getXMLGregCalTime(int hour, int minute, int second) {
        DatatypeFactory dtf;
        try {
            dtf = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException ex) {
            LOG.error("Failed to create a DatatypeFactory", ex);
            return null;
        }
        XMLGregorianCalendar cal = dtf.newXMLGregorianCalendar();
        cal.setTime(hour, minute, second);
        return cal;
    }
    
    /**
     * Returns an XMLGregorianCalendar object based on the provided input.
     *
     * @param year number to be used for year
     * @param month number to be used for month
     * @param day number ot be used for day
     * @return XMLGregorianCalendar object based on the provided input or NULL if
     * there was a problem during setup.
     */
    public static XMLGregorianCalendar getXMLGregCalDate(int year, int month, int day) {
        DatatypeFactory dtf;
        try {
            dtf = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException ex) {
            LOG.error("Failed to create a DatatypeFactory", ex);
            return null;
        }
        XMLGregorianCalendar cal = dtf.newXMLGregorianCalendar();
        cal.setYear(year);
        cal.setMonth(month);
        cal.setDay(day);
        return cal;
    }
}
