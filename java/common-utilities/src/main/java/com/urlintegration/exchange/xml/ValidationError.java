/*
 * Copyright (c) 2016. University Corporation for Atmospheric Research (UCAR). All rights reserved.
 */
package com.urlintegration.exchange.xml;

/**
 * A validation error class which represents the error and the relevant location
 * within the validated file
 */
public class ValidationError {

    private final ValidationErrorEnum errorType;
    private final String error;
    private final String fileName;
    private final Integer lineNumber;
    private final Integer columnNumber;

    /**
     *
     * @param errorType The category of XML validation error.
     * @param error The error that caused a validation problem
     * @param fileName The file in which the error occurred
     * @param lineNumber The line number of the end of the text that caused the
     * error or warning
     * @param columnNumber The column number of the end of the text that caused
     * the error or warning
     */
    public ValidationError(ValidationErrorEnum errorType, String error, String fileName, Integer lineNumber, Integer columnNumber) {
        this.errorType = errorType;
        this.error = error;
        this.fileName = fileName;
        this.lineNumber = lineNumber;
        this.columnNumber = columnNumber;
    }

    public String getError() {
        return error;
    }

    public String getFileName() {
        return fileName;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public Integer getColumnNumber() {
        return columnNumber;
    }

    public ValidationErrorEnum getErrorType() {
        return this.errorType;
    }

    @Override
    public String toString() {
        return String.format("%s: %s line %d, col %d: %s", getErrorType().toString(), getFileName(), getLineNumber(), getColumnNumber(), getError());
    }
}
