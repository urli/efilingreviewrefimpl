/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.urlintegration.exchange.utilities;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Result that contains error information and result.
 *
 * @author JMierwa
 */
public class ProcessingResult {

    private static final String NEW_LINE = System.getProperty("line.separator");
    private ResultStatusEnum result;
    private String longResponseMsg = "";
    private String shortResponseMsg = "";
    private String applicationErrorCode = "";
    private Exception ex = null;

    /**
     * Basic constructor.
     *
     * @param resultValue - the
     * {@linkplain com.urlintegration.exchange.utilities.ResultStatusEnum}
     * to set for the response
     */
    public ProcessingResult(final ResultStatusEnum resultValue) {
        this.result = resultValue;
    }

    /**
     * Constructor with additional message information.
     *
     * @param resultValue - the
     * {@link com.urlintegration.exchange.utilities.ResultStatusEnum} to
     * set for the response
     * @param message - Any additional information to be included with the
     * notification response.
     * <p>
     * The most likely use is for errors that did not result in exceptions by
     * the notification mechanism used.
     */
    public ProcessingResult(final ResultStatusEnum resultValue, final String message) {
        this.result = resultValue;
        this.longResponseMsg = message;
    }

    /**
     * Constructor with additional message information.
     *
     * @param resultValue - the
     * {@link com.urlintegration.exchange.utilities.ResultStatusEnum} to
     * set for the response
     * @param message - Any additional information to be included with the
     * notification response.
     * @param shortMsg - A short version of the long message
     * <p>
     * The most likely use is for errors that did not result in exceptions by
     * the notification mechanism used.
     */
    public ProcessingResult(final ResultStatusEnum resultValue, final String message, final String shortMsg) {
    	this.result = resultValue;
    	this.longResponseMsg = message;
    	this.shortResponseMsg = shortMsg;
    }

    /**
     * Constructor with additional message information and exception
     * information.
     *
     * @param resultValue - the
     * {@link com.urlintegration.exchange.utilities.ResultStatusEnum}
     * to set for the response
     * @param message - Any additional information to be included with the
     * notification response.
     * <p>
     * The most likely use is for errors that did not result in exceptions by
     * the notification mechanism used.
     * @param ex - Any exception that occurred during the message send and build
     * process.
     */
    public ProcessingResult(final ResultStatusEnum resultValue, final String message, final Exception ex) {
        this.result = resultValue;
        this.longResponseMsg = message;
        this.ex = ex;
    }

    /**
     * Constructor with additional message information and exception
     * information.
     *
     * @param resultValue - the
     * {@link com.urlintegration.exchange.utilities.ResultStatusEnum}
     * to set for the response
     * @param message - Any additional information to be included with the
     * notification response.
     * @param shortMsg - A short version of the message.
     * <p>
     * The most likely use is for errors that did not result in exceptions by
     * the notification mechanism used.
     * @param ex - Any exception that occurred during the message send and build
     * process.
     */
    public ProcessingResult(final ResultStatusEnum resultValue, final String message, final String shortMsg,  final String applicationCode, final Exception ex) {
        this.result = resultValue;
        this.longResponseMsg = message;
        this.shortResponseMsg = shortMsg;
        this.applicationErrorCode = applicationCode;
        this.ex = ex;
    }

    /**
     * Checks the response contents to see it the notification was successful or
     * not.
     *
     * @return True when successful, otherwise returns false.
     */
    public final boolean successful() {
        return result == ResultStatusEnum.SUCCESS;
    }

    /**
     * Checks the response contents to see if the result is a failure.
     *
     * @return True when a failure, otherwise returns false.
     */
    public final boolean failed() {
        return result == ResultStatusEnum.FAIL;
    }

    /**
     * Gets the message associated with the response.
     *
     * @return the response message, if any, or an empty string if not.
     */
    public final String getlongResponseMsg() {
        if (StringUtils.isNotBlank(longResponseMsg)) {
            return longResponseMsg;
        } else if (ex != null) {
            return ex.getMessage();
        }
        return result.value();
    }

    /**
     * Gets the message associated with the response.
     *
     * @return the response message, if any, or an empty string if not.
     */
    public final String getShortResponseMsg() {
        if (StringUtils.isNotBlank(this.shortResponseMsg)) {
            return this.shortResponseMsg;
        } else {
            return "";
        }
    }

    /**
     * Gets the application error code associated with the response.
     *
     * @return the response message, if any, or an empty string if not.
     */
    public final String getApplicationErrorCode() {
        if (StringUtils.isNotBlank(this.applicationErrorCode)) {
            return this.applicationErrorCode;
        } else {
            return "";
        }
    }


    /**
     * Gets any exception that occurred during the notification.
     *
     * @return the exception that occurred when the
     * {@link com.urlintegration.exchange.utilities.ResultStatusEnum}
     * indicates an error. It will return null otherwise.
     */
    public final Exception getException() {
        return ex;
    }

    /**
     * Returns a formatted string representation of this class for display
     * purposes.
     *
     * @return the class as a formatted string.
     */
    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder(String.format("ProcessingResult: '%s'", this.result.toString()));
        sb.append(String.format(", ResultMessage: '%s'",
                (StringUtils.isNotBlank(this.longResponseMsg)) ? this.longResponseMsg : "EMPTY STRING"));
        sb.append(", ShortResponseMsg: '");
        sb.append((StringUtils.isNotBlank(this.shortResponseMsg)) ? this.shortResponseMsg : "EMPTY STRING");
        sb.append("', ApplicationErrorCode: '");
        sb.append((StringUtils.isNotBlank(this.applicationErrorCode)) ? this.applicationErrorCode : "EMPTY STRING");
        String exString;
        if (this.ex != null) {
            final StringBuilder sbex = new StringBuilder(NEW_LINE);
            sbex.append(this.ex.toString());
            sbex.append(NEW_LINE);

            //add fist 5 lines of the stack trace
            for (StackTraceElement element : this.ex.getStackTrace()) {
                sbex.append(element);
                sbex.append(NEW_LINE);
            }
            exString = sbex.toString();
        } else {
            exString = "NULL";
        }
        sb.append(String.format(", Exception: %s", exString));
        return sb.toString();
    }

    /**
     * Static class that initializes the response as a standard success
     * response.
     *
     * @return the initialized success response
     */
    public static ProcessingResult createSuccessResult() {
        return new ProcessingResult(ResultStatusEnum.SUCCESS);
    }

    /**
     * Static class that initializes the response as a standard success response
     * with an additional information message.
     *
     * @param responseMsg Additional text related to a success response.
     * @return the initialized success response with response message.
     */
    public static ProcessingResult createSuccessResult(final String responseMsg) {
        return new ProcessingResult(ResultStatusEnum.SUCCESS, responseMsg);
    }

    /**
     * Static class that initializes the response as a standard success response
     * with an additional information message.
     *
     * @param responseMsg Additional text related to a success response.
     * @param shortMsg A short version of the response message.
     * @return the initialized success response with response message.
     */
    public static ProcessingResult createSuccessResult(final String responseMsg, String shortMsg) {
        return new ProcessingResult(ResultStatusEnum.SUCCESS, responseMsg, shortMsg);
    }

    /**
     * Static class that initializes the response as a general fail response
     * with no additional detail or exception information.
     *
     * @return The fail response with a common string additional message.
     */
    public static ProcessingResult createFailResult() {
        return new ProcessingResult(ResultStatusEnum.FAIL, "Miscellaneous failure.");
    }

    /**
     * Static class that initializes the response as a fail response with an
     * additional detail message but without exception information.
     *
     * @param errorMsg Error related text to the response.
     * @return The fail response with an string additional message
     */
    public static ProcessingResult createFailResult(final String errorMsg) {
        return new ProcessingResult(ResultStatusEnum.FAIL, errorMsg);
    }

    /**
     * Static class that initializes the response as a fail response with an
     * additional detail message but without exception information.
     *
     * @param errorMsg Error related text to the response.
     * @param shortMsg A short version of the error message.
     * @return The fail response with an string additional message
     */
    public static ProcessingResult createFailResult(final String errorMsg, String shortMsg) {
        return new ProcessingResult(ResultStatusEnum.FAIL, errorMsg, shortMsg);
    }

    /**
     * Static class that initializes the response as a fail response with an
     * additional detail message and additional exception information.
     * <p>
     * This should be used in cases when additional information beyond that
     * provided by the exception would be useful to the calling class.
     *
     * @param errorMsg Error related text to the response.
     * @param ex The exception related to the response.
     * @return the fail response with a an string additional message and an
     * exception associated with the error.
     */
    public static ProcessingResult createFailResult(
            final String errorMsg, final Exception ex) {
        return new ProcessingResult(ResultStatusEnum.FAIL, errorMsg, ex);
    }

    /**
     * Static class that initializes the response as a fail response with an
     * additional detail message and additional exception information.
     * <p>
     * This should be used in cases when additional information beyond that
     * provided by the exception would be useful to the calling class.
     *
     * @param errorMsg Error related text to the response.
     * @param shortMsg A short version of the error message.
     * @param ex The exception related to the response.
     * @return the fail response with a an string additional message and an
     * exception associated with the error.
     */
    public static ProcessingResult createFailResult(
            final String errorMsg, final String shortMsg, final Exception ex) {
        return new ProcessingResult(ResultStatusEnum.FAIL, errorMsg, shortMsg, "", ex);
    }

    /**
     * Static class that initializes the response as a fail response with an
     * additional detail message and additional exception information.
     * <p>
     * This should be used in cases when additional information beyond that
     * provided by the exception would be useful to the calling class.
     *
     * @param errorMsg Error related text to the response.
     * @param shortMsg A short version of the error message.
     * @param applicationErrorCode An application specific code that is related to the error message.
     * @param ex The exception related to the response.
     * @return the fail response with a an string additional message and an exception associated with the error.
     */
    public static ProcessingResult createFailResult(
            final String errorMsg, final String shortMsg, final String applicationErrorCode, final Exception ex) {
        return new ProcessingResult(ResultStatusEnum.FAIL, errorMsg, shortMsg, applicationErrorCode, ex);
    }

    /**
     * Static class that initializes the response as an abort
     * response.
     *
     * @return the initialized success response
     */
    public static ProcessingResult createAbortResult() {
        return new ProcessingResult(ResultStatusEnum.ABORT);
    }

    /**
     * Static class that initializes the response as an abort response
     * with an additional information message.
     *
     * @param responseMsg Additional text related to the abort response.
     * @return the initialized success response with response message.
     */
    public static ProcessingResult createAbortResult(final String responseMsg) {
        return new ProcessingResult(ResultStatusEnum.ABORT, responseMsg);
    }

    /**
     * Static class that initializes the response as an abort response
     * with an additional information message.
     *
     * @param responseMsg Additional text related to the abort response.
     * @return the initialized success response with response message.
     */
    public static ProcessingResult createAbortResult(final String responseMsg, final String reasonCode) {
        return new ProcessingResult(ResultStatusEnum.ABORT, responseMsg, reasonCode);
    }

    /**
     * Static class that initializes the response as a fail response with an
     * empty detail message and exception information.
     * <p>
     * This should be used in cases when additional information beyond that
     * provided by the exception would not be useful to the calling class.
     * @param ex The exception related to the response.
     * @return the fail response without a an string additional message and with
     * an exception associated with the error
     */
    public static ProcessingResult createFailResult(final Exception ex) {
        return new ProcessingResult(ResultStatusEnum.FAIL, ex.getMessage(), ex);
    }

    /**
     * Returns the hash code for this object.
     *
     * @return The calculated hash code of this object.
     */
    @Override
    public final int hashCode() {
        return new HashCodeBuilder().append(this.result)
                .append(this.longResponseMsg).toHashCode();
    }

    /**
     * Check for logical equality of this class.
     *
     * @param object The object to be tested for equality with this one.
     * @return True when the responses are equal
     */
    @Override
    public final boolean equals(final Object object) {
        if ((object != null) && (object.getClass() != this.getClass())) {
            return false;
        }
        ProcessingResult resp = (ProcessingResult) object;
        if (this.result != null && resp != null && resp.result != null) {
        return ((this.result.name().equals(resp.result.name()))
                && (this.longResponseMsg.equals(resp.longResponseMsg)));
        }
        return false;
    }

    @org.jetbrains.annotations.NotNull
    public final String buildErrorMessage() {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotBlank(this.getlongResponseMsg())) {
            sb.append(this.getlongResponseMsg());
            sb.append("; ");
        } else if (StringUtils.isNotBlank(this.getShortResponseMsg())) {
            sb.append(this.getShortResponseMsg());
            sb.append("; ");
        }
        if (this.getException() != null) {
            sb.append(this.getException().getMessage());
            sb.append("\n");
            StringWriter exceptionWriter = new StringWriter();
            this.getException().printStackTrace(new PrintWriter(exceptionWriter));
            sb.append(exceptionWriter.toString());
        }
        return sb.toString();
    }

    public String test() {
        return "";
    }

}