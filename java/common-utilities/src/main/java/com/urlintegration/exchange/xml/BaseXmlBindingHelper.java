/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.urlintegration.exchange.xml;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.xml.sax.SAXException;

/**
 * An abstract base helper class for all XML and WSDL binding object helpers.
 * <p>
 * All biding helper objects should extend from this class either directly or indirectly. It provides the
 * common implementation methods getSchema and getContextPackages of
 * {@link com.urlintegration.exchange.xml.MarshallHelper}.
 *
 * @author JMierwa
 */
public abstract class BaseXmlBindingHelper implements MarshallHelper {
    /**
     * Delimiter in a string to identify locations
     */
    public static final String SCHEMA_LOCATION_DELIMITER = ":";

    private final String jaxbContext;
    private final String schemaLocation;

    /**
     * Base constructor.
     * 
     * @param jaxbContext
     *            A colon delimited string of all of the class namespaces associated with this helpers class.
     *            <p>
     *            It is used for setting up the JAXBContext when marshaling.
     * @param schemaLocation
     *            The location of the class schema file on the CLASSPATH.
     */
    public BaseXmlBindingHelper(String jaxbContext, String schemaLocation) {
        this.jaxbContext = jaxbContext;
        this.schemaLocation = schemaLocation;
    }

    /**
     * Returns The {@link javax.xml.validation.Schema} associated with the helper.
     * 
     * @return The {@link javax.xml.validation.Schema} to be used for validation.
     * @throws SAXException
     *             Occurs on validation or marshaling errors.
     */
    public Schema getSchema() throws SAXException, SchemaAccessException {
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        String[] parts = schemaLocation.split(SCHEMA_LOCATION_DELIMITER);
        ClassLoader cl = this.getClass().getClassLoader();
        ArrayList<Source> schemaSourceList = new ArrayList<Source>();
        for (String schemaLoc : parts) {
            InputStream in = cl.getResourceAsStream(schemaLoc);

            if (in == null) {
                throw new SchemaAccessException(String.format("Could not access schema at path %s", schemaLoc));
            }
            URL url = cl.getResource(schemaLoc);
            if (url == null) {
                throw new SchemaAccessException(String.format("Could not create URL for schema at path %s", schemaLoc));
            }
            Source source = new StreamSource(in);
            source.setSystemId(url.toExternalForm());
            schemaSourceList.add(source);
        }
        Source[] schemaSourceArr = new Source[schemaSourceList.size()];
        schemaSourceArr = schemaSourceList.toArray(schemaSourceArr);
        return schemaFactory.newSchema(schemaSourceArr);
    }

    /**
     * Returns a colon delimited string of all of the class namespaces for setting up the JAXBContext when
     * marshaling.
     * 
     * @return {@link java.lang.String}
     */
    public String getContextPackages() {
        return jaxbContext;
    }

    /**
     * Validates an unmarshalled XML Binding object against the schema. The referenced schema must be on the class path.
     *
     * @param typeNamespace The {@link javax.xml.namespace.QName} namespace URI to which the type belongs.
     * @param elementName The XML element name to which the input binding object is related.
     * @param inputBindingObject The java object that is the XML binding object. It is not type checked prior to attempting the marshalling.
     * @return the marshalled XML binding object as a {@link java.lang.String}.
     * @throws BindingException {@link com.urlintegration.exchange.xml.BindingException} when any unmarshalling or validation error occurs.
     */
    protected String validate(String typeNamespace, String elementName, Object inputBindingObject) throws BindingException {
        String output;
        try {
            output = JAXBUtil.validate(typeNamespace, elementName, this.jaxbContext, getSchema(), inputBindingObject).toString();
        } catch (JAXBException jex) {
            throw new BindingException(String.format("Unexpected error with marshaller for %s/%s.", typeNamespace, elementName), jex);
        } catch (SchemaAccessException sex) {
            throw new BindingException(String.format("Failed accessing or creating schema for validation of %s/%s.", typeNamespace, elementName), sex);
        } catch (SAXException sex) {
            throw new BindingException(String.format("Validation parsing failure for %s/%s.", typeNamespace, elementName), sex);
        }
        return output;
    }
}
