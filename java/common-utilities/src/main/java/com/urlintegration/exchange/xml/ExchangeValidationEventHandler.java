/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.urlintegration.exchange.xml;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.ValidationEventLocator;

/**
 * Validation handler to display details about errors encountered during marshaling
 * and unmarshaling.
 * @author JMierwa
 */
public class ExchangeValidationEventHandler implements ValidationEventHandler {
    private static final Logger LOG = LoggerFactory.getLogger(ExchangeValidationEventHandler.class);
    private static final String LS = System.getProperty( "line.separator" );
    private List<ValidationError> marshallingErrors = new ArrayList<ValidationError>();

    /**
     * Processor for a raised event during marshaling and unmarshaling.
     * <p>
     * It keeps track of all errors encountered.
     * @param event The {@link javax.xml.bind.ValidationEvent} raised.
     * @return True when processing should continue and false otherwise;
     * <p>
     * Always returns true.
     */
    public boolean handleEvent(ValidationEvent event) {
        marshallingErrors.add(translateEvent(event));
        return true;
    }
    
    /**
     * Returns a string representation of each error message raised.
     * @return {@link java.lang.String}
     */
    public String renderResults() {
        if (this.marshallingErrors.size() > 0) {
            int errorNum = 1;
            StringBuilder sb = new StringBuilder(Integer.toString(this.marshallingErrors.size()));
            sb.append(" Marshalling errors:");
            sb.append(LS);
            for (ValidationError error : this.marshallingErrors) {
                sb.append(String.format("Error[%s]: %s%s", errorNum, error.toString(), LS));
            }
            return sb.toString();
        } else {
            return "";
        }
    }
    
    public List<ValidationError> getErrors() {
        return this.marshallingErrors;
    }
    
    /**
     * Writes any encountered errors to the output log.
     */
    public void logResults() {
        if (this.marshallingErrors.size() > 0) {
            LOG.error(renderResults());
        }
    }
    
    /**
     * Returns true when any events were raised during marshalling or unmarshalling.
     * @return True when any events were raised during marshalling or unmarshalling.
     */
    public boolean failed() {
        return !marshallingErrors.isEmpty();
    }

    private ValidationError translateEvent( ValidationEvent event ){

        ValidationEventLocator locator = event.getLocator();
        if( locator != null ){
            return new ValidationError( ValidationErrorEnum.SCHEMA, event.getMessage(), "MessageBody", locator.getLineNumber(), locator.getColumnNumber() );
        }
        return new ValidationError( ValidationErrorEnum.SCHEMA, event.getMessage(), "MessageBody", null, null );
    }
}
