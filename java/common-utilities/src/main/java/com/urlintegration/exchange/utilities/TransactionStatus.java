/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.urlintegration.exchange.utilities;

/**
 * Enumeration defining the status of the exchange.
 *
 * @author JMierwa
 */
public enum TransactionStatus {

    /**
     * Successful transaction.
     */
    S("Success", "Successful transaction,"),

    /**
     * Failure of transaction.
     */
    E("Error", "Transaction failed");

    private String name;
    private String description;

    /**
     * Constructor for the enumeration.
     *
     * @param name The user-friendly name of the enumeration.
     * @param description The definition of the enumeration value.
     */
    TransactionStatus(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    /**
     * Get the user friendly name of the enumeration.
     * @return {@link java.lang.String}.
     */
    public final String getName() {
        return name;
    }

    /**
     * Get the definition of the enumeration value.
     * @return {@link java.lang.String}.
     */
    public final String getDescription() {
        return description;
    }
}