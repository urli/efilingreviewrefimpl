/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.urlintegration.exchange.utilities;

/**
 * Exception class for errors when accessing data from persisted storage of any sort.
 * @author Walter Wood
 */
public class DataAccessException extends Exception {
    private String errorCode;

    /**
     * Constructs an instance of {@link com.urlintegration.exchange.utilities.DataAccessException} with
     * the specified detail message.
     *
     * @param message the detail message.
     */
    public DataAccessException(final String message) {
        super(message);
    }

    /**
     * Constructs an instance of {@link com.urlintegration.exchange.utilities.DataAccessException} with
     * the specified detail message.
     *
     * @param message the detail message.
     * @param errorCode the error code
     */
    public DataAccessException(final String message, final String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    /**
     * Constructs an instance of {@link com.urlintegration.exchange.utilities.DataAccessException} with
     * the original exception that occurred.
     *
     * @param e the original exception.
     */
    public DataAccessException(final Exception e) {
        super(e);
    }

    /**
     * Constructs an instance of {@link com.urlintegration.exchange.utilities.DataAccessException} with
     * the original exception that occurred.
     *
     * @param e the original exception.
     * @param errorCode the error code
     */
    public DataAccessException(final Exception e, final String errorCode) {
        super(e);
        this.errorCode = errorCode;
    }

    /**
     * Constructs an instance of {@link com.urlintegration.exchange.utilities.DataAccessException} with
     * the specified detail message and the original exception that occurred.
     *
     * @param message the detail message.
     * @param e the original exception.
     */
    public DataAccessException(final String message, final Exception e) {
        super(message, e);
    }

    /**
     * Constructs an instance of {@link com.urlintegration.exchange.utilities.DataAccessException} with
     * the specified detail message and the original exception that occurred.
     *
     * @param message the detail message.
     * @param errorCode the error code
     * @param e the original exception.
     */
    public DataAccessException(final String message, final String errorCode, final Exception e) {
        super(message, e);
        this.errorCode = errorCode;
    }

    /**
     * Returns an error code related to the data access error
     * @return the code
     */
    public String getErrorCode() {
        return this.errorCode;
    }

    /**
     * Sets the error code for the exception.
     * @param errorCode the code to set
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
