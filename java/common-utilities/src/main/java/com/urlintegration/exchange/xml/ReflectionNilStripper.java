/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.urlintegration.exchange.xml;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.namespace.QName;

import org.apache.log4j.Logger;

/**
 * A partially complete generic class for identifying and resolving false NIL attribute settings in binding objects. This
 * can occur because of ambiguities in the JAX WS binding specification.
 * <p>
 *     This is an incomplete coding of a fully generic nil stripping implementation and needs to be corrected.
 * </p>
 */
public class ReflectionNilStripper {

    private static final Logger logger = Logger.getLogger(ReflectionNilStripper.class);

    /**
     * Recursively checks for and corrects any objects that would cause an XML element to incorrectly have the xsi:nil="true"
     * set when there are child elements present for that element.
     * @param object An XML binding object to be checked for false xsi:nil attribute settings.
     * @param <T> The type of the object
     * @return The corrected version of the input object.
     * @throws ClassNotFoundException {@link java.lang.ClassNotFoundException} when the class cannot be found on the classpath.
     * @throws IllegalArgumentException {@link java.lang.IllegalArgumentException} when something goes wrong with the recursive method calling.
     * @throws IllegalAccessException {@link java.lang.IllegalAccessException} when something goes wrong with the recursive method calling.
     * @throws InvocationTargetException {@link java.lang.reflect.InvocationTargetException} when something goes wrong with the recursive method calling.
     * @throws SecurityException {@link java.lang.SecurityException} when something goes wrong with the recursive method calling.
     * @throws NoSuchMethodException {@link java.lang.NoSuchMethodException} when something goes wrong with the recursive method calling.
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static <T extends Object> T strip(T object) throws ClassNotFoundException, IllegalArgumentException,
            IllegalAccessException, InvocationTargetException, SecurityException, NoSuchMethodException {
        Class<?> c = Class.forName(object.getClass().getCanonicalName());
        List<Field> fields = getAllFields(c);
        for (Field field : fields) {
            for (Annotation a : field.getDeclaredAnnotations()) {
                if (a.annotationType().equals(XmlElement.class) || a.annotationType().equals(XmlElementRef.class)) {
                    Method m = getMethodForField(field, c);
                    if (m != null) {
                        Object result = m.invoke(object, (Object[]) null);

                        if (result != null) {
                            JAXBElement<?> j;
                            if (result instanceof JAXBElement) {
                                stripJAXBElement((JAXBElement) result);
                            } else {
                                QName qname = getQname(a);
                                j = new JAXBElement(qname, result.getClass(), result);
                                if (result instanceof Collection) {
                                    if (((Collection) result).size() > 0 && j.isNil()) {
                                        logger.info(String.format("found false nill for %s", j.getName()));
                                        j.setNil(false);
                                    }
                                    for (Object o : ((Collection) result)) {
                                        if (o != null) {
                                            if (o instanceof JAXBElement) {
                                                stripJAXBElement((JAXBElement) o);
                                            } else {
                                                strip(o);
                                            }
                                        } else {
                                            logger.info(String.format("Object %s contains a null %s member in a collection ", object.getClass().getSimpleName(), qname));
                                        }
                                    }
                                } else {
                                    if (j.isNil()) {
                                        logger.info(String.format("found false nill for %s", j.getName()));
                                        j.setNil(false);
                                    }
                                    strip(result);
                                }

                            }
                        }
                    }
                }
            }
        }
        return object;
    }

    @SuppressWarnings("rawtypes")
    private static List<Field> getAllFields(Class c) {
        List<Field> fields = new ArrayList<Field>();
        fields.addAll(Arrays.asList(c.getDeclaredFields()));
        if (c.getSuperclass() != null) {
            fields.addAll(getAllFields(c.getSuperclass()));
        }
        return fields;
    }

    @SuppressWarnings("rawtypes")
    private static List<Method> getAllMethods(Class c) {
        List<Method> methods = new ArrayList<Method>();
        methods.addAll(Arrays.asList(c.getDeclaredMethods()));
        if (c.getSuperclass() != null) {
            methods.addAll(getAllMethods(c.getSuperclass()));
        }
        return methods;
    }

    @SuppressWarnings("rawtypes")
    private static void stripJAXBElement(JAXBElement element) throws ClassNotFoundException, IllegalArgumentException,
            IllegalAccessException, InvocationTargetException, SecurityException, NoSuchMethodException {
        if (element.getValue() != null) {
            if (element.isNil()) {
                logger.info(String.format("found false nill for %s", element.getName()));
                element.setNil(false);
            }
            strip(element.getValue());
        }
    }

    private static QName getQname(Annotation a) {
        if (!(a instanceof XmlElementRef) && !(a instanceof XmlElement)) {
            throw new IllegalArgumentException();
        }
        QName qname;
        if (a instanceof XmlElementRef) {
            qname = new QName(((XmlElementRef) a).name(), ((XmlElementRef) a).namespace());
        } else {
            qname = new QName(((XmlElement) a).name(), ((XmlElement) a).namespace());
        }
        return qname;
    }

    @SuppressWarnings("rawtypes")
    private static Method getMethodForField(Field f, Class c) {
        List<Method> methods = getAllMethods(c);
        for (Method method : methods) {
            if (method.getName().toUpperCase().equals(getMethodNameFromField(f).toUpperCase())) {
                return method;
            }
        }
        return null;
    }

    private static String getMethodNameFromField(Field field) {
        String fieldName = field.getName();
        if (fieldName.contains("_")) {
            fieldName = fieldName.replaceAll("_", "");
        }
        return "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
    }
}