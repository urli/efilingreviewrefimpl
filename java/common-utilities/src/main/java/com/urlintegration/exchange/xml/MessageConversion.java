/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.urlintegration.exchange.xml;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * A common conversion class to apply an XSLT transform to an XML document or fragment.
 * @author JMierwa
 */
public class MessageConversion {

    private static Logger log = LoggerFactory.getLogger(MessageConversion.class);

    /**
     * Applies an XSL transform to an XML byte array and returns a {@link java.io.ByteArrayOutputStream}
     * @param xslPath the physical location of the XSL transform
     * @param path the relative path to access any referenced entities in the transform.
     * @param inputObjectAsArray An XML fragment or instance represented as a byte array.
     * @return {@link java.io.ByteArrayOutputStream}
     * @throws TransformerException {@link javax.xml.transform.TransformerException}
     */
    public ByteArrayOutputStream convertMessage(String xslPath, String path, byte[] inputObjectAsArray) throws TransformerException  {
        if (log.isDebugEnabled()) {
            log.debug(String.format("Entering: %s.%s", this.getClass().getName(), "convertMessage()"));
        }
        InputStream iStream = this.getClass().getClassLoader().getResourceAsStream(xslPath);
        Source xslSource = new StreamSource(iStream);
        System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl");
        TransformerFactory tFactory1 = new net.sf.saxon.TransformerFactoryImpl();
        tFactory1.setURIResolver(new ClasspathUriResolver(path));
        Transformer transformer1 = tFactory1.newTransformer(xslSource);
        ByteArrayOutputStream serviceOutput = new ByteArrayOutputStream();
        StreamResult res = new StreamResult(serviceOutput);
        transformer1.transform(new StreamSource(new ByteArrayInputStream(inputObjectAsArray)), res);
        if (log.isDebugEnabled()) {
            log.debug(String.format("Exiting: %s.%s", this.getClass().getName(), "convertMessage"));
        }
        return (ByteArrayOutputStream) res.getOutputStream();
    }
}
