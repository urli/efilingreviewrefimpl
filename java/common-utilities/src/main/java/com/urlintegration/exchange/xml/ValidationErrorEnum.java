/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.urlintegration.exchange.xml;

/**
 *
 * @author JMierwa
 */
public enum ValidationErrorEnum {

    UNSPECIFIED("Unspecified validation error", "102"),
    MTOM_ERROR("Error accessing MTOM attachment", "1"),
    SCHEMA("Schema validation error.", "24"),
    SCHEMATRON("Schematron business rules validation error.", "100");
    
    private final String value;
    private final String ecfErrorCodeText;

    /**
     * private constructor for the enumerator.
     * @param val The user friendly description for the enumeration.
     */
    private ValidationErrorEnum(final String val, final String ecfErrorCodeText) {
        this.value = val;
        this.ecfErrorCodeText = ecfErrorCodeText;
    }

    /**
     * Returns the user friendly value of the enumeration value.
     *
     * @return the user friendly value of the enumeration value.
     */
    public final String value() {
        return value;
    }
    
    /**
     * Returns the ECF Error Code Text value that this value would be equivalent to.
     * @return {@link java.lang.String}
     */
    public final String ecfErrorCode() {
        return this.ecfErrorCodeText;
    }
}
