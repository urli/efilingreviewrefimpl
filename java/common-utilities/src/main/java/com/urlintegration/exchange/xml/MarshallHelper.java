/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.urlintegration.exchange.xml;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.validation.Schema;
import org.xml.sax.SAXException;

/**
 * Helper interface for accessing context specific information for marshaling.
 * @author JMierwa
 */
public interface MarshallHelper {
    
    /**
     * Returns the {@link javax.xml.bind.JAXBElement} of the element to which the helper pertains.
     * @param obj the Java binding object for which a {@link javax.xml.bind.JAXBElement} is required.
     * @return The {@link javax.xml.bind.JAXBElement}
     */
    JAXBElement<?> toJAXBElement(Serializable obj);

    /**
     * Returns The {@link javax.xml.validation.Schema} associated with the helper.
     * @return The {@link javax.xml.validation.Schema} to be used for validation.
     * @throws SAXException Occurs on validation or marshaling errors.
     * @throws SchemaAccessException When the schema location does not find a resource
     * to open.
     */
    Schema getSchema() throws SAXException, SchemaAccessException;
    
    /**
     * Returns a colon delimited string of all of the class namespaces for
     * setting up the JAXBContext when marshaling.
     * @return {@link java.lang.String}
     */
    String getContextPackages();
}
