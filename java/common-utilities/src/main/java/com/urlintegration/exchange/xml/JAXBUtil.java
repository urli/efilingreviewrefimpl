/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.urlintegration.exchange.xml;

import com.urlintegration.io.ByteArrayInOutStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;

import org.apache.commons.io.Charsets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 * A common JAXB binding helper for performing common utility functions.
 *
 * @author JMierwa
 */
public final class JAXBUtil {

    private static final Logger LOG = LoggerFactory.getLogger(JAXBUtil.class.getName());
    private static final String ECF_SCHEMA_ERROR_CODE = "24";

    /**
     * Private constructor that we explicitly define to prevent external and
     * internal instantiation.
     */
    private JAXBUtil() {
        throw new AssertionError("Should never instantiate this class.");
    }

    /**
     * Marshall a JAXB binding object and return the output as a byte array.
     * @param <T> A JAXB binding object to be marshalled.
     * @param typeNamespace The fully qualified namespace of the JAXB binding
     * object.
     * @param elementName The XML element name that the JAXB binding object is
     * associated with.
     * @param contextPackages A colon delimited string of all of the class
     * namespaces for setting up the JAXBContext.
     * @param obj The JAXB binding object to be marshalled.
     * @return {@link java.io.ByteArrayOutputStream}
     * @throws JAXBException A marshalling error that occurred.
     * @throws com.urlintegration.exchange.xml.BindingException
     */
    public static <T> ByteArrayInOutStream toByteArrayOutputStream(
            final String typeNamespace,
            final String elementName,
            final String contextPackages,
            final T obj)
            throws JAXBException, BindingException {

        LOG.debug("Marshalling {}.{} to XML.", typeNamespace, elementName);
        ExchangeValidationEventHandler errorHandler = new ExchangeValidationEventHandler();
        JAXBContext jc = JAXBContext.newInstance(contextPackages);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.setEventHandler(errorHandler);
        QName qn = new QName(typeNamespace, elementName);
        JAXBElement je = new JAXBElement(qn, obj.getClass(), obj);
        ByteArrayInOutStream baos = new ByteArrayInOutStream();
        m.marshal(je, baos);
        if (errorHandler.failed()) {
            LOG.error(errorHandler.renderResults());
            throw new BindingException(errorHandler.renderResults(), errorHandler.getErrors(), ValidationErrorEnum.SCHEMA.toString());
        }
        return baos;
    }

    /**
     * Marshall a JAXB binding object and return the output as an input stream.
     * @param <T> A JAXB binding object to be marshalled.
     * @param typeNamespace The fully qualified namespace of the JAXB binding
     * object.
     * @param elementName The XML element name that the JAXB binding object is
     * associated with.
     * @param contextPackages A colon delimited string of all of the class
     * namespaces for setting up the JAXBContext.
     * @param obj The JAXB binding object to be marshalled.
     * @return {@link com.urlintegration.io.ByteArrayInOutStream}
     * @throws JAXBException A marshalling error that occurred.
     * @throws com.urlintegration.exchange.xml.BindingException
     */
    public static <T> InputStream toInputStream(
            final String typeNamespace,
            final String elementName,
            final String contextPackages,
            final T obj)
            throws JAXBException, BindingException {

        return toByteArrayOutputStream(typeNamespace, elementName, contextPackages, obj).getInputStream();
    }

    /**
     * Marshall a JAXB binding object and return the output as a string.
     * @param <T> A JAXB binding object to be marshalled.
     * @param typeNamespace The fully qualified namespace of the JAXB binding
     * object.
     * @param elementName The XML element name that the JAXB binding object is
     * associated with.
     * @param contextPackages A colon delimited string of all of the class
     * namespaces for setting up the JAXBContext.
     * @param obj The JAXB binding object to be marshalled.
     * @return {@link java.io.ByteArrayOutputStream}
     * @throws JAXBException A marshalling error that occurred.
     * @throws com.urlintegration.exchange.xml.BindingException
     * @throws java.io.UnsupportedEncodingException
     */
    public static <T> String toString(
            final String typeNamespace,
            final String elementName,
            final String contextPackages,
            final T obj)
            throws JAXBException, BindingException, UnsupportedEncodingException {

        return toByteArrayOutputStream(typeNamespace, elementName, contextPackages, obj)
                .toString(Charsets.UTF_8.displayName());
    }

    /**
     * Returns an unmarshalled object from an input string.
     * @param <T> The type of the returned object
     * @param contextPackages A colon delimited string of all of the class
     * namespaces for setting up the JAXBContext.
     * @param classObj The seed class instance of the object to be returned.
     * <p>
     * This will typically be passed in as <code>new myClass()</code>.
     * @param inputMsg The string to be unmarshalled.
     * @return The unmarshalled object.
     * @throws JAXBException 
     * @throws com.urlintegration.exchange.xml.BindingException 
     */
    public static <T> T toObject(
            final String contextPackages, final T classObj, final String inputMsg)
    throws JAXBException, BindingException {

        JAXBContext jc = JAXBContext.newInstance(contextPackages, classObj.getClass().getClassLoader());            
        ExchangeValidationEventHandler errorHandler = new ExchangeValidationEventHandler();           
        Unmarshaller u = jc.createUnmarshaller();
        u.setEventHandler(errorHandler);
        Object obj = u.unmarshal(new StreamSource(new StringReader(inputMsg)));
        JAXBElement je = (JAXBElement) obj;
        T returnObj = (T) je.getValue();   
        if (errorHandler.failed()) {
            LOG.error(errorHandler.renderResults());
            throw new BindingException(errorHandler.renderResults());
        }      
        return returnObj;
    }

    /**
     * Returns an unmarshalled object from an input string.
     * @param <T> The type of the returned object
     * @param contextPackages A colon delimited string of all of the class
     * namespaces for setting up the JAXBContext.
     * @param classObj The seed class instance of the object to be returned.
     * <p>
     * This will typically be passed in as <code>new myClass()</code>.
     * @param is The {@link java.io.InputStream} to be unmarshalled.
     * @return The unmarshalled object.
     * @throws JAXBException when marshalling fails.
     * @throws BindingException {@link com.urlintegration.exchange.xml.BindingException} when marshalling or false nil stripping fails.
     */
    public static <T> T toObject(
            final String contextPackages, final T classObj, final InputStream is)
    throws JAXBException, BindingException {

        JAXBContext jc = JAXBContext.newInstance(contextPackages, classObj.getClass().getClassLoader());            
        ExchangeValidationEventHandler errorHandler = new ExchangeValidationEventHandler();        
        Unmarshaller u = jc.createUnmarshaller();
        u.setEventHandler(errorHandler);
        Object obj = u.unmarshal(is);
        JAXBElement je = (JAXBElement) obj;
        T returnObj = (T) je.getValue();   
        if (errorHandler.failed()) {
            LOG.error(errorHandler.renderResults());
            throw new BindingException(errorHandler.renderResults());
        }            
        return returnObj;
    }
    
    /**
     * Marshall a JAXB binding object and return the output as a byte array.
     * @param <T> A JAXB binding object to be Marshalled.
     * @param typeNamespace The fully qualified namespace of the JAXB binding
     * object.
     * @param elementName The XML element name that the JAXB binding object is
     * associated with.
     * @param contextPackages A colon delimited string of all of the class
     * namespaces for setting up the JAXBContext.
     * @param schema
     * @param obj The JAXB binding object to be marshalled.
     * @return {@link java.io.ByteArrayOutputStream}
     * @throws JAXBException A marshalling error that occurred.
     * @throws com.urlintegration.exchange.xml.BindingException
     * @throws com.urlintegration.exchange.xml.SchemaAccessException
     * @throws org.xml.sax.SAXException
     */
    public static <T> ByteArrayOutputStream validate(
            final String typeNamespace,
            final String elementName,
            final String contextPackages,
            final Schema schema,
            final T obj)
            throws JAXBException, BindingException, SAXException, SchemaAccessException {

        LOG.debug("Validating {}.{}.", typeNamespace, elementName);
        ExchangeValidationEventHandler errorHandler = new ExchangeValidationEventHandler();
        JAXBContext jc = JAXBContext.newInstance(contextPackages);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.setEventHandler(errorHandler);
        m.setSchema(schema);
        QName qn = new QName(typeNamespace, elementName);
        JAXBElement je = new JAXBElement(qn, obj.getClass(), obj);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        m.marshal(je, baos);
        if (errorHandler.failed()) {
            LOG.error(errorHandler.renderResults());
            throw new BindingException("ValidationFailed", errorHandler.getErrors(), ECF_SCHEMA_ERROR_CODE);
        }
        return baos;
    }
}