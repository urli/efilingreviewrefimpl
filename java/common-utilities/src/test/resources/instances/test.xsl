<?xml version="1.0" encoding="UTF-8"?>
<!--
  Copyright (c) 2017 by URL Integration

   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
      - Prior written consent by URL Integration is required.
      - Redistributions of source code must retain the above copyright notice,
         this list of conditions and the following disclaimer.
      - Redistributions in binary form must reproduce the above copyright notice,
         this list of conditions and the following disclaimer in the documentation
         and/or other materials provided with the distribution.
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
  SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  -->

<xsl:stylesheet version="2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:fn="http://www.w3.org/2005/xpath-functions" 
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:msg="http://urlintegration.com/efiling/ProcessOfServicEmaileMessage/1.0"
	xmlns:rpt="urn:urlintegration:exchange:disposition-matching:audit"
 	xmlns:nc="http://niem.gov/niem/niem-core/2.0"
        xmlns:dt="http://xsltsl.org/date-time" 
  	xmlns:str="http://xsltsl.org/string"  
        xmlns:dec="http://www.cjis.iowa.gov/DispositionMatching/ExchangeCounts/1.0"
        xmlns:iaext="http://www.cjis.iowa.gov/extension/1.0"
        xmlns:s="http://niem.gov/niem/structures/2.0"
        xmlns:j="http://niem.gov/niem/domains/jxdm/4.1" 
        xmlns:a="http://niem.gov/niem/appinfo/2.0"
	exclude-result-prefixes="msg str dt nc rpt fn fo dec iaext s j a">
	
	<xsl:import href="date-time.xsl"/>
	<xsl:import href="string.xsl"/>
		
	<xsl:output method="html"/>
	
	<xsl:template match="/">
		<html>
			<head>
				<title>CJIS Disposition Matching Daily Report</title>
				<style type="css">
					table, th, td {
					    border: 1px solid black;
					    border-collapse: collapse;
					}
					th, td {
					    padding: 5px;
					}
					th {
					    text-align: left;
					}
				</style>
			</head>	
			<body>
				<h1>CJIS Disposition Matching Daily Report</h1>
				<p style="font: 0.75em arial, sans-serif;">
					Disposition Matching Daily totals 
					<strong><xsl:value-of select="./msg:ServiceRequestMessage/msg:CaseNumberText"/></strong>
				</p>
				<table id="dates">
					<tr>
						<td>From Date/Time</td>
						<td>
							<xsl:if test="string-length(normalize-space(//rpt:ReportDateRange/nc:StartDate/nc:DateTime))">
								<xsl:call-template name="dt:format-date-time">
									<xsl:with-param name="year" select="substring(//rpt:ReportDateRange/nc:StartDate/nc:DateTime,1,4)"/>
									<xsl:with-param name="month" select="substring(//rpt:ReportDateRange/nc:StartDate/nc:DateTime,6,2)"/>
									<xsl:with-param name="day" select="substring(//rpt:ReportDateRange/nc:StartDate/nc:DateTime,9,2)"/>
									<xsl:with-param name="format" select="'%B&#160;%d,&#160;%Y&#160;'"/>
								</xsl:call-template> 
								<xsl:text>  </xsl:text>
								<xsl:call-template name="dt:format-date-time">
									<xsl:with-param name="hour" select="substring(//rpt:ReportDateRange/nc:StartDate/nc:DateTime,12,2)"/>
									<xsl:with-param name="minute" select="substring(//rpt:ReportDateRange/nc:StartDate/nc:DateTime,15,2)"/>
									<xsl:with-param name="format" select="'%i:%M %q'"/>
								</xsl:call-template>
							</xsl:if>
						</td>
					</tr>
					<tr>
						<td>To Date/Time</td>
						<td>
							<xsl:if test="string-length(normalize-space(//rpt:ReportDateRange/nc:EndDate/nc:DateTime))">
								<xsl:call-template name="dt:format-date-time">
									<xsl:with-param name="year" select="substring(//rpt:ReportDateRange/nc:EndDate/nc:DateTime,1,4)"/>
									<xsl:with-param name="month" select="substring(//rpt:ReportDateRange/nc:EndDate/nc:DateTime,6,2)"/>
									<xsl:with-param name="day" select="substring(//rpt:ReportDateRange/nc:EndDate/nc:DateTime,9,2)"/>
									<xsl:with-param name="format" select="'%B&#160;%d,&#160;%Y&#160;'"/>
								</xsl:call-template> 
								<xsl:text>  </xsl:text>
								<xsl:call-template name="dt:format-date-time">
									<xsl:with-param name="hour" select="substring(//rpt:ReportDateRange/nc:EndDate/nc:DateTime,12,2)"/>
									<xsl:with-param name="minute" select="substring(//rpt:ReportDateRange/nc:EndDate/nc:DateTime,15,2)"/>
									<xsl:with-param name="format" select="'%i:%M %q'"/>
								</xsl:call-template>
							</xsl:if>
						</td>
					</tr>
				</table>
				<p><br/></p>
				<div id="exchange" style="border-top: 2px DarkBlue dotted;overflow:visible;height:9px;margin: 5px 0 10px 0;">
					<table id="exchangeTotals" style="table-layout:fixed;width:100%">
						<tbody>
							<tr>
								<th></th>
								<th>Totals</th>
								<th>DA</th>
								<th>DC</th>
								<th>DD</th>
								<th>AA</th>
								<th>TC</th>
							</tr>
							<tr>
								<td>Send Attempts</td>
								<xsl:apply-templates select="//rpt:SendAttemptCount"/>
							</tr>
							<tr>
								<td>Send Success</td>
								<xsl:apply-templates select="//rpt:SuccessSentCount"/>
							</tr>
							<tr>
								<td>Send Failure</td>
								<xsl:apply-templates select="//rpt:SendFailCount"/>
							</tr>
							<tr>
								<td>Success Async Received</td>
								<xsl:apply-templates select="//rpt:SuccessAsyncResponseCount"/>
							</tr>
							<tr>
								<td>Fail Async Received</td>
								<xsl:apply-templates select="//rpt:FailAsyncResponseCount"/>
							</tr>
							<tr>
								<td>No Async Received</td>
								<xsl:apply-templates select="//rpt:AsyncNotReceivedCount"/>
							</tr>
							
						</tbody>
					</table>
				</div>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="//rpt:SendAttemptCount">
		<td><xsl:value-of select="."/></td>		
	</xsl:template>
	
	<xsl:template match="//rpt:SuccessSentCount">
		<td><xsl:value-of select="."/></td>		
	</xsl:template>
	
	<xsl:template match="//rpt:SendFailCount">
		<td><xsl:value-of select="."/></td>		
	</xsl:template>
	
	<xsl:template match="//rpt:SuccessAsyncResponseCount">
		<td><xsl:value-of select="."/></td>		
	</xsl:template>
	
	<xsl:template match="//rpt:FailAsyncResponseCount">
		<td><xsl:value-of select="."/></td>		
	</xsl:template>
	
	<xsl:template match="//rpt:AsyncNotReceivedCount">
		<td><xsl:value-of select="."/></td>		
	</xsl:template>
	
</xsl:stylesheet>