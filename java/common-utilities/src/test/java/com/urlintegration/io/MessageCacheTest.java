package com.urlintegration.io;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static org.junit.Assert.*;

/**
 * Created by JMierwa on 3/19/2018.
 */
public class MessageCacheTest {
    private static final Logger LOG = LoggerFactory.getLogger(MessageCacheTest.class);
    private static final String MESSAGE_ID = "1234567";
    private String baseTestPath;

    @After
    public void tearDown() throws Exception {
        FileUtils.deleteQuietly(new File(this.baseTestPath));
    }

    @Before
    public void setUp() throws Exception {
        SimpleDateFormat lDateFormat = new SimpleDateFormat("yyyyMMdd");
        String lstrDate = lDateFormat.format(Calendar.getInstance().getTime());
        String lstrYear = lstrDate.substring(0, 4);
        this.baseTestPath = "TestDir" + File.separator + lstrYear;
        LOG.debug("baseTestPath path is {}", this.baseTestPath);
    }

    @Test
    public void testGetDirectory() throws Exception {
        System.out.println("validateRequest");
        MessageCache instance = new MessageCache("TestDir");
        boolean expResult = true;
        String result0 = instance.getDirectory("123");
        LOG.debug("Generated directory path is {}", result0);
        File temp = new File(result0);
        boolean result = temp.exists();
        assertEquals(expResult, result);
    }

}