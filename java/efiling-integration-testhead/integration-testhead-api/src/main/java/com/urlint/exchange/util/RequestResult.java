/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.urlint.exchange.util;

/**
 * Simple POJO to convey s result.
 * Created on 9/20/2017.
 */
public class RequestResult {
    private ResultCode resultCode;
    private String resultMessage;

    /**
     * Constructor to build a request result.
     * @param resultCode The {@link com.urlint.exchange.util.ResultCode} indicating success or failure.
     * @param resultMessage A {@link java.lang.String} describing the actual detail of the result.
     */
    private RequestResult(ResultCode resultCode, String resultMessage) {
        this.resultCode = resultCode;
        this.resultMessage = resultMessage;
    }

    /**
     * Returns true when the {@link com.urlint.exchange.util.RequestResult} contains a {@link com.urlint.exchange.util.ResultCode}
     * of "S".
     * @return {@link java.lang.Boolean}
     */
    public boolean isSuccessful() {
        return resultCode == ResultCode.S;
    }

    /**
     * Returns a {@link com.urlint.exchange.util.RequestResult} containing a {@link com.urlint.exchange.util.ResultCode}
     * of "S".
     * @return {@link com.urlint.exchange.util.RequestResult}
     */
    public static RequestResult createSuccess() {
        ResultCode code = ResultCode.S;
        return new RequestResult(code, code.getDescription());
    }

    /**
     * Returns a {@link com.urlint.exchange.util.RequestResult} containing a {@link com.urlint.exchange.util.ResultCode}
     * of "E".
     * @return {@link com.urlint.exchange.util.RequestResult}
     */
    public static RequestResult createError(String errorMsg) {
        ResultCode code = ResultCode.E;
        return new RequestResult(code, errorMsg);
    }
}
