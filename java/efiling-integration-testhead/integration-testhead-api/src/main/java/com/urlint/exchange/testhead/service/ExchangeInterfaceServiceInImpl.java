/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.urlint.exchange.testhead.service;

import cje.common.header.CJEExchangeHeaderType;
import cje.efiling.ecf.DocumentRenditionType;
import cje.efiling.filing.FilingMessageType;
import cje.efiling.niem.core.BinaryType;
import cje.efiling.niem.core.DocumentType;
import cje.efiling.niem.core.OrganizationType;
import cje.efiling.wrapper.NotifyFilingReviewCompleteRequestType;
import cje.efiling.wrapper.NotifyFilingReviewCompleteResponseType;
import cje.efiling.wrapper.ReviewFilingRequestType;
import cje.efiling.wrapper.ReviewFilingResponseType;
import com.urlint.exchange.util.RequestResult;
import com.urlintegration.exchange.utilities.RequiredDataException;
import com.urlintegration.exchange.xml.BindingException;
import com.urlintegration.io.MessageCache;
import gov.georgia.cje.binding.helper.*;
import gov.georgia.cje.util.BusinessRuleException;
import gov.georgia.cje.util.CommonConfiguration;
import gov.georgia.cje.util.FilingConstants;
import https.docs_oasis_open_org.legalxml_courtfiling.ns.v5_0.reviewfilingcallback.NotifyFilingReviewCompleteMessageType;
import https.docs_oasis_open_org.legalxml_courtfiling.ns.v5_0.wsdl.filingassemblymde.FilingAssemblyMDE;
import https.docs_oasis_open_org.legalxml_courtfiling.ns.v5_0.wsdl.filingreviewmde.FilingReviewMDE;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.NotFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.attachment.ByteDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPFaultException;
import java.io.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Implementation of {@link com.urlint.exchange.testhead.service.ExchangeInterfaceServiceIn} to interwork with the SOAP web service
 * @author jmierwa
 */
@Service
@Import(ClientEndpointConfiguration.class)
public class ExchangeInterfaceServiceInImpl implements ExchangeInterfaceServiceIn {
    private static final Logger LOG = LoggerFactory.getLogger(ExchangeInterfaceServiceInImpl.class);

    @Autowired
    private CommonConfiguration config;

    @Autowired
    private FilingReviewMDE eFilingServiceClient;

    @Autowired
    private FilingAssemblyMDE eFilingResponseServiceClient;

    /**
     * Builds the SOAP message and header and invokes the web service.
     * <p>
     *     Note that the actual message construction makes heavy use of helper objects that partially hide the details involved in
     *     building the relative components.
     * </p>
     * @param XMLTemplateLocation the physical location of the local XML instance on the same machine running the browser
     *                            that will be invoking the service
     * @param leadAttachmentLocation the physical location of the local PDF or other attachment on the same machine running
     *                               the browser that will be added to the XML template
     * @param connectedAttachmentLocation a comma delimited list of the physical locations of the local PDF or other
     *                                    attachment on the same machine running the browser that will be added to the XML template
     * @return {@link com.urlint.exchange.util.ResultCode}
     */
    @Override
    public RequestResult file(String XMLTemplateLocation,
                              String leadAttachmentLocation,
                              String connectedAttachmentLocation) {
        LOG.debug("Rest Service Invoked: ExchangeInterfaceServiceInImpl - XMLTemplateLocation=[{}], leadAttachmentLocation=[{}], " +
                "connectedAttachmentLocation=[{}]", XMLTemplateLocation, leadAttachmentLocation, connectedAttachmentLocation);
        RequestResult result = RequestResult.createSuccess();

        String messageID = UUID.randomUUID().toString();
        ReviewFilingRequestType reqMsg;
        FilingMessageType fmsg;

        try {
            // 1. Open the XML message template and unmarshall it
            InputStream objectStream = new FileInputStream(new File(XMLTemplateLocation));
            reqMsg = ReviewFilingRequestHelper.toObject(objectStream);
            fmsg = reqMsg.getFilingMessage();
//            fmsg = FilingMsgHelper.toObject(objectStream);

            fmsg.setDocumentIdentification(new IdentificationHelper.IdentificationBuilder().id(messageID).build());

            // 2. Open the attachment
            InputStream attachmentStream = new FileInputStream(new File(leadAttachmentLocation));

            // 3. Get the attachment & update the binary object with the attachment
            BinaryHelper bh = FilingLeadDocumentHelper.getBinaryHelper(fmsg.getFilingLeadDocument());
            bh.setBinaryObject(new ByteDataSource(IOUtils.toByteArray(attachmentStream)));

            // 4. Add any connected documents
            if (fmsg.getFilingConnectedDocument() != null &&
                    fmsg.getFilingConnectedDocument().size() > 0 &&
                    StringUtils.isNotBlank(connectedAttachmentLocation)) {
                String[] connectedAttachmentLocationArray = connectedAttachmentLocation.split(",");
                if (connectedAttachmentLocationArray.length != fmsg.getFilingConnectedDocument().size()) {
                    String errMsg = String.format("The number of attachments specified in parameter connectedAttachmentLocation (%s) " +
                                    "does not match the number of FilingConnectedDocument elements (%s) in the XML referenced at %s",
                            connectedAttachmentLocationArray.length, fmsg.getFilingConnectedDocument().size(), leadAttachmentLocation);
                    throw new RequiredDataException(errMsg);
                }
                int index = 0;
                for (DocumentType doc : fmsg.getFilingConnectedDocument()) {
                    InputStream connectedAttachmentStream = new FileInputStream(new File(connectedAttachmentLocationArray[index]));
                    BinaryHelper bhc = DocumentTypeHelper.getBinaryHelper(doc);
                    bhc.setBinaryObject(new ByteDataSource(IOUtils.toByteArray(connectedAttachmentStream)));
                    index++;
                }
            }
            ReviewFilingRequestType reviewMsg = new ReviewFilingRequestType();
            reviewMsg.setFilingMessage(fmsg);

            // 7. Build the header component to the SOAP invocation
            OrganizationType sender = new OrganizationHelper.OrganizationBuilder().category("PROS")
                    .county("197")
                    .build();
            OrganizationType receiver = new OrganizationHelper.OrganizationBuilder().category("CT")
                    .county("197")
                    .build();
            CJEExchangeHeaderType header = CJEHeaderHelper.buildHeader(messageID, messageID, new Date(),
                    EntityHelper.buildEntityOrganization(sender), EntityHelper.buildEntityOrganization(receiver));

            // 8. Invoke the service
            ReviewFilingResponseType resp = this.eFilingServiceClient.reviewFiling(reviewMsg, header);
            if (resp == null) {
                LOG.error("Unexpected Error", "Null Response returned");
                result = RequestResult.createError("Null Response returned");
            }
        } catch (JAXBException e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        } catch (BindingException e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        } catch (IOException ie) {
            LOG.error("Unexpected Error", ie);
            result = RequestResult.createError(ie.getMessage());
        } catch (SOAPFaultException e) {
            LOG.error(e.getFault().toString());
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        } catch (RequiredDataException rex) {
            result = RequestResult.createError(rex.getMessage());
        } catch (Exception e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        }
        return result;
    }

    @Override
    public RequestResult submit(String endpointURL, String messageID) {
        Collection<File> messageDirectory = FileUtils.listFilesAndDirs(new File(config.getRequestCacheDirectory()),
                new NotFileFilter(TrueFileFilter.INSTANCE), DirectoryFileFilter.DIRECTORY);
        RequestResult result = RequestResult.createSuccess();

        MessageCache requestMessageCache = new MessageCache(config.getRequestCacheDirectory());
        List<File> cachePaths = requestMessageCache.findDirectory(messageID, new File(config.getRequestCacheDirectory()));
        if (cachePaths.size() == 0) {
            return RequestResult.createError(String.format("Could not find directory named %s", messageID));
        }
        String cachePath = cachePaths.get(0).getAbsolutePath();
        File messageFile = getMessage(cachePath, ReviewFilingRequestHelper.ELEMENT_NAME);

        ReviewFilingRequestType reqMsg;
        try {
            InputStream objectStream = new FileInputStream(messageFile);
            reqMsg = ReviewFilingRequestHelper.toObject(objectStream);
            FilingMessageType fmsg = reqMsg.getFilingMessage();

            Map<String, File> attachmentFiles = getAttachments(cachePath);

            String docID = fmsg.getFilingLeadDocument().getId().replace("_", "");
            for (Map.Entry<String, File> entry : attachmentFiles.entrySet()) {
                InputStream attachmentStream = new FileInputStream(entry.getValue());

                if (entry.getValue().toString().contains(FilingConstants.LEAD_ATTACHMENT)) {
                    BinaryHelper bh = DocumentTypeHelper.getBinaryHelper(fmsg.getFilingLeadDocument());
                    bh.setBinaryObject(new ByteDataSource(IOUtils.toByteArray(attachmentStream)));
                    attachmentStream.close();

                } else if (entry.getValue().toString().contains(FilingConstants.CONNECTED_ATTACHMENT)) {
                    String[] pathParts = entry.getValue().toString().split("_");
                    for (DocumentType doc : fmsg.getFilingConnectedDocument()) {
                        if (StringUtils.isBlank(doc.getId())) {
                            throw new BusinessRuleException("Document in filing message must contain a valid ID attribute.");
                        }
                        if (doc.getId().equals(pathParts[1])) {
                            BinaryHelper bh = DocumentTypeHelper.getBinaryHelper(doc);
                            bh.setBinaryObject(new ByteDataSource(IOUtils.toByteArray(attachmentStream)));
                            attachmentStream.close();
                        }
                    }
                }
            }
            Map<String, Object> ctxt = ((BindingProvider)this.eFilingServiceClient).getRequestContext();
            ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL);
            LOG.debug("Updated port endpoint to {}", endpointURL);

            File headerFile = getMessage(cachePath, CJEHeaderHelper.ELEMENT_NAME);
            InputStream headerStream = new FileInputStream(headerFile);
            CJEExchangeHeaderType header = CJEHeaderHelper.toObject(headerStream);

            ReviewFilingResponseType resp = this.eFilingServiceClient.reviewFiling(reqMsg, header);
            if (resp == null) {
                LOG.error("Unexpected Error", "Null Response returned");
                result = RequestResult.createError("Null Response returned");
            }

        } catch (FileNotFoundException e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        } catch (BindingException e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        } catch (JAXBException e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        } catch (BusinessRuleException e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        } catch (IOException e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        }
        // Find XML File - ReviewFilingRequest
        return result;
    }

    @Override
    public RequestResult submitResponse(String endpointURL, String messageID) {
        LOG.debug("Entering submitResponse: endpointURL = {}, messageID = {}", endpointURL, messageID);
        Collection<File> messageDirectory = FileUtils.listFilesAndDirs(new File(config.getResponseCacheDirectory()),
                new NotFileFilter(TrueFileFilter.INSTANCE), DirectoryFileFilter.DIRECTORY);
        RequestResult result = RequestResult.createSuccess();

        MessageCache responseMessageCache = new MessageCache(config.getResponseCacheDirectory());
        List<File> cachePaths = responseMessageCache.findDirectory(messageID, new File(config.getResponseCacheDirectory()));
        if (LOG.isDebugEnabled()) {
            StringBuilder sb = new StringBuilder("Attachments: Count = ");
            sb.append(cachePaths.size());
            int index = 0;
            for (File f : cachePaths) {
                sb.append("\n   [");
                sb.append(index);
                sb.append("]: ");
                sb.append(f.getAbsolutePath());
                index++;
            }
            LOG.debug(sb.toString());
        }
        if (cachePaths.size() == 0) {
            return RequestResult.createError(String.format("Could not find directory named %s", messageID));
        }
        String cachePath = cachePaths.get(0).getAbsolutePath();
        File messageFile = getMessage(cachePath, NotifyFilingReviewCompleteRequestHelper.ELEMENT_NAME);

        NotifyFilingReviewCompleteRequestType reqMsg;
        try {
            InputStream objectStream = new FileInputStream(messageFile);
            reqMsg = NotifyFilingReviewCompleteRequestHelper.toObject(objectStream);
            NotifyFilingReviewCompleteMessageType nfrcMsg = reqMsg.getNotifyFilingReviewCompleteMessage();

            Map<String, File> attachmentFiles = getAttachments(cachePath);

            String docID = nfrcMsg.getReviewedLeadDocument().getId().replace("_", "");
            for (Map.Entry<String, File> entry : attachmentFiles.entrySet()) {
                InputStream attachmentStream = new FileInputStream(entry.getValue());

                if (entry.getValue().toString().contains(FilingConstants.LEAD_ATTACHMENT)) {
                    BinaryHelper bh = DocumentTypeHelper.getBinaryHelper(nfrcMsg.getReviewedLeadDocument());
                    bh.setBinaryObjectFromFile(entry.getValue().getAbsolutePath());
                    LOG.debug("Set Lead Document attachment {} to {}", docID, entry.getValue().getAbsolutePath());

                } else if (entry.getValue().toString().contains(FilingConstants.CONNECTED_ATTACHMENT)) {
                    String[] pathParts = entry.getValue().toString().split("_");
                    for (DocumentType doc : nfrcMsg.getReviewedConnectedDocument()) {
                        if (StringUtils.isBlank(doc.getId())) {
                            throw new BusinessRuleException("Document in filing message must contain a valid ID attribute.");
                        }
                        if (doc.getId().equals(pathParts[1])) {
                            BinaryHelper bh = DocumentTypeHelper.getBinaryHelper(doc);
                            bh.setBinaryObjectFromFile(entry.getValue().getAbsolutePath());
                            LOG.debug("Set Connected Document attachment {} to {}", doc.getId(), entry.getValue().getAbsolutePath());
                        }
                    }
                }
            }
            Map<String, Object> ctxt = ((BindingProvider)this.eFilingResponseServiceClient).getRequestContext();
            ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL);
            LOG.debug("Updated port endpoint to {}", endpointURL);

            File headerFile = getMessage(cachePath, CJEHeaderHelper.ELEMENT_NAME);
            InputStream headerStream = new FileInputStream(headerFile);
            CJEExchangeHeaderType header = CJEHeaderHelper.toObject(headerStream);

            NotifyFilingReviewCompleteResponseType resp = this.eFilingResponseServiceClient.notifyFilingReviewComplete(reqMsg, header);
            if (resp == null) {
                LOG.error("Unexpected Error", "Null Response returned");
                result = RequestResult.createError("Null Response returned");
            }

        } catch (FileNotFoundException e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        } catch (BindingException e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        } catch (JAXBException e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        } catch (BusinessRuleException e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        } catch (IOException e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        }
        // Find XML File - ReviewFilingRequest
        return result;
    }

    @Override
    public RequestResult fileResponse(String XMLTemplateLocation,
                              String leadAttachmentLocation,
                              String connectedAttachmentLocation) {
        LOG.debug("Rest Service Invoked: ExchangeInterfaceServiceInImpl/fileResponse - XMLTemplateLocation=[{}], leadAttachmentLocation=[{}], " +
                "connectedAttachmentLocation=[{}]", XMLTemplateLocation, leadAttachmentLocation, connectedAttachmentLocation);
        RequestResult result = RequestResult.createSuccess();

        String messageID = UUID.randomUUID().toString();
        NotifyFilingReviewCompleteRequestType reqMsg;

        try {
            // 1. Open the XML message template and unmarshall it
            InputStream objectStream = new FileInputStream(new File(XMLTemplateLocation));
            reqMsg = NotifyFilingReviewCompleteRequestHelper.toObject(objectStream);
            NotifyFilingReviewCompleteMessageType nfrcMsg = reqMsg.getNotifyFilingReviewCompleteMessage();

            nfrcMsg.setDocumentIdentification(new IdentificationHelper.IdentificationBuilder().id(messageID).build());

            // 2. Open the attachment
            InputStream attachmentStream = new FileInputStream(new File(leadAttachmentLocation));

            // 3. Get the attachment & update the binary object with the attachment
            BinaryHelper bh = DocumentTypeHelper.getBinaryHelper(nfrcMsg.getReviewedLeadDocument());
            bh.setBinaryObject(new ByteDataSource(IOUtils.toByteArray(attachmentStream)));
            attachmentStream.close();

            // 4. Add any connected documents
            if (nfrcMsg.getReviewedConnectedDocument() != null &&
                    nfrcMsg.getReviewedConnectedDocument().size() > 0 &&
                    StringUtils.isNotBlank(connectedAttachmentLocation)) {
                String[] connectedAttachmentLocationArray = connectedAttachmentLocation.split(",");
                if (connectedAttachmentLocationArray.length != nfrcMsg.getReviewedConnectedDocument().size()) {
                    String errMsg = String.format("The number of attachments specified in parameter connectedAttachmentLocation (%s) " +
                                    "does not match the number of FilingConnectedDocument elements (%s) in the XML referenced at %s",
                            connectedAttachmentLocationArray.length, nfrcMsg.getReviewedConnectedDocument().size(), XMLTemplateLocation);
                    throw new RequiredDataException(errMsg);
                }
                int index = 0;
                for (DocumentType doc : nfrcMsg.getReviewedConnectedDocument()) {
                    InputStream connectedAttachmentStream = new FileInputStream(new File(connectedAttachmentLocationArray[index]));
                    BinaryHelper bhc = DocumentTypeHelper.getBinaryHelper(doc);
                    bhc.setBinaryObject(new ByteDataSource(IOUtils.toByteArray(connectedAttachmentStream)));
                    connectedAttachmentStream.close();
                    index++;
                }
            }

            // 7. Build the header component to the SOAP invocation
            OrganizationType sender = new OrganizationHelper.OrganizationBuilder().category("PROS")
                    .county("197")
                    .build();
            OrganizationType receiver = new OrganizationHelper.OrganizationBuilder().category("CT")
                    .county("197")
                    .build();
            CJEExchangeHeaderType header = CJEHeaderHelper.buildHeader(messageID, messageID, new Date(),
                    EntityHelper.buildEntityOrganization(sender), EntityHelper.buildEntityOrganization(receiver));

            // 8. Invoke the service
            NotifyFilingReviewCompleteResponseType resp = this.eFilingResponseServiceClient.notifyFilingReviewComplete(reqMsg, header);
            if (resp == null) {
                LOG.error("Unexpected Error", "Null Response returned");
                result = RequestResult.createError("Null Response returned");
            }
        } catch (JAXBException e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        } catch (BindingException e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        } catch (IOException ie) {
            LOG.error("Unexpected Error", ie);
            result = RequestResult.createError(ie.getMessage());
        } catch (SOAPFaultException e) {
            LOG.error(e.getFault().toString());
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        } catch (RequiredDataException rex) {
            result = RequestResult.createError(rex.getMessage());
        } catch (Exception e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        }
        return result;
    }

    public Map<String, File> getAttachments(String startingDirectory) {
        // initialize File object
        File dir = new File(startingDirectory);
        FileFilter fileFilter = new WildcardFileFilter("*.pdf");
        File[] files = dir.listFiles(fileFilter);
        Arrays.sort(files);
        // Need to remove the duplicates
        Map<String, File> finalAttachments = new HashMap<String, File>();
        for (File attachment : files) {
            String[] parts = attachment.getName().split("_");
            String prefix = parts[0] + "_" + parts[1];
            if (!finalAttachments.containsKey(prefix)) {
                finalAttachments.put(prefix, attachment);
            }
        }
        return finalAttachments;
    }

    public File getMessage(String startingDirectory, String pattern) {
        // initialize File object
        File dir = new File(startingDirectory);
        FileFilter fileFilter = new WildcardFileFilter(pattern + "*.xml");
        File[] messageFiles =  dir.listFiles(fileFilter);
        // Return the most recent one.
        return messageFiles[messageFiles.length-1];
    }

    @Override
    public RequestResult originalFile(String XMLTemplateLocation,
                              String attachmentLocation,
                              String attachmentID,
                              String filingCategory,
                              String documentType) {
        LOG.debug("Rest Service Invoked: ExchangeInterfaceServiceInImpl - XMLTemplateLocation=[{}], attachmentLocation=[{}], " +
                        "attachmentID=[{}], filingCategory=[{}], documentType=[{}]",
                XMLTemplateLocation, attachmentLocation, attachmentID, filingCategory, documentType);
        RequestResult result = RequestResult.createSuccess();

        ReviewFilingRequestType reqMsg;
        FilingMessageType fmsg;

        try {
            // 1. Open the XML message template and unmarshall it
            InputStream objectStream = new FileInputStream(new File(XMLTemplateLocation));
            reqMsg = ReviewFilingRequestHelper.toObject(objectStream);
            fmsg = reqMsg.getFilingMessage();
//            fmsg = FilingMsgHelper.toObject(objectStream);

            // 2. Open the attachment
            InputStream attachmentStream = new FileInputStream(new File(attachmentLocation));

            // 3. Build the attachment and add it to the ECF DocumentRendition element
            String format = "application/pdf";
            BinaryType attachment = new BinaryHelper.BinaryBuilder(attachmentID, format)
                    .binaryObject(new ByteDataSource(IOUtils.toByteArray(attachmentStream)))
                    .binarySize(BigDecimal.valueOf(new File(XMLTemplateLocation).length()))
                    .build();

            DocumentRenditionType rendition = DocumentRenditionHelper.buildDocumentRendition(attachment);

            // 4. Add the rendition to the ECF augmentation
            cje.efiling.ecf.DocumentAugmentationType ecfAug =
                    new ECFDocumentAugmentationHelper.ECFDocumentationBuilder(rendition, "1", documentType)
                            .specialHandlingInstructions("Do this")
                            .build();

            // 5. Create the GA augmentation
            cje.efiling.ga.extension.FilingDocumentAugmentationType gaAug =
                    new GADocumentAugmentationHelper.GADocumentAugmentationBuilder(true).build();

            // 6. Build the lead document
            cje.efiling.niem.core.DocumentType leadDocument = new FilingLeadDocumentHelper.FilingLeadDocumentBuilder(
                    filingCategory,
                    "123456",
                    ecfAug)
                    .gaDocumentationAugmentation(gaAug)
                    .build();

            fmsg.setFilingLeadDocument(leadDocument);
            ReviewFilingRequestType reviewMsg = new ReviewFilingRequestType();
            reviewMsg.setFilingMessage(fmsg);

            // 7. Build the header component to the SOAP invocation
            OrganizationType sender = new OrganizationHelper.OrganizationBuilder().category("PROS")
                    .county("197")
                    .build();
            OrganizationType receiver = new OrganizationHelper.OrganizationBuilder().category("CT")
                    .county("197")
                    .build();
            CJEExchangeHeaderType header = CJEHeaderHelper.buildHeader("1", "123456789", new Date(),
                    EntityHelper.buildEntityOrganization(sender), EntityHelper.buildEntityOrganization(receiver));

            // 8. Invoke the service
            this.eFilingServiceClient.reviewFiling(reviewMsg, header);

        } catch (JAXBException e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        } catch (BindingException e) {
            LOG.error("Unexpected Error", e);
            result = RequestResult.createError(e.getMessage());
        } catch (IOException ie) {
            LOG.error("Unexpected Error", ie);
            result = RequestResult.createError(ie.getMessage());
        }
        return result;
    }
}