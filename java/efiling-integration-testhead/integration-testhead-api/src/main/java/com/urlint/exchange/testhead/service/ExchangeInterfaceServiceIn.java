/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.urlint.exchange.testhead.service;

import com.urlint.exchange.util.RequestResult;

/**
 * Interface to decouple actual invocation of web service from thd body of the rest service.
 * @author jmierwa
 */
public interface ExchangeInterfaceServiceIn {

    /**
     * Builds the SOAP message and header and invokes the web service XMLTemplateLocation the physical location of the local XML instance on the same machine running the browser that will be invoking the service
     * @param XMLTemplateLocation the physical location of the local XML instance on the same machine running the browser that will be invoking the service
     * @param leadAttachmentLocation the physical location of the local PDF or other attachment on the same machine running the browser that will be added to the XML template
     * @param connectedAttachmentLocation a comma delimited list of the physical locations of the local PDF or other attachment on the same machine running the browser that will be added to the XML template
     * @return {@link com.urlint.exchange.util.ResultCode}
     */
    RequestResult file(String XMLTemplateLocation,
                       String leadAttachmentLocation,
                       String connectedAttachmentLocation);
    
    /**
     * Builds the SOAP message and header using the message ID to retrieve an XML 
     * template and attachments from the message cache.
     * @param endpointURL The endpoint URL to which the request will be sent.
     * @param messageID The index into the message cache to access that messages files.
     * @return {@link com.urlint.exchange.util.ResultCode}
     */
    public RequestResult submit(String endpointURL, String messageID);
    /**
     * Builds the SOAP message and header and invokes the web service XMLTemplateLocation the physical location of the local XML instance on the same machine running the browser that will be invoking the service
     * @param XMLTemplateLocation the physical location of the local XML instance on the same machine running the browser that will be invoking the service
     * @param leadAttachmentLocation the physical location of the local PDF or other attachment on the same machine running the browser that will be added to the XML template
     * @param connectedAttachmentLocation a comma delimited list of the physical locations of the local PDF or other attachment on the same machine running the browser that will be added to the XML template
     * @return {@link com.urlint.exchange.util.ResultCode}
     */
    RequestResult fileResponse(String XMLTemplateLocation,
                       String leadAttachmentLocation,
                       String connectedAttachmentLocation);

    /**
     * Builds the SOAP message and header using the message ID to retrieve an XML
     * template and attachments from the message cache.
     * @param endpointURL The endpoint URL to which the request will be sent.
     * @param messageID The index into the message cache to access that messages files.
     * @return {@link com.urlint.exchange.util.ResultCode}
     */
    public RequestResult submitResponse(String endpointURL, String messageID);


    RequestResult originalFile(String XMLTemplateLocation,
                       String attachmentLocation,
                       String attachmentID,
                       String filingCategory,
                       String documentType);
}
