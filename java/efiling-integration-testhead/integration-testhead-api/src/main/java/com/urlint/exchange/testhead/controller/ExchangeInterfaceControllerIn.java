/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.urlint.exchange.testhead.controller;


import com.urlint.exchange.util.RequestResult;
import com.urlint.exchange.testhead.service.ExchangeInterfaceServiceIn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Definition of a Spring Boot Rest client-side service that invokes the SOAP EFiling Review web service.
 *
 * @author jmierwa
 */
@RestController
public class ExchangeInterfaceControllerIn {
    private static final Logger LOG = LoggerFactory.getLogger(ExchangeInterfaceControllerIn.class);

    @Autowired
    private ExchangeInterfaceServiceIn exchangeInterfaceServiceIn;

    /**
     * The rest request that takes the parameters, assembl;es the SOAP message request and invokes the SOAP EFiling Review
     * web service client. Returns a simple JSON response of {@link com.urlint.exchange.util.RequestResult} indicating
     * success or failure.
     * @param XMLTemplateLocation the physical location of the local XML instance on the same machine running the browser
     *                            that will be invoking the service
     * @param leadAttachmentLocation the physical location of the local PDF or other attachment on the same machine running
     *                               the browser that will be added to the XML template
     * @param connectedAttachmentLocation a comma delimited list of the physical locations of the local PDF or other
     *                                    attachment on the same machine running the browser that will be added to the XML template
     * <p>
     *     The primary intent of this service is to test MTOM with the filing request and as such, does not build the SOAP
     *     message from scratch. It expects a partial XML Instance of the filing message to which the binary attachment data
     *     will be added to the lead filing document only.
     *
     *     An example instance invocation is as follows:
     *     {@code http://localhost:8888/file?template=C%3A%5Ctemp%5CGA%5CTest.xml&attachment=C%3A%5Ctemp%5CGA%5CSmallTest.pdf&attachmentID=122345&attachmentCat=Complaint&attachmentDocType=Accusation}
     * </p>
     * @return {@link com.urlint.exchange.util.RequestResult}
     */
    @RequestMapping("/file")
    public RequestResult file(@RequestParam("template") String XMLTemplateLocation,
                              @RequestParam("leadAttachment") String leadAttachmentLocation,
                              @RequestParam("connectedAttachmentList") String connectedAttachmentLocation) {

        RequestResult response = exchangeInterfaceServiceIn.file(XMLTemplateLocation,
                                                                 leadAttachmentLocation,
                                                                 connectedAttachmentLocation);
        LOG.debug("*****************************************************************************************************************");
        LOG.debug("Rest Service Invoked: ExchangeInterfaceControllerIn - XMLTemplateLocation=[{}], leadAttachmentLocation=[{}], " +
                        "connectedAttachmentLocation=[{}]",
                XMLTemplateLocation, leadAttachmentLocation, connectedAttachmentLocation);


        return response;
    }
    
    @RequestMapping("/submit")
    public RequestResult submit(@RequestParam("endpoint") String endpointURL,
                                @RequestParam("messageID") String messageID) {
        RequestResult response = exchangeInterfaceServiceIn.submit(endpointURL, messageID);
        LOG.debug("*****************************************************************************************************************");
        LOG.debug("Rest Service Invoked: ExchangeInterfaceControllerIn - endpoint=[{}], messageID=[{}]",
                endpointURL, messageID);

        return response;
    }

    @RequestMapping("/fileResponse")
    public RequestResult fileResponse(@RequestParam("template") String XMLTemplateLocation,
                              @RequestParam("leadAttachment") String leadAttachmentLocation,
                              @RequestParam("connectedAttachmentList") String connectedAttachmentLocation) {

        RequestResult response = exchangeInterfaceServiceIn.fileResponse(XMLTemplateLocation,
                leadAttachmentLocation,
                connectedAttachmentLocation);
        LOG.debug("*****************************************************************************************************************");
        LOG.debug("Rest Service Invoked: ExchangeInterfaceControllerIn - XMLTemplateLocation=[{}], leadAttachmentLocation=[{}], " +
                        "connectedAttachmentLocation=[{}]",
                XMLTemplateLocation, leadAttachmentLocation, connectedAttachmentLocation);


        return response;
    }

    @RequestMapping("/submitResponse")
    public RequestResult submitResponse(@RequestParam("endpoint") String endpointURL,
                                @RequestParam("messageID") String messageID) {
        RequestResult response = exchangeInterfaceServiceIn.submitResponse(endpointURL, messageID);
        LOG.debug("*****************************************************************************************************************");
        LOG.debug("Rest Service Invoked: ExchangeInterfaceControllerIn - endpoint=[{}], messageID=[{}]",
                endpointURL, messageID);

        return response;
    }

    @RequestMapping("/originalFile")
    public RequestResult file(@RequestParam("template") String XMLTemplateLocation,
                              @RequestParam("attachment") String attachmentLocation,
                              @RequestParam("attachmentID") String attachmentID,
                              @RequestParam("attachmentCat") String filingCategory,
                              @RequestParam("attachmentDocType") String documentType) {

        RequestResult response = exchangeInterfaceServiceIn.originalFile(XMLTemplateLocation,
                attachmentLocation,
                attachmentID,
                filingCategory,
                documentType);
        LOG.debug("*****************************************************************************************************************");
        LOG.debug("Rest Service Invoked: ExchangeInterfaceControllerIn - XMLTemplateLocation=[{}], attachmentLocation=[{}], " +
                        "attachmentID=[{}], filingCategory=[{}], documentType=[{}]",
                XMLTemplateLocation, attachmentLocation, attachmentID, filingCategory, documentType);


        return response;
    }
}
