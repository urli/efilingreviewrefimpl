/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.urlint.exchange.testhead.service;

import gov.georgia.cje.util.CommonConfiguration;
import https.docs_oasis_open_org.legalxml_courtfiling.ns.v5_0.wsdl.filingassemblymde.FilingAssemblyMDE;
import https.docs_oasis_open_org.legalxml_courtfiling.ns.v5_0.wsdl.filingreviewmde.FilingReviewMDE;
import org.apache.cxf.feature.LoggingFeature;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Configuration class to instantiate the SOAP EFiling Review Web Service Client.
 * Created by JMierwa on 9/27/2017.
 */
@Configuration
@Import(CommonConfiguration.class)
public class ClientEndpointConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(ClientEndpointConfiguration.class);

    @Value("${client.endpoint.address}")
    private String address;

    @Value("${response.client.endpoint.address}")
    private String responseClientAddress;

    @Autowired
    private CommonConfiguration config;

    /**
     * Instantiate the EFiling Client and enable MTOM if configured in the application.properties file.
     * @returns proxy for {@link https.docs_oasis_open_org.legalxml_courtfiling.ns.v5_0.wsdl.filingreviewmde.FilingReviewMDE}
     */
    @Bean(name = "eFilingServiceClient")
    public FilingReviewMDE eFilingServiceClient() {
        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
        jaxWsProxyFactoryBean.setServiceClass(FilingReviewMDE.class);
        jaxWsProxyFactoryBean.setAddress(address);

        jaxWsProxyFactoryBean.setFeatures(Collections.singletonList(new LoggingFeature()));
        LOG.info("Client default address set set to {}", address);

        if (config.isMTOMEnabled()) {
            if (jaxWsProxyFactoryBean.getProperties() != null) {
                jaxWsProxyFactoryBean.getProperties().put(CommonConfiguration.MTOM_ENABLED, config.isMTOMEnabled());
            } else {
                Map<String, Object> clientProperties = new HashMap<String, Object>();
                clientProperties.put(CommonConfiguration.MTOM_ENABLED, config.isMTOMEnabled());
                jaxWsProxyFactoryBean.setProperties(clientProperties);
            }
            LOG.info("MTOM has been enabled for this service");
        } else {
            LOG.info("MTOM NOT enabled for this service");
        }
        return (FilingReviewMDE) jaxWsProxyFactoryBean.create();
    }

    /**
     * Instantiate the EFiling Client and enable MTOM if configured in the application.properties file.
     * @returns proxy for {@link https.docs_oasis_open_org.legalxml_courtfiling.ns.v5_0.wsdl.filingreviewmde.FilingReviewMDE}
     */
    @Bean(name = "eFilingResponseServiceClient")
    public FilingAssemblyMDE eFilingResponseServiceClient() {
        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
        jaxWsProxyFactoryBean.setServiceClass(FilingAssemblyMDE.class);
        jaxWsProxyFactoryBean.setAddress(responseClientAddress);

        jaxWsProxyFactoryBean.setFeatures(Collections.singletonList(new LoggingFeature()));
        LOG.info("Client default address set set to {}", responseClientAddress);

        if (config.isMTOMEnabled()) {
            if (jaxWsProxyFactoryBean.getProperties() != null) {
                jaxWsProxyFactoryBean.getProperties().put(CommonConfiguration.MTOM_ENABLED, config.isMTOMEnabled());
            } else {
                Map<String, Object> clientProperties = new HashMap<String, Object>();
                clientProperties.put(CommonConfiguration.MTOM_ENABLED, config.isMTOMEnabled());
                jaxWsProxyFactoryBean.setProperties(clientProperties);
            }
            LOG.info("MTOM has been enabled for this service");
        } else {
            LOG.info("MTOM NOT enabled for this service");
        }
        return (FilingAssemblyMDE) jaxWsProxyFactoryBean.create();
    }
}
