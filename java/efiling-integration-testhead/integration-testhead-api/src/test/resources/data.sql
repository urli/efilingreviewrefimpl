/* 
 * Copyright (c) 2014 URL Integration for the McHenry County Circuit Clerk's
 * Office. Not for reproduction, dissemination or distribution without prior
 * written consent of URL Integration or the McHenry County Circuit Clerk.
 */
/**
 * Author:  dmiranda
 * Created: Mar 2, 2017
 */

INSERT INTO central.cjis_t_msg(t_msg_id, msg_id, exchange_id, from_agency_id, msg_status, accepted, user_notes, retries, next_retry_dttm, checksum, date_created, date_modified) VALUES (3, '65467', 2, 'Agency 1', 'NEW', 'false', 'User notes 11111', 0, null, null, current_timestamp, current_timestamp);
INSERT INTO central.cjis_t_exchange(exchange_id, product_name, exchange_name, direction, out_url, date_created, date_modified, send_async) VALUES (2, 'H2 Zuercher', 'H2 Zuercher', 'IN', 'urlintegration.com', current_timestamp, current_timestamp, TRUE);
INSERT INTO central.cjis_t_msg_status(msg_status_id, t_msg_id, is_outgoing, description, transaction_status, error_code, error_msg, key_data, agency_id, additional_info, sent_status, retries, next_retry_dttm, date_created, date_modified) VALUES (1001, 2, TRUE, 'H2 TEST', 'P', 'E000', null, null, '5', 'H2 info', 'RTS', '0', null, current_timestamp, current_timestamp);
INSERT INTO central.cjis_t_msg_status(msg_status_id, t_msg_id, is_outgoing, description, transaction_status, error_code, error_msg, key_data, agency_id, additional_info, sent_status, retries, next_retry_dttm, date_created, date_modified) VALUES (1002, 2, TRUE, 'H2 TEST 2', 'P', 'E000', null, null, '5', 'H2 info 2', 'RTS', '0', null, current_timestamp, current_timestamp);