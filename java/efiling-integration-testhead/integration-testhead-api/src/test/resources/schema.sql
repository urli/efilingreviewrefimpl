/* 
 * Copyright (c) 2014 URL Integration for the McHenry County Circuit Clerk's
 * Office. Not for reproduction, dissemination or distribution without prior
 * written consent of URL Integration or the McHenry County Circuit Clerk.
 */
/**
 * Author:  dmiranda
 * Created: Mar 2, 2017
 */
DROP SCHEMA IF EXISTS central;
CREATE SCHEMA central;
SET SCHEMA central;

DROP TABLE IF EXISTS central.cjis_t_exchange CASCADE;
DROP TABLE IF EXISTS central.cjis_t_msg CASCADE;
DROP TABLE IF EXISTS central.cjis_t_msg_metadata CASCADE;
DROP TABLE IF EXISTS central.cjis_t_msg_status CASCADE;
DROP TABLE IF EXISTS central.cjis_t_msg_xmldata CASCADE;

DROP SEQUENCE IF EXISTS central.cjis_exchange_id_seq;
DROP SEQUENCE IF EXISTS central.cjis_msg_status_seq;
DROP SEQUENCE IF EXISTS central.cjis_t_msg_id_seq;
DROP SEQUENCE IF EXISTS central.cjis_t_msg_meta_id_seq;
DROP SEQUENCE IF EXISTS central.cjis_t_xml_msg_id_seq;

CREATE SEQUENCE central.cjis_exchange_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 99 CACHE 1;
CREATE SEQUENCE central.cjis_msg_status_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 99999999 CACHE 1;
CREATE SEQUENCE central.cjis_t_msg_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9999999999 CACHE 1;
CREATE SEQUENCE central.cjis_t_msg_meta_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 99999999 CACHE 1;
CREATE SEQUENCE central.cjis_t_xml_msg_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 999999999 CACHE 1;

CREATE TABLE central.cjis_t_exchange (exchange_id bigint NOT NULL, product_name character varying(50) NOT NULL, exchange_name character varying(50) NOT NULL, direction character varying(3) NOT NULL, out_url character varying(500), date_created timestamp(6) NOT NULL, date_modified timestamp(6) NOT NULL, send_async boolean NOT NULL, CONSTRAINT pk_cjis_t_exchange PRIMARY KEY (exchange_id));
CREATE TABLE central.cjis_t_msg (t_msg_id bigint NOT NULL, msg_id character varying(100)  NOT NULL, exchange_id BIGINT NOT NULL, from_agency_id character varying(50) , msg_status character varying(5)  NOT NULL, accepted boolean default false, user_notes character varying(500) , retries int NOT NULL DEFAULT 0, next_retry_dttm timestamp(6), checksum character varying(50) , date_created timestamp(6) NOT NULL, date_modified timestamp(6) NOT NULL, CONSTRAINT pk_cjis_t_msg PRIMARY KEY (t_msg_id));
CREATE TABLE central.cjis_t_msg_metadata (t_msg_metadata bigint NOT NULL, t_msg_id bigint NOT NULL, metadata_type character varying(100), metadata_value character varying(500),  CONSTRAINT pk_cjis_t_msg_metadata PRIMARY KEY (t_msg_metadata));
CREATE TABLE central.cjis_t_msg_status (msg_status_id bigint NOT NULL, t_msg_id bigint NOT NULL, is_outgoing boolean not null, description character varying(100), transaction_status character(1)  NOT NULL, error_code character varying(50), error_msg text, key_data character varying(500), agency_id character varying(50), additional_info character varying(500), sent_status character varying(5), retries int NOT NULL DEFAULT 0, next_retry_dttm timestamp(6), date_created timestamp(6) NOT NULL, date_modified timestamp(6) NOT NULL, CONSTRAINT pk_cjis_t_msg_status PRIMARY KEY (msg_status_id));
CREATE TABLE central.cjis_t_msg_xmldata (t_msg_xmldata_id bigint NOT NULL, t_msg_id bigint NOT NULL, xmldata text, xmldata_length int, remarks character varying(500), CONSTRAINT pk_cjis_t_msg_xmldata PRIMARY KEY (t_msg_xmldata_id));