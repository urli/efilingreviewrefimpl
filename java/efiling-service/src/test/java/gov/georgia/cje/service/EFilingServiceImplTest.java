/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.georgia.cje.service;

import cje.efiling.wrapper.ReviewFilingRequestType;
import gov.georgia.cje.binding.helper.ReviewFilingRequestHelper;
import java.io.InputStream;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author JMierwa
 */
public class EFilingServiceImplTest {

    /**
     * Test of validateRequest method, of class EFilingServiceImpl.
     * @throws java.lang.Exception
     */
    @Test
    public void testValidateRequest() throws Exception {
        System.out.println("validateRequest");
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("TestRFR-AccusationInitial.xml");
        ReviewFilingRequestType msgBody = ReviewFilingRequestHelper.toObject(is);
        EFilingServiceImpl instance = new EFilingServiceImpl("Dummy");
        int expResult = 0;
        int result = instance.validateRequest(msgBody, is).size();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of validateRequest method, of class EFilingServiceImpl.
     * @throws java.lang.Exception
     */
    @Test
    public void testClearSchematronCache() throws Exception {
        System.out.println("clearSchematronCache");
        EFilingServiceImpl instance = new EFilingServiceImpl("Dummy");
        boolean expResult = true;
        boolean result = instance.clearSchematronCache();
        assertEquals(expResult, result);
    }
}
