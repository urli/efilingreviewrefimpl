/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package gov.georgia.cje.service;

import cje.common.header.CJEExchangeHeaderType;
import cje.efiling.filing.FilingMessageType;
import cje.efiling.niem.core.DocumentType;
import cje.efiling.wrapper.ReviewFilingRequestType;
import cje.efiling.wrapper.ReviewFilingResponseType;
import com.urlint.common.xml.ErrorListener;
import com.urlintegration.exchange.utilities.ProcessingResult;
import com.urlintegration.exchange.xml.BindingException;
import com.urlintegration.exchange.xml.ValidationError;
import com.urlintegration.exchange.xml.ValidationErrorEnum;
import com.urlintegration.io.MessageCache;
import edu.ucar.ral.crux.SchematronValidator;
import edu.ucar.ral.crux.ValidationException;
import gov.georgia.cje.binding.helper.*;
import gov.georgia.cje.util.FilingConstants;
import gov.georgia.cje.util.MTOMException;
import https.docs_oasis_open_org.legalxml_courtfiling.ns.v5_0.wsdl.filingreviewmde.FilingReviewMDE;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Implementation class for the EFiling Review Service interface generated from the WSDL.
 * @author jmierwa
 */
public class EFilingServiceImpl implements FilingReviewMDE {

    private static final Logger LOG = LoggerFactory.getLogger(EFilingServiceImpl.class.getName());
    private static final String SCHEMATRON_RESOURCE_PATH = "schematron/EFilingBasic.sch";
            
    private final SchematronValidator schematronValidator = new SchematronValidator();
    private final File schematronFile = new File(SCHEMATRON_RESOURCE_PATH);
    private MessageCache messageCache;

    /**
     * Constructor for EFilingServiceImpl
     * @param cacheLocation A {@link java.lang.String value of the absolute path to the location where incoming messages are stored }.
     */
    public EFilingServiceImpl (String cacheLocation) {
        this.messageCache = new MessageCache(cacheLocation);
        this.clearSchematronCache();
    }

    @Override
    public ReviewFilingResponseType reviewFiling(ReviewFilingRequestType body, CJEExchangeHeaderType cjeExchangeHeader) {
        LOG.info("Executing operation reviewFiling");
        ReviewFilingResponseType response = new ReviewFilingResponseType();

        Map<String, byte[]> attachments = new HashMap<String, byte[]>();
        ProcessingResult result = this.cacheAttachments(body.getFilingMessage(), cjeExchangeHeader.getMessageID().getValue(), attachments);
        if (result.successful()) {
            try {
                ReviewFilingRequestHelper rfrh = new ReviewFilingRequestHelper();
                InputStream messageStream = rfrh.toInputStream(body);
                List<ValidationError> errors = validateRequest(body, messageStream);
                if (errors.isEmpty()) {
                    result = storeMessage(messageStream, cjeExchangeHeader, attachments);
                    if (result.successful()) {
                        response.setMessageStatus(MessageStatusHelper.createSuccessMessageStatusHelper(cjeExchangeHeader.getMessageID().getValue()));
                    } else {
                        response.setMessageStatus(MessageStatusHelper.createMessageStatus(cjeExchangeHeader.getMessageID().getValue(), result));
                    }
                } else {
                    response.setMessageStatus(MessageStatusHelper.createXmlErrorMessageStatusHelper(cjeExchangeHeader.getMessageID().getValue(), errors));
                }
            } catch (BindingException ex) {
                response.setMessageStatus(MessageStatusHelper.createXmlErrorMessageStatusHelper(cjeExchangeHeader.getMessageID().getValue(), ex.getErrors()));
            } catch (JAXBException ex) {
                List<ValidationError> errorList = new ArrayList<ValidationError>();
                errorList.add(new ValidationError(ValidationErrorEnum.UNSPECIFIED, ex.getMessage(), "MessageBody", null, null));
                response.setMessageStatus(MessageStatusHelper.createXmlErrorMessageStatusHelper(cjeExchangeHeader.getMessageID().getValue(), errorList));
            }
        } else {
            response.setMessageStatus(MessageStatusHelper.createMessageStatus(cjeExchangeHeader.getMessageID().getValue(), result));
        }
        return response;
    }
    
    public List<ValidationError> validateRequest(ReviewFilingRequestType msgBody, InputStream messageStream) {
        List<ValidationError> errors = new ArrayList<ValidationError>();
        ReviewFilingRequestHelper rfrh = new ReviewFilingRequestHelper();

        try {
            rfrh.validate(msgBody);
        } catch (BindingException e) {
            errors.addAll(e.getErrors());
        }
        LOG.info( String.format( "Validating incoming request against Schematron rules (%s)", schematronFile.toString() ) );
        ErrorListener validationErrors = null;
        try {
            validationErrors = schematronValidator.validate2(messageStream, schematronFile);
        } catch (ValidationException ex) {
            errors.addAll(ex.getValidationErrors());
        } catch (IOException ex) {
            List<ValidationError> errorList = new ArrayList<ValidationError>();
            errorList.add(new ValidationError(ValidationErrorEnum.UNSPECIFIED, ex.getMessage(), "MessageBody", null, null));
        }
        if (validationErrors != null && validationErrors.containsErrors()) {
            errors.addAll( validationErrors.getErrors() );
        }
        return errors;
    }

    public final ProcessingResult storeMessage(InputStream messageStream, CJEExchangeHeaderType cjeExchangeHeader, Map<String, byte[]> attachments) {

        ProcessingResult result = ProcessingResult.createSuccessResult();
        try {
            String outFileName = buildFileName(cjeExchangeHeader.getMessageID().getValue(), ReviewFilingRequestHelper.ELEMENT_NAME, "xml");
            messageStream.reset();
            FileUtils.copyInputStreamToFile(messageStream, new File(outFileName));
            LOG.debug("Body saved to {}.", outFileName);

            CJEHeaderHelper headerHelper = new CJEHeaderHelper(cjeExchangeHeader);
            String headerFileName = buildFileName(cjeExchangeHeader.getMessageID().getValue(), CJEHeaderHelper.ELEMENT_NAME, "xml");
            InputStream headerStream = headerHelper.toInputStream(cjeExchangeHeader);
            FileUtils.copyInputStreamToFile(headerStream, new File(headerFileName));
            LOG.debug("Header saved to {}.", headerFileName);

            for (Map.Entry<String, byte[]> entry : attachments.entrySet()) {
                FileUtils.writeByteArrayToFile(new File(entry.getKey()), entry.getValue());
                LOG.debug("Attachment saved to {}.", entry.getKey());
            }
        } catch (IOException iex) {
            result = ProcessingResult.createFailResult("Unexpected error trying to save messages.", "", ValidationErrorEnum.UNSPECIFIED.toString(), iex);
        } catch (BindingException bex) {
            result = ProcessingResult.createFailResult("Unexpected error trying to save header.", "", ValidationErrorEnum.UNSPECIFIED.toString(), bex);
        } catch (JAXBException jex) {
            result = ProcessingResult.createFailResult("Unexpected error trying to save header.", "", ValidationErrorEnum.UNSPECIFIED.toString(), jex);
        }
        return result;
    }

    public ProcessingResult processAttachment(DocumentType filingDocument, String attachmentQualifier, String messageID, Map<String, byte[]> attachmentMap) {
        ProcessingResult result = ProcessingResult.createSuccessResult();
        BinaryHelper attachment = DocumentTypeHelper.getBinaryHelper(filingDocument);
        String outFileName;
        if (attachment != null) {
            String docID = filingDocument.getId().replace("_", "");
            if (StringUtils.isNotBlank(docID)) {
                outFileName = buildFileName(messageID, attachmentQualifier +"_" + docID, "pdf");
            } else {
                outFileName = buildFileName(messageID, "LeadAttachment_LD001", "pdf");
                LOG.error("Missing document ID should have been captured in schematron. Correct the schematron business rules.");
            }
            try {
                attachmentMap.put(outFileName, IOUtils.toByteArray(attachment.getBinaryObject()));
                LOG.debug("Attachment cached as {}", outFileName);
            } catch (MTOMException e) {
                result = ProcessingResult.createFailResult(e.getMessage(),"", ValidationErrorEnum.MTOM_ERROR.toString(), e);
            } catch (IOException e) {
                result = ProcessingResult.createFailResult(e.getMessage(),"", ValidationErrorEnum.MTOM_ERROR.toString(), e);
            }
        }
        else {
            result = ProcessingResult.createFailResult("No attachment found for received message", "", ValidationErrorEnum.UNSPECIFIED.toString(), null);
        }
        return result;
    }

    public ProcessingResult cacheAttachments(FilingMessageType filing, String messageID, Map<String, byte[]> attachmentMap) {
        ProcessingResult result;

        // Get lead document attachment
        result = processAttachment(filing.getFilingLeadDocument(), FilingConstants.LEAD_ATTACHMENT, messageID, attachmentMap);

        if (result.successful()) {
            for (DocumentType connectedDoc : filing.getFilingConnectedDocument()) {
                result = processAttachment(connectedDoc, FilingConstants.CONNECTED_ATTACHMENT, messageID, attachmentMap);
                if (result.failed()) {
                    break;
                }
            }
        }
        return result;
    }

    private String buildFileName(String messageID, String genericFileName, String fileExtension) {
        SimpleDateFormat format = new SimpleDateFormat("yMMdd_HHmmss");
        String timeStamp = format.format(Calendar.getInstance().getTime());
        String outFile = this.messageCache.getDirectory(messageID) + File.separator + String.format("%s_%s.%s", genericFileName, timeStamp, fileExtension);
        return outFile;
    }

    public final boolean clearSchematronCache() {
        try {
            String[] parts = SCHEMATRON_RESOURCE_PATH.split("/");
            File schematronCacheDirectory = schematronValidator.getCacheDir(new File(SCHEMATRON_RESOURCE_PATH));
            if (schematronCacheDirectory.exists()) {
                this.schematronValidator.clearCache(schematronFile);
                LOG.debug("Cleared schematron cache of preprocessed transforms");
                return true;
            } else {
                LOG.debug("Schematron cache did not exist so did not need to clean it");
                return false;
            }
        } catch (IOException ex) {
            LOG.warn("Did not clear schematron cache for '{}'", schematronFile.toString(), ex);
            return false;
        }
    }
}