/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gov.georgia.cje.service;


import javax.xml.ws.Endpoint;
import javax.xml.ws.soap.SOAPBinding;

import gov.georgia.cje.util.CommonConfiguration;
import org.apache.cxf.Bus;
import org.apache.cxf.feature.LoggingFeature;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.jaxws.binding.soap.JaxWsSoapBindingConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Class to manage the instantiation of the EFiling web service endpoint using Spring NoXML Configuration
 * Created by JMierwa on 9/14/2017.
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan
@Import(CommonConfiguration.class)
public class ProviderEndpointConfiguration {

    @Autowired
    private Bus bus;

    @Autowired
    private CommonConfiguration config;

    /**
     * Returns the {@link org.apache.cxf.jaxws.EndpointImpl} for the web service.
     * <p>
     *     Makes use of {@link gov.georgia.cje.util.CommonConfiguration} to set up MTOM for the service.
     * </p>
     * @return {@link org.apache.cxf.jaxws.EndpointImpl} for the web service
     */
    @Bean(name = "eFilingServiceBean")
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(
                bus,
                new EFilingServiceImpl(config.getAttachmentDirectory()),
                "http://www.w3.org/2003/05/soap/bindings/HTTP/" );  // need this to force the service to use SOAP 1.2. Defaults to SOAP 1.1 and can't use call to SOAPVersion.SOAP_12.httpBindingId because compile fails.

        JaxWsSoapBindingConfiguration j = new JaxWsSoapBindingConfiguration(endpoint.getServiceFactory());
        SOAPBinding binding = (SOAPBinding)endpoint.getBinding();
        if (config.isMTOMEnabled()) {
            binding.setMTOMEnabled(config.isMTOMEnabled());
        }
        JaxWsSoapBindingConfiguration bc = (JaxWsSoapBindingConfiguration)(endpoint.getBindingConfig());
        endpoint.publish("/EFilingService");

        LoggingFeature logFeature = new LoggingFeature();
        logFeature.setPrettyLogging(true);
        logFeature.initialize(bus);
        endpoint.getFeatures().add(logFeature);

        return endpoint;
    }
}
