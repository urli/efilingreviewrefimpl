package gov.georgia.cje.response;

import cje.common.header.CJEExchangeHeaderType;
import cje.efiling.filing.FilingMessageType;
import cje.efiling.niem.core.DocumentType;
import cje.efiling.wrapper.NotifyFilingReviewCompleteRequestType;
import cje.efiling.wrapper.NotifyFilingReviewCompleteResponseType;
import com.urlint.common.xml.ErrorListener;
import com.urlintegration.exchange.utilities.ProcessingResult;
import com.urlintegration.exchange.xml.BindingException;
import com.urlintegration.exchange.xml.ValidationError;
import com.urlintegration.exchange.xml.ValidationErrorEnum;
import com.urlintegration.io.MessageCache;
import edu.ucar.ral.crux.SchematronValidator;
import edu.ucar.ral.crux.ValidationException;
import gov.georgia.cje.binding.helper.*;
import gov.georgia.cje.util.FilingConstants;
import gov.georgia.cje.util.MTOMException;
import https.docs_oasis_open_org.legalxml_courtfiling.ns.v5_0.reviewfilingcallback.NotifyFilingReviewCompleteMessageType;
import https.docs_oasis_open_org.legalxml_courtfiling.ns.v5_0.wsdl.filingassemblymde.FilingAssemblyMDE;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Response service to complete the act of filing by notifying the user of the result.
 */
public class EFilingServiceResponseImpl implements FilingAssemblyMDE {

    private static final Logger LOG = LoggerFactory.getLogger(EFilingServiceResponseImpl.class.getName());
    private static final String SCHEMATRON_RESOURCE_PATH = "schematron/EFilingBasic.sch";

    private final SchematronValidator schematronValidator = new SchematronValidator();
    private final File schematronFile = new File(SCHEMATRON_RESOURCE_PATH);
    private MessageCache messageCache;

    /**
     * Constructor for EFilingServiceImpl
     * @param cacheLocation A {@link java.lang.String value of the absolute path to the location where incoming messages are stored }.
     */
    public EFilingServiceResponseImpl (String cacheLocation) {
        this.messageCache = new MessageCache(cacheLocation);
        this.clearSchematronCache();
    }
    @Override
    public NotifyFilingReviewCompleteResponseType notifyFilingReviewComplete(NotifyFilingReviewCompleteRequestType body, CJEExchangeHeaderType cjeExchangeHeader) {
        LOG.info("Executing operation notifyFilingReviewComplete");

        NotifyFilingReviewCompleteResponseType response = new NotifyFilingReviewCompleteResponseType();

        Map<String, byte[]> attachments = new HashMap<String, byte[]>();
        ProcessingResult result = this.cacheAttachments(body.getNotifyFilingReviewCompleteMessage(), cjeExchangeHeader.getMessageID().getValue(), attachments);
        if (result.successful()) {
            try {
                NotifyFilingReviewCompleteRequestHelper nfrch = new NotifyFilingReviewCompleteRequestHelper();
                InputStream messageStream = nfrch.toInputStream(body);
                List<ValidationError> errors = validateRequest(body, messageStream);
                if (errors.isEmpty()) {
                    result = storeMessage(messageStream, cjeExchangeHeader, attachments);
                    if (result.successful()) {
                        response.setMessageStatus(MessageStatusHelper.createSuccessMessageStatusHelper(cjeExchangeHeader.getMessageID().getValue()));
                    } else {
                        response.setMessageStatus(MessageStatusHelper.createMessageStatus(cjeExchangeHeader.getMessageID().getValue(), result));
                    }
                } else {
                    response.setMessageStatus(MessageStatusHelper.createXmlErrorMessageStatusHelper(cjeExchangeHeader.getMessageID().getValue(), errors));
                }
            } catch (BindingException ex) {
                response.setMessageStatus(MessageStatusHelper.createXmlErrorMessageStatusHelper(cjeExchangeHeader.getMessageID().getValue(), ex.getErrors()));
            } catch (JAXBException ex) {
                List<ValidationError> errorList = new ArrayList<ValidationError>();
                errorList.add(new ValidationError(ValidationErrorEnum.UNSPECIFIED, ex.getMessage(), "MessageBody", null, null));
                response.setMessageStatus(MessageStatusHelper.createXmlErrorMessageStatusHelper(cjeExchangeHeader.getMessageID().getValue(), errorList));
            }
        } else {
            response.setMessageStatus(MessageStatusHelper.createMessageStatus(cjeExchangeHeader.getMessageID().getValue(), result));
        }
        return response;
}

    public List<ValidationError> validateRequest(NotifyFilingReviewCompleteRequestType msgBody, InputStream messageStream) {
        List<ValidationError> errors = new ArrayList<ValidationError>();
        NotifyFilingReviewCompleteRequestHelper nfrch = new NotifyFilingReviewCompleteRequestHelper();

        try {
            nfrch.validate(msgBody);
        } catch (BindingException e) {
            errors.addAll(e.getErrors());
        }
        LOG.info( String.format( "Validating incoming request against Schematron rules (%s)", schematronFile.toString() ) );
        ErrorListener validationErrors = null;
        try {
            validationErrors = schematronValidator.validate2(messageStream, schematronFile);
        } catch (ValidationException ex) {
            errors.addAll(ex.getValidationErrors());
        } catch (IOException ex) {
            List<ValidationError> errorList = new ArrayList<ValidationError>();
            errorList.add(new ValidationError(ValidationErrorEnum.UNSPECIFIED, ex.getMessage(), "MessageBody", null, null));
        }
        if (validationErrors != null && validationErrors.containsErrors()) {
            errors.addAll( validationErrors.getErrors() );
        }
        return errors;
    }

    public final ProcessingResult storeMessage(InputStream messageStream, CJEExchangeHeaderType cjeExchangeHeader, Map<String, byte[]> attachments) {
        ProcessingResult result = ProcessingResult.createSuccessResult();
        try {
            String outFileName = buildFileName(cjeExchangeHeader.getMessageID().getValue(), NotifyFilingReviewCompleteRequestHelper.ELEMENT_NAME, "xml");
            messageStream.reset();
            FileUtils.copyInputStreamToFile(messageStream, new File(outFileName));
            LOG.debug("Body saved to {}.", outFileName);

            CJEHeaderHelper headerHelper = new CJEHeaderHelper(cjeExchangeHeader);
            String headerFileName = buildFileName(cjeExchangeHeader.getMessageID().getValue(), CJEHeaderHelper.ELEMENT_NAME, "xml");
            InputStream headerStream = headerHelper.toInputStream(cjeExchangeHeader);
            FileUtils.copyInputStreamToFile(headerStream, new File(headerFileName));
            LOG.debug("Header saved to {}.", headerFileName);

            for (Map.Entry<String, byte[]> entry : attachments.entrySet()) {
                File f = new File(entry.getKey());
                FileUtils.writeByteArrayToFile(new File(entry.getKey()), entry.getValue());
                LOG.debug("Attachment saved to {}.", entry.getKey());
            }
        }
        catch (IOException iex) {
            result = ProcessingResult.createFailResult("Unexpected error trying to save messages.", "", ValidationErrorEnum.UNSPECIFIED.toString(), iex);
        } catch (BindingException bex) {
            result = ProcessingResult.createFailResult("Unexpected error trying to save header.", "", ValidationErrorEnum.UNSPECIFIED.toString(), bex);
        } catch (JAXBException jex) {
            result = ProcessingResult.createFailResult("Unexpected error trying to save header.", "", ValidationErrorEnum.UNSPECIFIED.toString(), jex);
        }
        return result;
    }

    public ProcessingResult processAttachment(DocumentType filingDocument, String attachmentQualifier, String messageID, Map<String, byte[]> attachmentMap) {
        ProcessingResult result = ProcessingResult.createSuccessResult();
        BinaryHelper attachment = DocumentTypeHelper.getBinaryHelper(filingDocument);
        String outFileName;
        if (attachment != null) {
            String docID = filingDocument.getId().replace("_", "");
            if (StringUtils.isNotBlank(docID)) {
                outFileName = buildFileName(messageID, attachmentQualifier +"_" + docID, "pdf");
            } else {
                outFileName = buildFileName(messageID, "LeadAttachment_LD001", "pdf");
                LOG.error("Missing document ID should have been captured in schematron. Correct the schematron business rules.");
            }
            try {
                attachmentMap.put(outFileName, IOUtils.toByteArray(attachment.getBinaryObject()));
                LOG.debug("Attachment cached as {}", outFileName);
            } catch (MTOMException e) {
                result = ProcessingResult.createFailResult(e.getMessage(),"", ValidationErrorEnum.MTOM_ERROR.toString(), e);
            } catch (IOException e) {
                result = ProcessingResult.createFailResult(e.getMessage(),"", ValidationErrorEnum.MTOM_ERROR.toString(), e);
            }
        }
        else {
            result = ProcessingResult.createFailResult("No attachment found for received message", "", ValidationErrorEnum.UNSPECIFIED.toString(), null);
        }
        return result;
    }

    public ProcessingResult cacheAttachments(NotifyFilingReviewCompleteMessageType reviewComplete, String messageID, Map<String, byte[]> attachmentMap) {
        ProcessingResult result;

        // Get lead document attachment
        result = processAttachment(reviewComplete.getReviewedLeadDocument(), FilingConstants.LEAD_ATTACHMENT, messageID, attachmentMap);

        if (result.successful()) {
            for (DocumentType connectedDoc : reviewComplete.getReviewedConnectedDocument()) {
                result = processAttachment(connectedDoc, FilingConstants.CONNECTED_ATTACHMENT, messageID, attachmentMap);
                if (result.failed()) {
                    break;
                }
            }
        }
        return result;
    }

    private String buildFileName(String messageID, String genericFileName, String fileExtension) {
        SimpleDateFormat format = new SimpleDateFormat("yMMdd_HHmmss");
        String timeStamp = format.format(Calendar.getInstance().getTime());
        String outFile = this.messageCache.getDirectory(messageID) + File.separator + String.format("%s_%s.%s", genericFileName, timeStamp, fileExtension);
        return outFile;
    }

    public final boolean clearSchematronCache() {
        try {
            String[] parts = SCHEMATRON_RESOURCE_PATH.split("/");
            File schematronCacheDirectory = schematronValidator.getCacheDir(new File(SCHEMATRON_RESOURCE_PATH));
            if (schematronCacheDirectory.exists()) {
                this.schematronValidator.clearCache(schematronFile);
                LOG.debug("Cleared schematron cache of preprocessed transforms");
                return true;
            } else {
                LOG.debug("Schematron cache did not exist so did not need to clean it");
                return false;
            }
        } catch (IOException ex) {
            LOG.warn("Did not clear schematron cache for '{}'", schematronFile.toString(), ex);
            return false;
        }
    }
}