<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <title>Test Schematron Illustrating Transform Bug</title>
    
    <ns prefix="x" uri="http://www.w3.org/TR/REC-html40"/>
    <ns prefix="filing" uri="https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/filing"/>
    <ns prefix="nc" uri="http://release.niem.gov/niem/niem-core/3.0/"/>
    
    <include href="ExternalTestPattern.sch"/>
    
    <pattern id="ecf">
        <rule context="/filing:FilingMessage">
            <assert test="./nc:DocumentIdentification/nc:IdentificationID">DocumentID must be present.</assert>
        </rule>
    </pattern>

</schema>