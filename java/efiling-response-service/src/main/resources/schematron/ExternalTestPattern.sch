<?xml version="1.0" encoding="UTF-8"?>
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="code-list-rules">
<!-- Required namespace declarations as indicated in this set of rules:
     <ns prefix="nc" uri="http://release.niem.gov/niem/niem-core/3.0/"/> -->
    
   <rule context="nc:BinaryFormatText">
      <assert test="( false() or ( contains('&#127;application/json&#127;application/msword&#127;application/pdf&#127;application/vnd.oasis.opendocument.text&#127;application/vnd.openxmlformats-officedocument.wordprocessingml.document&#127;application/xml&#127;',concat('&#127;',.,'&#127;')) ) ) ">Invalid binary format code value.</assert>
   </rule>
</pattern>