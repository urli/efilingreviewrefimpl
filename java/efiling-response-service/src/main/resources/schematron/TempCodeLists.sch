<?xml version="1.0" encoding="UTF-8"?>
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="code-list-rules">
<!-- Georgia CJE Genericode List Constraints 
$Id: order-constraints-doc.cva,v 1.21 2012/04/18 21:53:16 admin Exp $

    Required namespace declarations as indicated in this set of rules:
    
<ns prefix="cbrn" uri="http://release.niem.gov/niem/domains/cbrn/3.2/"/>
<ns prefix="ecf" uri="https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/ecf"/>
<ns prefix="ga" uri="http://cje.georgia.gov/v.0/extension"/>
<ns prefix="filing" uri="https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/filing"/>
<ns prefix="hdr" uri="http://cje.georgia.gov/extension/header/1.0"/>
<ns prefix="x" uri="http://www.w3.org/TR/REC-html40"/>
<ns prefix="nc" uri="http://release.niem.gov/niem/niem-core/3.0/"/>
<ns prefix="j" uri="http://release.niem.gov/niem/domains/jxdm/5.2/"/>

--><rule context="nc:BinaryFormatText">
      <!--{}[](binaryFormatText)--><assert test="( false() or ( contains('&#127;application/json&#127;application/msword&#127;application/pdf&#127;application/vnd.oasis.opendocument.text&#127;application/vnd.openxmlformats-officedocument.wordprocessingml.document&#127;application/xml&#127;',concat('&#127;',.,'&#127;')) ) ) ">Invalid binary format code value.</assert>
   </rule>
</pattern>