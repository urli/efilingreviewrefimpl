<?xml version="1.0" encoding="UTF-8"?>
<!-- Schematron business rules for E-Filing SSP
     Version 1.0 -->
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <title>Filing Review Request Tests</title>

    <ns prefix="hdr" uri="http://cje.georgia.gov/extension/header/1.0"/>
    <ns prefix="cbrn" uri="http://release.niem.gov/niem/domains/cbrn/3.2/"/>
    <ns prefix="ecf" uri="https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/ecf"/>
    <ns prefix="ga" uri="http://cje.georgia.gov/v.0/extension"/>
    <ns prefix="filing" uri="https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/filing"/>
    <ns prefix="x" uri="http://www.w3.org/TR/REC-html40"/>
    <ns prefix="nc" uri="http://release.niem.gov/niem/niem-core/3.0/"/>
    <ns prefix="j" uri="http://release.niem.gov/niem/domains/jxdm/5.2/"/>
    <ns prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
    <ns prefix="structures" uri="http://release.niem.gov/niem/structures/3.0/"/>
    <ns prefix="s" uri="http://release.niem.gov/niem/structures/3.0/"/>
    <ns prefix="criminal" uri="https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/criminal"/>
    <ns prefix="reviewfilingcallback" uri="https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/reviewfilingcallback"/> 
    <ns prefix="scr" uri="http://release.niem.gov/niem/domains/screening/3.2/"/>
    
    <include href="CodeLists.sch"/>
    
    <pattern id="ECF">
        <rule context="//filing:FilingMessage">
            <assert test="./nc:DocumentIdentification/nc:IdentificationID and string-length(normalize-space(./nc:DocumentIdentification/nc:IdentificationID)) > 0 and not(./nc:DocumentIdentification/@xsi:nil = 'true')">ECF 6.2.4 Message and Filing Identifiers: DocumentID must be present.</assert>
        </rule>
        <rule context="//filing:FilingConnectedDocument">
            <assert test="exists(../filing:FilingLeadDocument)">Cannot have a <name/> without a filing:FilingLeadDocument.</assert>
            <report test="../filing:FilingLeadDocument/@xsi:nil = 'true'">Cannot have a <name/> when filing:FilingLeadDocument xsi:nil is set to true.</report>
        </rule>
    </pattern>
    
    
    <!--Initial Filings (Accusation and Indictment)-->
    
    <!--Case Docket ID check; initial filings should not include Case Initiation Date; initial filings should not include Case Status.-->
    <pattern id="InitialFiling">
        <rule context="//filing:FilingLeadDocument/ecf:DocumentAugmentation[contains('Accusation,Indictment', ecf:RegisterActionDescriptionCode) and ../ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'false']">
            <let name="docType" value="./ecf:RegisterActionDescriptionCode"/>
            <report test="../../nc:Case/nc:CaseDocketID and ../../nc:Case/nc:CaseDocketID/@xsi:nil = 'false'">Initial <value-of select="$docType"/> filing cannot contain nc:Case/nc:CaseDocketID</report>
            <report test="../../nc:Case/nc:ActivityDateRange">Initial <value-of select="$docType"/> filing cannot contain nc:Case/nc:ActivityDateRange</report>
            <report test="../../nc:Case/nc:ActivityStatus">Initial <value-of select="$docType"/> filing cannot contain nc:Case/nc:ActivityStatus</report>
            <report test="string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseProsecutionAttorney/nc:RoleOfPerson/ecf:FilingAttorneyID/nc:IdentificationID)) > 0">Initial <value-of select="$docType"/> filing cannot contain value for j:CaseProsecutionAttorney/ecf:FilingAttorneyID</report>
            <report test="string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseDefendantParty/nc:EntityPerson/ecf:FilingAttorneyID/nc:IdentificationID)) > 0">Initial <value-of select="$docType"/> filing cannot contain value for j:CaseProsecutionAttorney/ecf:FilingAttorneyID</report>
        </rule>
    </pattern>

    <!--PersonChargeAssociation must refer back to the defendant to which the Charge applies; the DefendantArrest association must point/reference to the appropriate defendant from the case; only Accusations and Indictments can contain the element j:OrganizationORIIdentification.-->
	<pattern id="InitialFilingDefendantClass">
	    <rule context="//j:Arrest[not(@xsi:nil = 'true')]">
	        <assert test="./j:ArrestSubject/nc:RoleOfPerson/@s:ref">Element j:ArrestSubject/nc:RoleOfPerson must contain ref attribute pointing to a defendant.</assert>
	        <let name="ref" value="./j:ArrestSubject/nc:RoleOfPerson/@s:ref"/>
	        <assert test="//j:CaseDefendantParty/nc:EntityPerson[@s:id = $ref]">
	            Element j:ArrestSubject/nc:RoleOfPerson reference of '<value-of select="$ref"/> does not point to an nc:EntityPerson element for a j:CaseDefendantParty.
	        </assert>
	        <assert test="exists(./j:ArrestAgency/j:OrganizationAugmentation/j:OrganizationORIIdentification) 
	            or string-length(normalize-space(./j:ArrestAgency/j:OrganizationAugmentation/j:OrganizationORIIdentification/nc:IdentificationID)) > 0">
	            Element <name/> in an Accusation or Indictment must contain j:ArrestAgency/j:OrganizationAugmentation/j:OrganizationORIIdentification/nc:IdentificationID.
	        </assert>
	    </rule>
	    <rule context="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Accusation' or ecf:RegisterActionDescriptionCode = 'Indictment']">
	        <assert test="../../nc:Case/j:CaseAugmentation/j:CaseCharge/criminal:ChargeAugmentation/j:PersonChargeAssociation/nc:Person[@s:ref]">
	            For most exchanges, ECF 5.2 requires the presence of j:PersonChargeAssociation so the offense references back to the defendant to which this charge offense applies. 
	            The person is a reference using the ref attribute.
	        </assert>
	        
	        <let name="arrest" value="../../nc:Case/criminal:CaseAugmentation/j:Arrest"/>
		    <assert test="$arrest">An Indictment or Accusation filing require that there is element ecf:CaseAugmentation contains j:Arrest.</assert>

		</rule>
		<rule context="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode != 'Accusation' and ecf:RegisterActionDescriptionCode != 'Indictment']">
			<report test="exists(../../nc:Case/criminal:CaseAugmentation/j:Arrest)">The association DefendantArrest must be present in an Accusation or Indictment, but not in other filings.</report>
			<report test="exists(../../nc:Case/criminal:CaseAugmentation/j:Arrest/j:ArrestAgency/j:OrganizationAugmentation/j:OrganizationORIIdentification) or string-length(normalize-space(../../nc:Case/criminal:CaseAugmentation/j:Arrest/j:ArrestAgency/j:OrganizationAugmentation/j:OrganizationORIIdentification/nc:IdentificationID)) > 0">Element <name/> in an Accusation or Indictment must contain the child elements j:OrganizationORIIdentification/nc:IdentificationID.</report>
		</rule>
    </pattern>

    <!--An Indictment should not have a binary attachment or filing document augmentation, as only data is passed through to the Clerk; if there is a document, it must not be of type 'Accusation'.-->
    <pattern id="IndictmentInitialFiling">
        <rule context="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Indictment']">
            <report subject="./[ecf:RegisterActionDescriptionCode = 'Indictment']" test="ecf:DocumentRendition/nc:Attachment">Must not have attached binary document</report>
        </rule>
    </pattern>
    <!--An Indictment should not contain the element ecf:PersonAugmentation under j:CaseProsecutionAttorney/nc:RoleOfPerson.-->
    <pattern id="IndictmentPersonAugmentation">
		<rule context="filing:FilingMessage/nc:Case/j:CaseAugmentation//j:CaseProsecutionAttorney/nc:RoleOfPerson">
			<report test="(../../../../filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Indictment']) and exists(./ecf:PersonAugmentation)">Element <name/> in an Indictment must not contain the child element /ecf:PersonAugmentation.</report>
		</rule>
    </pattern>
    <!--An Accusation should have a binary attachment, a document sealed indicator, a document with type 'Accusation', and must not include amended charges if it is an initial filing.-->
    <pattern id="AccusationInitialFiling">
        <rule context="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Accusation' and ../ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'False']">
            <assert subject="./[ecf:RegisterActionDescriptionCode = 'Accusation']" test="ecf:DocumentRendition/nc:Attachment">Must have attached binary document</assert>
            <assert test="../ga:FilingDocumentAugmentation/ga:DocumentSealedIndicator">Must have document sealed indicator</assert>
            <assert test="../ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator">Accusation filing must always include element DocumentAmendedIndicator.</assert>
            <report test="../../nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/j:OriginalChargeAmendedChargeAssociation">Initial filing must not have amended charges.</report>
            <report test="../../nc:Case/nc:CaseDocketID">An initial Accusation cannot include a case number.</report>
        </rule>
    </pattern>
    
    <!--Rules for amending an Accusation or Indictment.-->
    
    <!--If an Accusation is being amended, it must have the code 'AmendCharges'; an amended Accusation must have an attached binary document, a sealed indicator, an amended indicator; and an amended charge must be associated with the new charge.-->
    <pattern id="AccusationAmendedCharges">
        <rule context="filing:FilingLeadDocument/ga:FilingDocumentAugmentation[ga:DocumentAmendedIndicator = 'True']">
            <assert subject="../[ecf:RegisterActionDescriptionCode = 'Accusation']" test="ecf:DocumentRendition/nc:Attachment">Must have attached binary document</assert>
            <assert test="../ga:FilingDocumentAugmentation/ga:DocumentSealedIndicator">Must have document sealed indicator</assert>
            <assert test="../ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator">Amended Charges must have Accusation Augmentation</assert>
            <assert test="../../nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/j:OriginalChargeAmendedChargeAssociation">If amending a charge, amended charge must be associated with new charge via the Amended Charge Relationship.</assert>
            <assert test="../../nc:Case/j:CaseAugmentation/j:CaseCharge/j:ChargeStatute/ga:OffenseTrackingNumberIdentification/nc:IdentificationID">For a new charge on an existing case (adding a charge), the OTN should be included.</assert>
            <assert test="../../nc:Case/nc:CaseDocketID">For a new charge on an existing case (adding a charge), the case number should be included.</assert>
            <let name="docType" value="../ecf:DocumentAugmentation/ecf:RegisterActionDescriptionCode"/>
            <assert test="string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseProsecutionAttorney/nc:RoleOfPerson/ecf:FilingAttorneyID/nc:IdentificationID)) > 0">Initial <value-of select="$docType"/> filing cannot contain value for j:CaseProsecutionAttorney/ecf:FilingAttorneyID</assert>
            <assert test="string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseDefendantParty/nc:EntityPerson/ecf:FilingAttorneyID/nc:IdentificationID)) > 0">Initial <value-of select="$docType"/> filing cannot contain value for j:CaseProsecutionAttorney/ecf:FilingAttorneyID</assert>
        </rule>
        <rule context="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode != 'Accusation' and ecf:RegisterActionDescriptionCode != 'Indictment']">
			<report test="exists(../../nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/j:OriginalChargeAmendedChargeAssociation)">Filings other than Accusations or Indictments cannot have association AssociateAmendedCharge under the ga:ChargeAugmentation element.</report>
        </rule>
    </pattern>

    <!--Accusation: amended charges without an OTN and not associated with a booking event. A CCH Charge Update must occur if additional charge is added to an Accusation (i.e. the Indictment is being amended to include an additional charge) and does not include an OTN (must obtain an OTN from CCH).-->
    <pattern id="AccusationAmendedCharges2">
        <rule context="filing:FilingLeadDocument[nc:DocumentCategoryText = 'Accusation' or nc:DocumentCategoryText = 'Indictment']">
            <report subject="./ga:FilingDocumentAugmentation[ga:DocumentAmendedIndicator = 'True']" test="exists(../filing:FilingMessage/nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/j:OriginalChargeAmendedChargeAssociation/j:AmendedCharge/j:ChargeTrackingIdentification/nc:IdentificationID)">If an additional charge is presented that was not part of the original booking event and not associated with an existing OTN, the prosecutor's CMS must initiate a CCH Charge Update to acquire a new CTN.</report> 
        </rule>
    </pattern>
    
    <!--All E-Filings-->
    <!--These rules may apply to any E-Filing, but generally apply to filings other than Accusations or Indictments (initial filings). Organized alphabetically by class.-->
    
    <!--Attorney class: rules for assigning an ID to a prosecutor or defense attorney.-->
    <pattern id="CommonFilingsAttorneyClass">
        <rule context="//ecf:AttorneyID[not(contains('Indictment,Accusation', ancestor::filing:FilingMessage/filing:FilingLeadDocument/ecf:DocumentAugmentation/ecf:RegisterActionDescriptionCode))]">
            <let name="doc" value="ancestor::filing:FilingMessage/filing:FilingLeadDocument"/>
            <let name="docType" value="$doc/ecf:DocumentAugmentation/ecf:RegisterActionDescriptionCode"/>
            <assert test="string-length(normalize-space(./nc:IdentificationID))"><value-of select="$docType"/> filing must have value in <name/>/nc:IdentificationID</assert>
        </rule>
        <rule context="filing:FilingMessage/nc:Case/j:CaseAugmentation/j:CaseProsecutionAttorney">
            <assert test="exists(nc:RoleOfPerson/@s:id)"><name/> child element nc:RoleOfPerson must contain attribute structures:id.</assert>
        </rule>
        <rule context="filing:FilingMessage/nc:Case/j:CaseAugmentation/j:CaseDefenseAttorney">
            <assert test="exists(nc:RoleOfPerson/@s:id)"><name/>Element j:CaseDefenseAttorney/nc:RoleOfPerson must contain attribute structures:id.</assert>
        </rule>
        <rule context="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Petition']">
			<report test="exists(../../nc:Case/j:CaseAugmentation/j:CaseProsecutionAttorney)">A probation violation petition must not contain element j:CaseProsecutionAttorney.</report>
		</rule>
    </pattern>
    
    <!--Attorney class: rules for element RepresentedParty.-->
    <pattern id="CommonFilingsAttorneyRepresentedParty">
        <rule context="//j:CaseDefenseAttorney">
            <let name="docType" value="../../../filing:FilingLeadDocument/ecf:DocumentAugmentation/ecf:RegisterActionDescriptionCode"/>
            <let name="defRef" value="./ecf:CaseOfficialAugmentation/ecf:CaseRepresentedParty/nc:EntityPerson/@s:ref"/>
            <assert test="./ecf:CaseOfficialAugmentation/ecf:CaseRepresentedParty/nc:EntityPerson/@s:ref">For <value-of select="$docType"/>, when the Role for the Attorney is Defense, this element is required. When the Role for the Attorney is Prosecutor, this element is not required.</assert>
            <assert test="..//j:CaseDefendantParty/nc:EntityPerson[@structures:id =$defRef]">Represented party must point to a defendant</assert>
        </rule>
        <rule context="//j:CaseProsecutingAttorney">
            <report test="./ecf:CaseOfficialAugmentation/ecf:CaseRepresentedParty"><name/> must not represent any party specified in the filing</report>
        </rule>
    </pattern>
    
    
    <!--Case class: rules for element CaseStatus in filings other than Indictments.-->  
    <pattern id="CommonFilingsCaseStatus">
        <rule context="//filing:FilingLeadDocument/ecf:DocumentAugmentation[(ecf:RegisterActionDescriptionCode = 'Accusation' and ../ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true') or (ecf:RegisterActionDescriptionCode != 'Indictment' and ecf:RegisterActionDescriptionCode != 'Accusation')]">
            <report test="../../nc:Case/nc:ActivityStatus/nc:StatusDescriptionText">The Case Status attribute in every filing except for an Indictment must exist.</report>
            <let name="caseNumber" value="../../nc:Case/nc:CaseDocketID"/>
            <assert test="string-length(normalize-space(../../nc:Case/nc:CaseDocketID)) > 0">The Court Case Number must exist in every filing except for initial filings (i.e. Accusations and Indictments) and can never contain attribute xsi:nil = 'true'.</assert>
        </rule>
    </pattern>
    <!--Case class: rules for element CaseType.-->
    <pattern id="CommonFilingsCaseType">
        <rule context="filing:FilingLeadDocument/ecf:DocumentAugmentation">
            <assert test="exists(../../nc:Case/ecf:CaseAugmentation/ecf:CaseCategoryCode) and ../../nc:Case/ecf:CaseAugmentation/ecf:CaseCategoryCode = 'criminal'">For CJE, the Case Type attribute in every filing must exist and contain a value of 'criminal' (case sensitive).</assert>
        </rule>
    </pattern>
    
	<!--Charge class rules.-->
    <pattern id="CommonFilingChargeClass">
        <rule context="//j:CaseCharge | //j:Charge">
            <assert test="./@s:id">Each charge must have attribute structures:id assigned.</assert>
		</rule>
    </pattern>
    
    <!--Charge class: OTN & CTN rules.-->
    <pattern id="CommonFilingChargeClassIDNumbers">
        <rule context="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Accusation' or ecf:RegisterActionDescriptionCode = 'Indictment']">
            <assert test="string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseCharge/j:ChargeStatute/ga:OffenseTrackingNumberIdentification/nc:IdentificationID)) > 0 
                                                     or ../../nc:Case/j:CaseAugmentation/j:CaseCharge/j:ChargeStatute/ga:OffenseTrackingNumberIdentification/@xsi:nil = 'true'">Must either contain child element IdentificationID with a valid value or have attribute xsi:nil="true" if OTN value is empty or not given by GCIC for new charges not associated with an existing OTN.</assert>
            <assert test="string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseCharge/j:ChargeTrackingIdentification/nc:IdentificationID)) > 0 
                                                     or ../../nc:Case/j:CaseAugmentation/j:CaseCharge/j:ChargeTrackingIdentification/@xsi:nil = 'true'">Must either contain child element IdentificationID with a valid value or have attribute xsi:nil="true" if CTN value is empty or not given by GCIC for new charges not associated with an existing CTN.</assert>
        </rule>
        <rule context="//ga:ViolationFilingAugmentation//j:Charge">
            <assert test="string-length(normalize-space(./j:ChargeStatute/ga:OffenseTrackingNumberIdentification/nc:IdentificationID)) > 0 
                or ./j:ChargeStatute/ga:OffenseTrackingNumberIdentification/@xsi:nil = 'true'">Must either contain child element IdentificationID with a valid value or have attribute xsi:nil="true" if OTN value is empty or not given by GCIC for new charges not associated with an existing OTN.</assert>
            <assert test="string-length(normalize-space(./j:ChargeTrackingIdentification/nc:IdentificationID)) > 0 
                or //ga:ViolationFilingAugmentation//j:Charge/j:ChargeTrackingIdentification/@xsi:nil = 'true'">Must either contain child element IdentificationID with a valid value or have attribute xsi:nil="true" if CTN value is empty or not given by GCIC for new charges not associated with an existing CTN.</assert>
        </rule>
    </pattern>
    
    <!--Charge class: GCIC and Warrant rules for Accusations, Indictments, or Probation Violations.-->
    <pattern id="CommonFilingChargeClassGCICandWarrant">
        <rule context="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Accusation' or ecf:RegisterActionDescriptionCode = 'Indictment']">
            <assert test="string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/ga:ChargeWarrantNumber)) > 0 
                or ../../nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/ga:ChargeWarrantNumber/@xsi:nil = 'true'">Warrant number must either contain a value or include xsi:nil="true" if no value exists.</assert>
        </rule>
        <rule context="//ga:ViolationFilingAugmentation//j:Charge">
            <assert test="string-length(normalize-space(./ga:ChargeAugmentation/ga:ChargeWarrantNumber)) > 0 
                or ./ga:ChargeAugmentation/ga:ChargeWarrantNumber/@xsi:nil = 'true'">Warrant number must either contain a value or include xsi:nil="true" if no value exists.</assert>
        </rule>
    </pattern>
    
    <!--Contact Information class -->
    <pattern id="ContactInformationAssociation">
		<rule context="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Accusation' or ecf:RegisterActionDescriptionCode = 'Indictment' or ecf:RegisterActionDescriptionCode = 'Appearance']">
			<assert test="exists(../../nc:Case/j:CaseAugmentation/j:CaseDefendantParty/nc:EntityPerson/ecf:PersonAugmentation/nc:ContactInformationAssociation)">The association PersonContactInformation must be present in Accusations, Indictments, and Appearances.</assert>
		</rule>
		<rule context="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Motion' or ecf:RegisterActionDescriptionCode = 'Petition' or ecf:RegisterActionDescriptionCode = 'Warrant']">
			<report test="exists(../../nc:Case/j:CaseAugmentation/j:CaseDefendantParty/nc:EntityPerson/ecf:PersonAugmentation/nc:ContactInformationAssociation)">The association PersonContactInformation can only be present in Accusations, Indictments, and Appearances.</report>
		</rule>
    </pattern>
    
    <!--Court class: the CourtTypeCode and CountyCode must exist for all E-Filings.-->
    <pattern id="CaseCourt">
		<rule context="filing:FilingMessage//j:CaseCourt">
			<let name="courtCategory" value="./ga:CourtCategoryCode"/>
			<assert test="(exists($courtCategory) and string-length(normalize-space($courtCategory)) > 0)">Element <name/> must include a value in /ga:CourtCategoryCode for all filings.</assert>
			<let name="countyCode" value="./nc:OrganizationIdentification/nc:IdentificationJurisdiction/ga:JurisdictionCountyCode"/>
			<assert test="(exists($countyCode) and string-length(normalize-space($countyCode)) > 0)">Element <name/> must include a value in /nc:OrganizationIdentification/nc:IdentificationJurisdiction/ga:JurisdictionCountyCode for all filings.</assert>
		</rule>
    </pattern>
    
	<!-- General rules restricting what should not be present for certain exchanges.-->
    <pattern id="GeneralRestrictionRules">
		<!--The connected document must have an id (the 'structures:id' attribute in the xml), must be associated back to the lead document, and must have a valid DocumentRelatedCode.-->
        <rule context="//filing:FilingConnectedDocument | //ecf:ReviewedConnectedDocument">
            <assert test="./@s:id">Connected document must have id.</assert>
            <assert test="string-length(normalize-space(ecf:DocumentAugmentation/nc:DocumentAssociation/ecf:DocumentAssociationAugmentation/ecf:DocumentRelatedCode)) > 0">Must have a valid code value for the association</assert>
            <let name="parentID" value="../filing:FilingLeadDocument/@s:id"/>
            <assert test="ecf:DocumentAugmentation/nc:DocumentAssociation/nc:PrimaryDocument/@s:ref = $parentID">Connected document must point back to lead document</assert>
            <assert test="ecf:DocumentAugmentation/nc:DocumentAssociation/ecf:DocumentAssociationAugmentation/ecf:DocumentRelatedCode"/>
            <let name="docType" value="../filing:FilingLeadDocument/ecf:DocumentAugmentation/ecf:RegisterActionDescriptionCode"/>
            <report test="contains('Appearance,Indictment', $docType)"><value-of select="$docType"/> filing cannot contain <name/></report>
            <report test="ga:FilingDocumentAugmentation/ga:DocumentStampedDate"><name/> cannot contain ga:FilingDocumentAugmentation/ga:DocumentStampedDate</report>
            <report test="nc:DocumentFileControlID"><name/> cannot contain nc:DocumentFileControlID</report>
        </rule>
        <rule context="//filing:FilingLeadDocument">
            <let name="docType" value="./ecf:DocumentAugmentation/ecf:RegisterActionDescriptionCode"/>
            <report test="not(contains('Motion,Violation', $docType)) and ./ga:FilingDocumentAugmentation/ga:DocumentSubtype"><value-of select="$docType"/> filing cannot contain <name/>/ga:FilingDocumentAugmentation/ga:DocumentSubtype</report>
            <report test="not(contains('Motion,Violation', $docType)) and ./nc:DocumentDescriptionText"><value-of select="$docType"/> filing cannot contain <name/>/nc:DocumentDescriptionText</report>
            <report test="($docType = 'Indictment') and ./ecf:DocumentAugmentation/ecf:SpecialHandlingInstructionsText"><value-of select="$docType"/> filing cannot contain <name/>/ga:FilingDocumentAugmentation/ecf:SpecialHandlingInstructionsText</report>
            <report test="($docType = 'Indictment') and ./ga:FilingDocumentAugmentation/ga:DocumentSealedIndicator"><value-of select="$docType"/> filing cannot contain <name/>/ga:FilingDocumentAugmentation/ga:DocumentSealedIndicator</report>
            <report test="./ga:FilingDocumentAugmentation/ga:DocumentStampedDate"><name/> cannot contain ga:FilingDocumentAugmentation/ga:DocumentStampedDate</report>
            <report test="./nc:DocumentFileControlID"><name/> cannot contain nc:DocumentFileControlID</report>
        </rule>
        <rule context="//ecf:ReviewedLeadDocument | //ecf:ReviewedConnectedDocument">
            <report test="./ecf:DocumentAugmentation/ecf:SpecialHandlingInstructionsText"><name/> cannot contain ecf:DocumentAugmentation/ecf:SpecialHandlingInstructionsText</report>
            <report test="./ga:FilingDocumentAugmentation/ga:DocumentSealedIndicator"><name/> cannot contain ga:FilingDocumentAugmentation/ga:DocumentSealedIndicator</report>
            <assert test="./ga:FilingDocumentAugmentation/ga:DocumentStampedDate"><name/> must contain ga:FilingDocumentAugmentation/ga:DocumentStampedDate</assert>
            <assert test="./nc:DocumentFileControlID"><name/> must contain nc:DocumentFileControlID</assert>
        </rule> 
        <rule context="//nc:RoleOfPerson[not(@xsi:nil) or @xsi:nil != 'true'] | //nc:EntityPerson[not(@xsi:nil) or @xsi:nil != 'true']">
            <let name="parent" value="local-name(..)"/>
            <let name="doc" value="ancestor::filing:FilingMessage/filing:FilingLeadDocument | reviewfilingcallback:NotifyFilingReviewCompleteMessage/ecf:ReviewedLeadDocument"/>
            <let name="docType" value="$doc/ecf:DocumentAugmentation/ecf:RegisterActionDescriptionCode"/>
            <report test="$parent != 'CaseDefendantParty' and ./nc:PersonTaxIdentification"><value-of select="$parent"/>/<name/> cannot contain nc:PersonTaxIdentification</report>
            <report test="(not(contains('Indictment,Accusation', $docType)) or (contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true')) and $parent = 'j:CaseDefendantParty' and ./nc:PersonTaxIdentification">Any subsequent filing cannot contain <value-of select="$parent"/>/nc:PersonTaxIdentification</report>

            <report test="$parent != 'CaseDefendantParty' and ./nc:PersonStateIdentification"><value-of select="$parent"/>/<name/> cannot contain nc:PersonStateIdentification</report>
            <report test="(not(contains('Indictment,Accusation', $docType)) or (contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true')) and $parent = 'j:CaseDefendantParty' and ./nc:PersonStateIdentification">Any subsequent filing cannot contain <value-of select="$parent"/>/nc:PersonStateIdentification</report>

            <report test="$parent != 'CaseDefendantParty' and (./j:PersonAugmentation or ./j:PersonAugmentation/j:PersonFBIIdentification)"><value-of select="$parent"/>/<name/> cannot contain j:PersonAugmentation/j:PersonFBIIdentification</report>
            <report test="(not(contains('Indictment,Accusation', $docType)) or (contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true')) and $parent = 'j:CaseDefendantParty' and (./j:PersonAugmentation or ./j:PersonAugmentation/j:PersonFBIIdentification)">Any subsequent filing cannot contain <value-of select="$parent"/>/j:PersonAugmentation/j:PersonFBIIdentification</report>

            <report test="$parent != 'CaseDefendantParty' and (./ga:PersonAugmentation or ./ga:PersonAugmentation/scr:InmateNumberID)"><value-of select="$parent"/>/<name/> cannot contain ga:PersonAugmentation/scr:InmateNumberID</report>
            <report test="(not(contains('Indictment,Accusation', $docType)) or (contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true')) and $parent = 'j:CaseDefendantParty' and (./ga:PersonAugmentation or ./ga:PersonAugmentation/scr:InmateNumberID)">Any subsequent filing cannot contain <value-of select="$parent"/>/ga:PersonAugmentation/scr:InmateNumberID</report>
           
            <report test="$parent != 'CaseDefendantParty' and ./nc:PersonBirthDate"><value-of select="$parent"/>/<name/> cannot contain nc:PersonBirthDate</report>
            <report test="(not(contains('Indictment,Accusation', $docType)) or (contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true')) and $parent = 'j:CaseDefendantParty' and ./nc:PersonBirthDate">Any subsequent filing cannot contain <value-of select="$parent"/>/nc:PersonBirthDate</report>
        </rule>
        <rule context="//nc:Case">
            <let name="doc" value="../filing:FilingLeadDocument"/>
            <let name="docType" value="$doc/ecf:DocumentAugmentation/ecf:RegisterActionDescriptionCode"/>
            <let name="part1" value="contains('Indictment,Accusation', $docType)"/>
            <let name="part2" value="$doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true'"/>
            <let name="isAmendedAccusationOrIndictment" value="contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true'"/>
            <!--
            <report test="(contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'false') 
                and ./nc:ActivityDateRange">Any initial filing cannot contain <name/>/nc:ActivityDateRange</report>
            <assert test="(contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true') 
                and ./nc:ActivityDateRange">Any subsequent filing must contain <name/>/nc:ActivityDateRange; <value-of select="$isAmendedAccusationOrIndictment"/>
               ; doctype=<value-of select="$docType"/>; part1=<value-of select="$part1"/>; part2=<value-of select="$part2"/></assert>
             -->   
             
            <report test="contains('Motion,Appearance,Petition', $docType) and ./nc:CaseTrackingID"> <value-of select="$docType"/> filing cannot contain <name/>/nc:CaseTrackingID</report>
            
            <!-- nc:Case/nc:ActivityDateRange (Class: Case, Attribute: Case Initiation Date) Not in Initial filing for Accusation & Indictment
nc:Case/nc:ActivityStatus (Class: Case, Attribute: Case Status)  Not in Initial filing for Accusation & Indictment
nc:Case/nc:CaseDocketID(Class: Case, Attribute: Court Case Number) Not in Initial filing for Accusation & Indictment

nc:Case/nc:CaseTitleText(Class: Case, Attribute: CaseTitle) Not in FilingResponse -->

            
        </rule>
    </pattern>
    
	<!--Defendant class. The charge must be referenced back to the defendant.-->
    <pattern id="CommonFilingDefendantClass">
		<rule context="/filing:FilingMessage/nc:Case/j:CaseAugmentation/j:CaseCharge/criminal:ChargeAugmentation/j:PersonChargeAssociation/nc:Person">
		    <let name="reference" value="./@s:ref"/>
			<assert test="./@s:ref">ECF 5.2 requires the presence of j:PersonChargeAssociation so the offense references back to the defendant to which this charge offense applies. The person is a reference using the ref attribute.</assert>
		    <assert test="/filing:FilingMessage//j:CaseDefendantParty/nc:EntityPerson[@s:id = $reference]">Charge association must point back to a defendant.</assert>
		</rule>
    </pattern>
    
	<!--Document rules.-->
    <pattern id="CommonEFilings">
		<!--If a filing is not an accusation or indictment, the lead document must have an id (the 'structures:id' attribute in the xml).-->
        <rule context="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode != 'Accusation' and ecf:RegisterActionDescriptionCode != 'Indictment']">
            <assert test="../@s:id">Lead document must have id.</assert>
        </rule>
        <rule context="/filing:FilingMessage/ecf:FilingPartyID">
            <assert test="string-length(normalize-space(./nc:IdentificationID)) > 0">There must be a value for <name/></assert>
        </rule>
    </pattern>
    
    
    <!--FilingAllegedViolation association rule for Probation Violation: Petition or Motion. -->
    <pattern id="ViolationAugmentation">
		<rule context="filing:FilingMessage/filing:FilingLeadDocument[nc:DocumentCategoryText = 'Violation']">
			<assert test="exists(../ga:ViolationFilingAugmentation)">A probation violation petition or motion to modify probation must contain element ga:ViolationFilingAugmentation for the FilingAllegedViolation association.</assert>
		</rule>
    </pattern>
    
    
    <!--Organization class: rules for element OrganizationID.-->
    <pattern id="OrganizationID">
        <rule context="//nc:Organization">
            <assert test="./@s:id">Every organization element should have attribute structures:id assigned.</assert>
        </rule>
    </pattern>
    
    <!--Person class rules.-->
    <pattern id="CommonFilingsPersonClass">
        <rule context="//j:CaseDefendantParty/nc:EntityPerson | //j:CaseProsecutionAttorney/nc:RoleOfPerson | //j:CaseDefenseAttorney/nc:RoleOfPerson | j:CaseOtherEntity/nc:EntityPerson">
		    <assert test="./@s:id"><name/> is missing attribute structures:id to the person element.</assert>
		</rule>
    </pattern>
    <!--PersonName class: rules for element FullName.-->
    <pattern id="CommonFilingPersonFullName">
		<rule context="//nc:PersonName">
		    <assert test="(./nc:PersonFullName and ((not(./nc:PersonNamePrefixText) or (./nc:PersonNamePrefixText/@xsi:nil = 'true'))
                                        		        or (not(./nc:PersonGivenName) or (./nc:PersonGivenName/@xsi:nil = 'true'))
                                        		        or (not(./nc:PersonMiddleName) or (./nc:PersonMiddleName/@xsi:nil = 'true'))
                                        		        or (not(./nc:PersonSurName) or (./nc:PersonSurName/@xsi:nil = 'true'))
                                        		        or (not(./nc:PersonNameSuffixText) or (./nc:PersonNameSuffixText/@xsi:nil = 'true')))) 
                  or (not(./nc:PersonFullName) and ((not(./nc:PersonNamePrefixText/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonNamePrefixText)) > 0)
                                        		        or (not(./nc:PersonGivenName/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonGivenName)) > 0)
                                        		        or (not(./nc:PersonMiddleName/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonMiddleName)) > 0)
                                        		        or (not(./nc:PersonSurName/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonSurName)) > 0)
                                        		        or (not(./nc:PersonNameSuffixText/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonNameSuffixText)) > 0)))">When FullName is present in an instance, then first name, middle name, and last name will not be present and vice versa.</assert>               
		</rule>
    </pattern>
    <!--PersonName class: rules for elements FirstName and LastName.-->
    <pattern id="PersonFirstandLastName">
        <rule context="//nc:PersonName[not(nc:PersonFullName)]">
            <assert test="(not(./nc:PersonGivenName/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonGivenName)) > 0)
                or (not(./nc:PersonSurName/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonSurName)) > 0)">At least one of first name, and last name must be present.</assert> 
        </rule>
    </pattern>
    
    <!--Probation Violation: Probation Officer rules.-->
    <pattern id="ProbationViolationProbationOfficer">
		<rule context="//filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Petition']">
			<let name="ProbationOfficer" value="../../nc:Case/j:CaseAugmentation/j:CaseOtherEntity/nc:EntityPerson/ecf:PersonAugmentation/ecf:FilingPartyID/nc:IdentificationID"/>
		    <let name="FilingAttorneyID" value="//filing:FilingMessage/filing:FilingLeadDocument/ecf:DocumentAugmentation/ecf:FilingAttorneyID/nc:IdentificationID"/>
		    <assert test="$ProbationOfficer = $FilingAttorneyID">The ProbationOfficerID '<value-of select="$ProbationOfficer"/>' must match the FilingAttorneyID '<value-of select="$FilingAttorneyID"/>' in the Document metadata in a Probation Violation Petition.</assert>
		</rule>
    </pattern>
    <!--Probation Violation: Addition Probation Officer rules. -->
     <pattern id="CaseOtherEntity">
		<rule context="//j:CaseOtherEntity">
			<report test="../../../filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode != 'Petition']">Only probation violation petition filings can contain the CaseOtherEntity element for the Probation Officer.</report>
		</rule>
    </pattern>

    <pattern id="ResponseDocument">
        <rule context="//ecf:ReviewedLeadDocument | ecf:ReviewedConnectedDocument">
            <assert test="./nc:DocumentFileControlID and string-length(normalize-space(./nc:DocumentFileControlID)) > 0 and not(./nc:DocumentFileControlID/@xsi:nill='true')">Response document must always return court identifier for the filing.</assert>
            <assert test="./ga:FilingDocumentAugmentation/ga:DocumentStampedDate/nc:Date">Response document must always return with a timestamp of the actual filed date for the document.</assert>
            <assert test="./ecf:DocumentAugmentation/ecf:DocumentRendition/nc:Attachment/nc:Base64BinaryObject">Response document must always return with the stamped document.</assert>
        </rule>
        <rule context="/reviewfilingcallback:NotifyFilingReviewCompleteMessage/nc:Case/j:CaseAugmentation//*[contains(name(), 'Case') and ends-with(name(), 'Party')]//*[contains(name(), 'Case') and ends-with(name(), 'Attorney')]//nc:EntityPerson">
            <assert test="./ecf:PersonAugmentation/ecf:FilingPartyID or ./@s:ref and string-length(normalize-space(./ecf:PersonAugmentation/ecf:FilingPartyID)) > 0">The response must return the court assigned identifier (FilingPartyID) for the case.</assert>
        </rule>
    </pattern>
    
    <!--Validation check for elements in schema not included in model.-->
    <!--
    <pattern id="">
		<rule context="//">
			<assert test="name(.)">Element <name/> is included in the schema, but should not be included in the model.</assert>
		</rule>
    </pattern>
    -->
    
    
<!-- To add after pilots
        
Section 4.2
    If they occur in an ECF message, augmentations that substitute for nc:CaseAugmentationPoint MUST occur in the following order:
        - j:CaseAugmentation
        - ecf:CaseAugmentation
        - ECF case-type-specific augmentations (listed in the table below)
            nc:CaseAugmentationPoint
            bankruptcy:CaseAugmentation
            citation:CaseAugmentation
            civil:CaseAugmentation
            criminal:CaseAugmentation
            domestic:CaseAugmentation
            juvenile:CaseAugmentation
        - Implementation-specific case augmentations

Section 4.5
    Errors MUST be reported with the cbrn:ErrorCodeText element. Successful request and response
    messages MUST return an cbrn:ErrorCodeText of “0”

    -->
</schema>