<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron">
    
    <title>Business rules for maximum total value</title>
    
    <ns prefix="cbrn" uri="http://release.niem.gov/niem/domains/cbrn/3.2/"/>
    <ns prefix="ecf" uri="https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/ecf"/>
    <ns prefix="ga" uri="http://cje.georgia.gov/v.0/extension"/>
    <ns prefix="x" uri="http://www.w3.org/TR/REC-html40"/>
    <ns prefix="nc" uri="http://release.niem.gov/niem/niem-core/3.0/"/>
    <ns prefix="j" uri="http://release.niem.gov/niem/domains/jxdm/5.2/"/>
    
    <!--
    <phase id="full-schematron">
        <active pattern="code-list-rules"/>
        <active pattern="total-limit"/>
    </phase>
    -->
    <include href="EFilingBasic.sch"/>
    <include href="CodeLists.sch"/>
</schema>