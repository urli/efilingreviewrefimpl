<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:saxon="http://saxon.sf.net/"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:schold="http://www.ascc.net/xml/schematron"
                xmlns:iso="http://purl.oclc.org/dsdl/schematron"
                xmlns:xhtml="http://www.w3.org/1999/xhtml"
                xmlns:hdr="http://cje.georgia.gov/extension/header/1.0"
                xmlns:cbrn="http://release.niem.gov/niem/domains/cbrn/3.2/"
                xmlns:ecf="https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/ecf"
                xmlns:ga="http://cje.georgia.gov/v.0/extension"
                xmlns:filing="https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/filing"
                xmlns:x="http://www.w3.org/TR/REC-html40"
                xmlns:nc="http://release.niem.gov/niem/niem-core/3.0/"
                xmlns:j="http://release.niem.gov/niem/domains/jxdm/5.2/"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:structures="http://release.niem.gov/niem/structures/3.0/"
                xmlns:s="http://release.niem.gov/niem/structures/3.0/"
                xmlns:criminal="https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/criminal"
                xmlns:reviewfilingcallback="https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/reviewfilingcallback"
                xmlns:scr="http://release.niem.gov/niem/domains/screening/3.2/"
                version="2.0"><!--Implementers: please note that overriding process-prolog or process-root is 
    the preferred method for meta-stylesheets to use where possible. -->
<xsl:param name="archiveDirParameter"/>
   <xsl:param name="archiveNameParameter"/>
   <xsl:param name="fileNameParameter"/>
   <xsl:param name="fileDirParameter"/>
   <xsl:variable name="document-uri">
      <xsl:value-of select="document-uri(/)"/>
   </xsl:variable>

   <!--PHASES-->


<!--PROLOG-->
<xsl:output method="text"/>

   <!--XSD TYPES FOR XSLT2-->


<!--KEYS AND FUNCTIONS-->


<!--DEFAULT RULES-->


<!--MODE: SCHEMATRON-SELECT-FULL-PATH-->
<!--This mode can be used to generate an ugly though full XPath for locators-->
<xsl:template match="*" mode="schematron-select-full-path">
      <xsl:apply-templates select="." mode="schematron-get-full-path"/>
   </xsl:template>

   <!--MODE: SCHEMATRON-FULL-PATH-->
<!--This mode can be used to generate an ugly though full XPath for locators-->
<xsl:template match="*" mode="schematron-get-full-path">
      <xsl:apply-templates select="parent::*" mode="schematron-get-full-path"/>
      <xsl:text>/</xsl:text>
      <xsl:choose>
         <xsl:when test="namespace-uri()=''">
            <xsl:value-of select="name()"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:text>*:</xsl:text>
            <xsl:value-of select="local-name()"/>
            <xsl:text>[namespace-uri()='</xsl:text>
            <xsl:value-of select="namespace-uri()"/>
            <xsl:text>']</xsl:text>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="preceding"
                    select="count(preceding-sibling::*[local-name()=local-name(current())                                   and namespace-uri() = namespace-uri(current())])"/>
      <xsl:text>[</xsl:text>
      <xsl:value-of select="1+ $preceding"/>
      <xsl:text>]</xsl:text>
   </xsl:template>
   <xsl:template match="@*" mode="schematron-get-full-path">
      <xsl:apply-templates select="parent::*" mode="schematron-get-full-path"/>
      <xsl:text>/</xsl:text>
      <xsl:choose>
         <xsl:when test="namespace-uri()=''">@<xsl:value-of select="name()"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:text>@*[local-name()='</xsl:text>
            <xsl:value-of select="local-name()"/>
            <xsl:text>' and namespace-uri()='</xsl:text>
            <xsl:value-of select="namespace-uri()"/>
            <xsl:text>']</xsl:text>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!--MODE: SCHEMATRON-FULL-PATH-2-->
<!--This mode can be used to generate prefixed XPath for humans-->
<xsl:template match="node() | @*" mode="schematron-get-full-path-2">
      <xsl:for-each select="ancestor-or-self::*">
         <xsl:text>/</xsl:text>
         <xsl:value-of select="name(.)"/>
         <xsl:if test="preceding-sibling::*[name(.)=name(current())]">
            <xsl:text>[</xsl:text>
            <xsl:value-of select="count(preceding-sibling::*[name(.)=name(current())])+1"/>
            <xsl:text>]</xsl:text>
         </xsl:if>
      </xsl:for-each>
      <xsl:if test="not(self::*)">
         <xsl:text/>/@<xsl:value-of select="name(.)"/>
      </xsl:if>
   </xsl:template>
   <!--MODE: SCHEMATRON-FULL-PATH-3-->
<!--This mode can be used to generate prefixed XPath for humans 
	(Top-level element has index)-->
<xsl:template match="node() | @*" mode="schematron-get-full-path-3">
      <xsl:for-each select="ancestor-or-self::*">
         <xsl:text>/</xsl:text>
         <xsl:value-of select="name(.)"/>
         <xsl:if test="parent::*">
            <xsl:text>[</xsl:text>
            <xsl:value-of select="count(preceding-sibling::*[name(.)=name(current())])+1"/>
            <xsl:text>]</xsl:text>
         </xsl:if>
      </xsl:for-each>
      <xsl:if test="not(self::*)">
         <xsl:text/>/@<xsl:value-of select="name(.)"/>
      </xsl:if>
   </xsl:template>

   <!--MODE: GENERATE-ID-FROM-PATH -->
<xsl:template match="/" mode="generate-id-from-path"/>
   <xsl:template match="text()" mode="generate-id-from-path">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:value-of select="concat('.text-', 1+count(preceding-sibling::text()), '-')"/>
   </xsl:template>
   <xsl:template match="comment()" mode="generate-id-from-path">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:value-of select="concat('.comment-', 1+count(preceding-sibling::comment()), '-')"/>
   </xsl:template>
   <xsl:template match="processing-instruction()" mode="generate-id-from-path">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:value-of select="concat('.processing-instruction-', 1+count(preceding-sibling::processing-instruction()), '-')"/>
   </xsl:template>
   <xsl:template match="@*" mode="generate-id-from-path">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:value-of select="concat('.@', name())"/>
   </xsl:template>
   <xsl:template match="*" mode="generate-id-from-path" priority="-0.5">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:text>.</xsl:text>
      <xsl:value-of select="concat('.',name(),'-',1+count(preceding-sibling::*[name()=name(current())]),'-')"/>
   </xsl:template>

   <!--MODE: GENERATE-ID-2 -->
<xsl:template match="/" mode="generate-id-2">U</xsl:template>
   <xsl:template match="*" mode="generate-id-2" priority="2">
      <xsl:text>U</xsl:text>
      <xsl:number level="multiple" count="*"/>
   </xsl:template>
   <xsl:template match="node()" mode="generate-id-2">
      <xsl:text>U.</xsl:text>
      <xsl:number level="multiple" count="*"/>
      <xsl:text>n</xsl:text>
      <xsl:number count="node()"/>
   </xsl:template>
   <xsl:template match="@*" mode="generate-id-2">
      <xsl:text>U.</xsl:text>
      <xsl:number level="multiple" count="*"/>
      <xsl:text>_</xsl:text>
      <xsl:value-of select="string-length(local-name(.))"/>
      <xsl:text>_</xsl:text>
      <xsl:value-of select="translate(name(),':','.')"/>
   </xsl:template>
   <!--Strip characters--><xsl:template match="text()" priority="-1"/>

   <!--SCHEMA SETUP-->
<xsl:template match="/">
      <xsl:apply-templates select="/" mode="M0"/>
      <xsl:apply-templates select="/" mode="M16"/>
      <xsl:apply-templates select="/" mode="M17"/>
      <xsl:apply-templates select="/" mode="M18"/>
      <xsl:apply-templates select="/" mode="M19"/>
      <xsl:apply-templates select="/" mode="M20"/>
      <xsl:apply-templates select="/" mode="M21"/>
      <xsl:apply-templates select="/" mode="M22"/>
      <xsl:apply-templates select="/" mode="M23"/>
      <xsl:apply-templates select="/" mode="M24"/>
      <xsl:apply-templates select="/" mode="M25"/>
      <xsl:apply-templates select="/" mode="M26"/>
      <xsl:apply-templates select="/" mode="M27"/>
      <xsl:apply-templates select="/" mode="M28"/>
      <xsl:apply-templates select="/" mode="M29"/>
      <xsl:apply-templates select="/" mode="M30"/>
      <xsl:apply-templates select="/" mode="M31"/>
      <xsl:apply-templates select="/" mode="M32"/>
      <xsl:apply-templates select="/" mode="M33"/>
      <xsl:apply-templates select="/" mode="M34"/>
      <xsl:apply-templates select="/" mode="M35"/>
      <xsl:apply-templates select="/" mode="M36"/>
      <xsl:apply-templates select="/" mode="M37"/>
      <xsl:apply-templates select="/" mode="M38"/>
      <xsl:apply-templates select="/" mode="M39"/>
      <xsl:apply-templates select="/" mode="M40"/>
      <xsl:apply-templates select="/" mode="M41"/>
      <xsl:apply-templates select="/" mode="M42"/>
      <xsl:apply-templates select="/" mode="M43"/>
   </xsl:template>

   <!--SCHEMATRON PATTERNS-->


<!--PATTERN code-list-rules-->


	<!--RULE -->
<xsl:template match="nc:BinaryFormatText" priority="1000" mode="M0">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="( false() or ( contains('&#127;application/json&#127;application/msword&#127;application/pdf&#127;application/vnd.oasis.opendocument.text&#127;application/vnd.openxmlformats-officedocument.wordprocessingml.document&#127;application/xml&#127;',concat('&#127;',.,'&#127;')) ) ) "/>
         <xsl:otherwise>
            <xsl:message>Invalid binary format code value. (( false() or ( contains('&#127;application/json&#127;application/msword&#127;application/pdf&#127;application/vnd.oasis.opendocument.text&#127;application/vnd.openxmlformats-officedocument.wordprocessingml.document&#127;application/xml&#127;',concat('&#127;',.,'&#127;')) ) ))</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M0"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M0"/>
   <xsl:template match="@*|node()" priority="-2" mode="M0">
      <xsl:apply-templates select="*" mode="M0"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="nc:BinaryFormatText" priority="1000" mode="M0">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="( false() or ( contains('&#127;application/json&#127;application/msword&#127;application/pdf&#127;application/vnd.oasis.opendocument.text&#127;application/vnd.openxmlformats-officedocument.wordprocessingml.document&#127;application/xml&#127;',concat('&#127;',.,'&#127;')) ) ) "/>
         <xsl:otherwise>
            <xsl:message>Invalid binary format code value. (( false() or ( contains('&#127;application/json&#127;application/msword&#127;application/pdf&#127;application/vnd.oasis.opendocument.text&#127;application/vnd.openxmlformats-officedocument.wordprocessingml.document&#127;application/xml&#127;',concat('&#127;',.,'&#127;')) ) ))</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M0"/>
   </xsl:template>

   <!--PATTERN ECF-->


	<!--RULE -->
<xsl:template match="/filing:FilingMessage" priority="1000" mode="M16">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="./nc:DocumentIdentification/nc:IdentificationID and string-length(normalize-space(./nc:DocumentIdentification/nc:IdentificationID)) &gt; 0 and not(./nc:DocumentIdentification/@xsi:nil = 'true')"/>
         <xsl:otherwise>
            <xsl:message>ECF 6.2.4 Message and Filing Identifiers: DocumentID must be present. (./nc:DocumentIdentification/nc:IdentificationID and string-length(normalize-space(./nc:DocumentIdentification/nc:IdentificationID)) &gt; 0 and not(./nc:DocumentIdentification/@xsi:nil = 'true'))</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M16"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M16"/>
   <xsl:template match="@*|node()" priority="-2" mode="M16">
      <xsl:apply-templates select="*" mode="M16"/>
   </xsl:template>

   <!--PATTERN InitialFiling-->


	<!--RULE -->
<xsl:template match="//filing:FilingLeadDocument/ecf:DocumentAugmentation[contains('Accusation,Indictment', ecf:RegisterActionDescriptionCode) and ../ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'false']"
                 priority="1000"
                 mode="M17">
      <xsl:variable name="docType" select="./ecf:RegisterActionDescriptionCode"/>

		    <!--REPORT -->
<xsl:if test="../../nc:Case/nc:CaseDocketID and ../../nc:Case/nc:CaseDocketID/@xsi:nil = false">
         <xsl:message>Initial <xsl:text/>
            <xsl:value-of select="$docType"/>
            <xsl:text/> filing cannot contain nc:Case/nc:CaseDocketID (../../nc:Case/nc:CaseDocketID and ../../nc:Case/nc:CaseDocketID/@xsi:nil = false)</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="../../nc:Case/nc:ActivityDateRange">
         <xsl:message>Initial <xsl:text/>
            <xsl:value-of select="$docType"/>
            <xsl:text/> filing cannot contain nc:Case/nc:ActivityDateRange (../../nc:Case/nc:ActivityDateRange)</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="../../nc:Case/nc:ActivityStatus">
         <xsl:message>Initial <xsl:text/>
            <xsl:value-of select="$docType"/>
            <xsl:text/> filing cannot contain nc:Case/nc:ActivityStatus (../../nc:Case/nc:ActivityStatus)</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseProsecutionAttorney/nc:RoleOfPerson/ecf:FilingAttorneyID/nc:IdentificationID)) &gt; 0">
         <xsl:message>Initial <xsl:text/>
            <xsl:value-of select="$docType"/>
            <xsl:text/> filing cannot contain value for j:CaseProsecutionAttorney/ecf:FilingAttorneyID (string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseProsecutionAttorney/nc:RoleOfPerson/ecf:FilingAttorneyID/nc:IdentificationID)) &gt; 0)</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseDefendantParty/nc:EntityPerson/ecf:FilingAttorneyID/nc:IdentificationID)) &gt; 0">
         <xsl:message>Initial <xsl:text/>
            <xsl:value-of select="$docType"/>
            <xsl:text/> filing cannot contain value for j:CaseProsecutionAttorney/ecf:FilingAttorneyID (string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseDefendantParty/nc:EntityPerson/ecf:FilingAttorneyID/nc:IdentificationID)) &gt; 0)</xsl:message>
      </xsl:if>
      <xsl:apply-templates select="*" mode="M17"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M17"/>
   <xsl:template match="@*|node()" priority="-2" mode="M17">
      <xsl:apply-templates select="*" mode="M17"/>
   </xsl:template>

   <!--PATTERN InitialFilingDefendantClass-->


	<!--RULE -->
<xsl:template match="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Accusation' or ecf:RegisterActionDescriptionCode = 'Indictment']"
                 priority="1001"
                 mode="M18">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="../../nc:Case/j:CaseAugmentation/j:CaseCharge/criminal:ChargeAugmentation/j:PersonChargeAssociation/nc:Person[@s:ref]"/>
         <xsl:otherwise>
            <xsl:message>For most exchanges, ECF 5.2 requires the presence of j:PersonChargeAssociation so the offense references back to the defendant to which this charge offense applies. The person is a reference using the ref attribute. (../../nc:Case/j:CaseAugmentation/j:CaseCharge/criminal:ChargeAugmentation/j:PersonChargeAssociation/nc:Person[@s:ref])</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="../../nc:Case/criminal:CaseAugmentation/j:Arrest/j:ArrestSubject/nc:RoleOfPerson[@s:ref]"/>
         <xsl:otherwise>
            <xsl:message>ECF is structured such that the Defendant is referenced by the case as opposed to the way depicted in the class diagrams and as such has been mapped for consistency with the ECF as opposed to extending. For this reason, the association is mapped from the arrest to the defendant with xsi:nil="true" and ref pointing to the defendant id. (../../nc:Case/criminal:CaseAugmentation/j:Arrest/j:ArrestSubject/nc:RoleOfPerson[@s:ref])</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="exists(../../nc:Case/criminal:CaseAugmentation/j:Arrest)"/>
         <xsl:otherwise>
            <xsl:message>The association DefendantArrest can only be present in an Accusation or Indictment. (exists(../../nc:Case/criminal:CaseAugmentation/j:Arrest))</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="exists(../../nc:Case/criminal:CaseAugmentation/j:Arrest/j:ArrestAgency/j:OrganizationAugmentation/j:OrganizationORIIdentification) or string-length(normalize-space(./j:OrganizationORIIdentification/nc:IdentificationID)) &gt; 0"/>
         <xsl:otherwise>
            <xsl:message>Element <xsl:text/>
               <xsl:value-of select="name(.)"/>
               <xsl:text/> in an Accusation or Indictment must contain the child elements j:OrganizationORIIdentification/nc:IdentificationID. (exists(../../nc:Case/criminal:CaseAugmentation/j:Arrest/j:ArrestAgency/j:OrganizationAugmentation/j:OrganizationORIIdentification) or string-length(normalize-space(./j:OrganizationORIIdentification/nc:IdentificationID)) &gt; 0)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M18"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode != 'Accusation' or ecf:RegisterActionDescriptionCode != 'Indictment']"
                 priority="1000"
                 mode="M18">

		<!--REPORT -->
<xsl:if test="exists(../../nc:Case/criminal:CaseAugmentation/j:Arrest)">
         <xsl:message>The association DefendantArrest must only be present in an Accusation or Indictment. (exists(../../nc:Case/criminal:CaseAugmentation/j:Arrest))</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="exists(../../nc:Case/criminal:CaseAugmentation/j:Arrest/j:ArrestAgency/j:OrganizationAugmentation/j:OrganizationORIIdentification) or string-length(normalize-space(./j:OrganizationORIIdentification/nc:IdentificationID)) &gt; 0">
         <xsl:message>Element <xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/> in an Accusation or Indictment must contain the child elements j:OrganizationORIIdentification/nc:IdentificationID. (exists(../../nc:Case/criminal:CaseAugmentation/j:Arrest/j:ArrestAgency/j:OrganizationAugmentation/j:OrganizationORIIdentification) or string-length(normalize-space(./j:OrganizationORIIdentification/nc:IdentificationID)) &gt; 0)</xsl:message>
      </xsl:if>
      <xsl:apply-templates select="*" mode="M18"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M18"/>
   <xsl:template match="@*|node()" priority="-2" mode="M18">
      <xsl:apply-templates select="*" mode="M18"/>
   </xsl:template>

   <!--PATTERN IndictmentInitialFiling-->


	<!--RULE -->
<xsl:template match="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Indictment']"
                 priority="1000"
                 mode="M19">

		<!--REPORT -->
<xsl:if test="ecf:DocumentRendition/nc:Attachment">
         <xsl:message>Must not have attached binary document (ecf:DocumentRendition/nc:Attachment)</xsl:message>
      </xsl:if>
      <xsl:apply-templates select="*" mode="M19"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M19"/>
   <xsl:template match="@*|node()" priority="-2" mode="M19">
      <xsl:apply-templates select="*" mode="M19"/>
   </xsl:template>

   <!--PATTERN IndictmentPersonAugmentation-->


	<!--RULE -->
<xsl:template match="filing:FilingMessage/nc:Case/j:CaseAugmentation//j:CaseProsecutionAttorney/nc:RoleOfPerson"
                 priority="1000"
                 mode="M20">

		<!--REPORT -->
<xsl:if test="(../../../../filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Indictment']) and exists(./ecf:PersonAugmentation)">
         <xsl:message>Element <xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/> in an Indictment must not contain the child element /ecf:PersonAugmentation. ((../../../../filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Indictment']) and exists(./ecf:PersonAugmentation))</xsl:message>
      </xsl:if>
      <xsl:apply-templates select="*" mode="M20"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M20"/>
   <xsl:template match="@*|node()" priority="-2" mode="M20">
      <xsl:apply-templates select="*" mode="M20"/>
   </xsl:template>

   <!--PATTERN AccusationInitialFiling-->


	<!--RULE -->
<xsl:template match="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Accusation' and ../ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'False']"
                 priority="1000"
                 mode="M21">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="ecf:DocumentRendition/nc:Attachment"/>
         <xsl:otherwise>
            <xsl:message>Must have attached binary document (ecf:DocumentRendition/nc:Attachment)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="../ga:FilingDocumentAugmentation/ga:DocumentSealedIndicator"/>
         <xsl:otherwise>
            <xsl:message>Must have document sealed indicator (../ga:FilingDocumentAugmentation/ga:DocumentSealedIndicator)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="../ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator"/>
         <xsl:otherwise>
            <xsl:message>Accusation filing must always include element DocumentAmendedIndicator. (../ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--REPORT -->
<xsl:if test="../../nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/j:OriginalChargeAmendedChargeAssociation">
         <xsl:message>Initial filing must not have amended charges. (../../nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/j:OriginalChargeAmendedChargeAssociation)</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="../../nc:Case/nc:CaseDocketID">
         <xsl:message>An initial Accusation cannot include a case number. (../../nc:Case/nc:CaseDocketID)</xsl:message>
      </xsl:if>
      <xsl:apply-templates select="*" mode="M21"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M21"/>
   <xsl:template match="@*|node()" priority="-2" mode="M21">
      <xsl:apply-templates select="*" mode="M21"/>
   </xsl:template>

   <!--PATTERN AccusationAmendedCharges-->


	<!--RULE -->
<xsl:template match="filing:FilingLeadDocument/ga:FilingDocumentAugmentation[ga:DocumentAmendedIndicator = 'True']"
                 priority="1001"
                 mode="M22">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="ecf:DocumentRendition/nc:Attachment"/>
         <xsl:otherwise>
            <xsl:message>Must have attached binary document (ecf:DocumentRendition/nc:Attachment)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="../ga:FilingDocumentAugmentation/ga:DocumentSealedIndicator"/>
         <xsl:otherwise>
            <xsl:message>Must have document sealed indicator (../ga:FilingDocumentAugmentation/ga:DocumentSealedIndicator)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="../ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator"/>
         <xsl:otherwise>
            <xsl:message>Amended Charges must have Accusation Augmentation (../ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="../../nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/j:OriginalChargeAmendedChargeAssociation"/>
         <xsl:otherwise>
            <xsl:message>If amending a charge, amended charge must be associated with new charge via the Amended Charge Relationship. (../../nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/j:OriginalChargeAmendedChargeAssociation)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="../../nc:Case/j:CaseAugmentation/j:CaseCharge/j:ChargeStatute/ga:OffenseTrackingNumberIdentification/nc:IdentificationID"/>
         <xsl:otherwise>
            <xsl:message>For a new charge on an existing case (adding a charge), the OTN should be included. (../../nc:Case/j:CaseAugmentation/j:CaseCharge/j:ChargeStatute/ga:OffenseTrackingNumberIdentification/nc:IdentificationID)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="../../nc:Case/nc:CaseDocketID"/>
         <xsl:otherwise>
            <xsl:message>For a new charge on an existing case (adding a charge), the case number should be included. (../../nc:Case/nc:CaseDocketID)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="docType"
                    select="../ecf:DocumentAugmentation/ecf:RegisterActionDescriptionCode"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseProsecutionAttorney/nc:RoleOfPerson/ecf:FilingAttorneyID/nc:IdentificationID)) &gt; 0"/>
         <xsl:otherwise>
            <xsl:message>Initial <xsl:text/>
               <xsl:value-of select="$docType"/>
               <xsl:text/> filing cannot contain value for j:CaseProsecutionAttorney/ecf:FilingAttorneyID (string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseProsecutionAttorney/nc:RoleOfPerson/ecf:FilingAttorneyID/nc:IdentificationID)) &gt; 0)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseDefendantParty/nc:EntityPerson/ecf:FilingAttorneyID/nc:IdentificationID)) &gt; 0"/>
         <xsl:otherwise>
            <xsl:message>Initial <xsl:text/>
               <xsl:value-of select="$docType"/>
               <xsl:text/> filing cannot contain value for j:CaseProsecutionAttorney/ecf:FilingAttorneyID (string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseDefendantParty/nc:EntityPerson/ecf:FilingAttorneyID/nc:IdentificationID)) &gt; 0)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M22"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode != 'Accusation' or ecf:RegisterActionDescriptionCode != 'Indictment']"
                 priority="1000"
                 mode="M22">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="exists(../../nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/j:OriginalChargeAmendedChargeAssociation)"/>
         <xsl:otherwise>
            <xsl:message>Filings other than Accusations or Indictments cannot have association AssociateAmendedCharge. (exists(../../nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/j:OriginalChargeAmendedChargeAssociation))</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M22"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M22"/>
   <xsl:template match="@*|node()" priority="-2" mode="M22">
      <xsl:apply-templates select="*" mode="M22"/>
   </xsl:template>

   <!--PATTERN AccusationAmendedCharges2-->


	<!--RULE -->
<xsl:template match="filing:FilingLeadDocument[nc:DocumentCategoryText='Accusation' or nc:DocumentCategoryText = 'Indictment']"
                 priority="1000"
                 mode="M23">

		<!--REPORT -->
<xsl:if test="exists(../filing:FilingMessage/nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/j:OriginalChargeAmendedChargeAssociation/j:AmendedCharge/j:ChargeTrackingIdentification/nc:IdentificationID)">
         <xsl:message>If an additional charge is presented that was not part of the original booking event and not associated with an existing OTN, the prosecutor's CMS must initiate a CCH Charge Update to acquire a new CTN. (exists(../filing:FilingMessage/nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/j:OriginalChargeAmendedChargeAssociation/j:AmendedCharge/j:ChargeTrackingIdentification/nc:IdentificationID))</xsl:message>
      </xsl:if>
      <xsl:apply-templates select="*" mode="M23"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M23"/>
   <xsl:template match="@*|node()" priority="-2" mode="M23">
      <xsl:apply-templates select="*" mode="M23"/>
   </xsl:template>

   <!--PATTERN CommonFilingsAttorneyClass-->


	<!--RULE -->
<xsl:template match="//ecf:AttorneyID[not(contains('Indictment,Accusation', ancestor::filing:FilingMessage/filing:FilingLeadDocument/ecf:DocumentAugmentation/ecf:RegisterActionDescriptionCode))]"
                 priority="1004"
                 mode="M24">
      <xsl:variable name="doc" select="ancestor::filing:FilingMessage/filing:FilingLeadDocument"/>
      <xsl:variable name="docType"
                    select="$doc/ecf:DocumentAugmentation/ecf:RegisterActionDescriptionCode"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="string-length(normalize-space(./nc:IdentificationID))"/>
         <xsl:otherwise>
            <xsl:message>
               <xsl:text/>
               <xsl:value-of select="$docType"/>
               <xsl:text/> filing must have value in <xsl:text/>
               <xsl:value-of select="name(.)"/>
               <xsl:text/>/nc:IdentificationID (string-length(normalize-space(./nc:IdentificationID)))</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M24"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="filing:FilingMessage/nc:Case/j:CaseAugmentation/j:CaseProsecutionAttorney"
                 priority="1003"
                 mode="M24">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="exists(nc:RoleOfPerson/@s:id)"/>
         <xsl:otherwise>
            <xsl:message>
               <xsl:text/>
               <xsl:value-of select="name(.)"/>
               <xsl:text/> child element nc:RoleOfPerson must contain attribute structures:id. (exists(nc:RoleOfPerson/@s:id))</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M24"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="filing:FilingMessage/nc:Case/j:CaseAugmentation/j:CaseDefenseAttorney"
                 priority="1002"
                 mode="M24">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="exists(nc:RoleOfPerson/@s:id)"/>
         <xsl:otherwise>
            <xsl:message>
               <xsl:text/>
               <xsl:value-of select="name(.)"/>
               <xsl:text/>Element j:CaseDefenseAttorney/nc:RoleOfPerson must contain attribute structures:id. (exists(nc:RoleOfPerson/@s:id))</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M24"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Petition']"
                 priority="1001"
                 mode="M24">

		<!--REPORT -->
<xsl:if test="exists(../../nc:Case/j:CaseAugmentation/j:CaseProsecutionAttorney)">
         <xsl:message>A probation violation petition must not contain element j:CaseProsecutionAttorney. (exists(../../nc:Case/j:CaseAugmentation/j:CaseProsecutionAttorney))</xsl:message>
      </xsl:if>
      <xsl:apply-templates select="*" mode="M24"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Accusation' or ecf:RegisterActionDescriptionCode = 'Appearance' or ecf:RegisterActionDescriptionCode = 'Indictment' or ecf:RegisterActionDescriptionCode = 'Motion' or ecf:RegisterActionDescriptionCode = 'Warrant']"
                 priority="1000"
                 mode="M24">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="exists(../../nc:Case/j:CaseAugmentation/j:CaseProsecutionAttorney)"/>
         <xsl:otherwise>
            <xsl:message>All filings except probation violation petitions must include element j:CaseProsecutionAttorney. (exists(../../nc:Case/j:CaseAugmentation/j:CaseProsecutionAttorney))</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M24"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M24"/>
   <xsl:template match="@*|node()" priority="-2" mode="M24">
      <xsl:apply-templates select="*" mode="M24"/>
   </xsl:template>

   <!--PATTERN CommonFilingsAttorneyRepresentedParty-->


	<!--RULE -->
<xsl:template match="//j:CaseDefenseAttorney" priority="1001" mode="M25">
      <xsl:variable name="docType"
                    select="../../../filing:FilingLeadDocument/ecf:DocumentAugmentation/ecf:RegisterActionDescriptionCode"/>
      <xsl:variable name="defRef"
                    select="./ecf:CaseOfficialAugmentation/ecf:CaseRepresentedParty/nc:EntityPerson/@s:ref"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="./ecf:CaseOfficialAugmentation/ecf:CaseRepresentedParty/nc:EntityPerson/@s:ref"/>
         <xsl:otherwise>
            <xsl:message>For <xsl:text/>
               <xsl:value-of select="$docType"/>
               <xsl:text/>, when the Role for the Attorney is Defendant, this element is required. When the Role for the Attorney is Prosecutor, this element is not required. (./ecf:CaseOfficialAugmentation/ecf:CaseRepresentedParty/nc:EntityPerson/@s:ref)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="..//j:CaseDefendantParty/nc:EntityPerson[@structures:id =$defRef]"/>
         <xsl:otherwise>
            <xsl:message>Represented party must point to a defendant (..//j:CaseDefendantParty/nc:EntityPerson[@structures:id =$defRef])</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M25"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="//j:CaseProsecutingAttorney" priority="1000" mode="M25">

		<!--REPORT -->
<xsl:if test="./ecf:CaseOfficialAugmentation/ecf:CaseRepresentedParty">
         <xsl:message>
            <xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/> must not represent any party specified in the filing (./ecf:CaseOfficialAugmentation/ecf:CaseRepresentedParty)</xsl:message>
      </xsl:if>
      <xsl:apply-templates select="*" mode="M25"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M25"/>
   <xsl:template match="@*|node()" priority="-2" mode="M25">
      <xsl:apply-templates select="*" mode="M25"/>
   </xsl:template>

   <!--PATTERN CommonFilingsCaseStatus-->


	<!--RULE -->
<xsl:template match="//filing:FilingLeadDocument/ecf:DocumentAugmentation[(ecf:RegisterActionDescriptionCode = 'Accusation' and ../ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true') or (ecf:RegisterActionDescriptionCode != 'Indictment' and ecf:RegisterActionDescriptionCode != 'Accusation')]"
                 priority="1000"
                 mode="M26">

		<!--REPORT -->
<xsl:if test="../../nc:Case/nc:ActivityStatus/nc:StatusDescriptionText">
         <xsl:message>The Case Status attribute in every filing except for an Indictment must exist. (../../nc:Case/nc:ActivityStatus/nc:StatusDescriptionText)</xsl:message>
      </xsl:if>
      <xsl:variable name="caseNumber" select="../../nc:Case/nc:CaseDocketID"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="string-length(normalize-space(../../nc:Case/nc:CaseDocketID)) &gt; 0"/>
         <xsl:otherwise>
            <xsl:message>The Court Case Number must exist in every filing except for initial filings (i.e. Accusations and Indictments) and can never contain attribute xsi:nil = 'true'. (string-length(normalize-space(../../nc:Case/nc:CaseDocketID)) &gt; 0)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M26"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M26"/>
   <xsl:template match="@*|node()" priority="-2" mode="M26">
      <xsl:apply-templates select="*" mode="M26"/>
   </xsl:template>

   <!--PATTERN CommonFilingsCaseType-->


	<!--RULE -->
<xsl:template match="filing:FilingLeadDocument/ecf:DocumentAugmentation" priority="1000"
                 mode="M27">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="exists(../../nc:Case/ecf:CaseAugmentation/ecf:CaseCategoryCode) and ../../nc:Case/ecf:CaseAugmentation/ecf:CaseCategoryCode = 'criminal'"/>
         <xsl:otherwise>
            <xsl:message>For CJE, the Case Type attribute in every filing must exist and contain a value of 'criminal' (case sensitive). (exists(../../nc:Case/ecf:CaseAugmentation/ecf:CaseCategoryCode) and ../../nc:Case/ecf:CaseAugmentation/ecf:CaseCategoryCode = 'criminal')</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M27"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M27"/>
   <xsl:template match="@*|node()" priority="-2" mode="M27">
      <xsl:apply-templates select="*" mode="M27"/>
   </xsl:template>

   <!--PATTERN CommonFilingChargeClass-->


	<!--RULE -->
<xsl:template match="//j:CaseCharge | //j:Charge" priority="1000" mode="M28">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="./@s:id"/>
         <xsl:otherwise>
            <xsl:message>Each charge must have attribute structures:id assigned. (./@s:id)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M28"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M28"/>
   <xsl:template match="@*|node()" priority="-2" mode="M28">
      <xsl:apply-templates select="*" mode="M28"/>
   </xsl:template>

   <!--PATTERN CommonFilingChargeClassIDNumbers-->


	<!--RULE -->
<xsl:template match="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Accusation' or ecf:RegisterActionDescriptionCode = 'Indictment']"
                 priority="1001"
                 mode="M29">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseCharge/j:ChargeStatute/ga:OffenseTrackingNumberIdentification/nc:IdentificationID)) &gt; 0                                                       or ../../nc:Case/j:CaseAugmentation/j:CaseCharge/j:ChargeStatute/ga:OffenseTrackingNumberIdentification/@xsi:nil = 'true'"/>
         <xsl:otherwise>
            <xsl:message>Must either contain child element IdentificationID with a valid value or have attribute xsi:nil="true" if OTN value is empty or not given by GCIC for new charges not associated with an existing OTN. (string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseCharge/j:ChargeStatute/ga:OffenseTrackingNumberIdentification/nc:IdentificationID)) &gt; 0 or ../../nc:Case/j:CaseAugmentation/j:CaseCharge/j:ChargeStatute/ga:OffenseTrackingNumberIdentification/@xsi:nil = 'true')</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseCharge/j:ChargeTrackingIdentification/nc:IdentificationID)) &gt; 0                                                       or ../../nc:Case/j:CaseAugmentation/j:CaseCharge/j:ChargeTrackingIdentification/@xsi:nil = 'true'"/>
         <xsl:otherwise>
            <xsl:message>Must either contain child element IdentificationID with a valid value or have attribute xsi:nil="true" if CTN value is empty or not given by GCIC for new charges not associated with an existing CTN. (string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseCharge/j:ChargeTrackingIdentification/nc:IdentificationID)) &gt; 0 or ../../nc:Case/j:CaseAugmentation/j:CaseCharge/j:ChargeTrackingIdentification/@xsi:nil = 'true')</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M29"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="//ga:ViolationFilingAugmentation//j:Charge" priority="1000" mode="M29">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="string-length(normalize-space(./j:ChargeStatute/ga:OffenseTrackingNumberIdentification/nc:IdentificationID)) &gt; 0                  or ./j:ChargeStatute/ga:OffenseTrackingNumberIdentification/@xsi:nil = 'true'"/>
         <xsl:otherwise>
            <xsl:message>Must either contain child element IdentificationID with a valid value or have attribute xsi:nil="true" if OTN value is empty or not given by GCIC for new charges not associated with an existing OTN. (string-length(normalize-space(./j:ChargeStatute/ga:OffenseTrackingNumberIdentification/nc:IdentificationID)) &gt; 0 or ./j:ChargeStatute/ga:OffenseTrackingNumberIdentification/@xsi:nil = 'true')</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="string-length(normalize-space(./j:ChargeTrackingIdentification/nc:IdentificationID)) &gt; 0                  or //ga:ViolationFilingAugmentation//j:Charge/j:ChargeTrackingIdentification/@xsi:nil = 'true'"/>
         <xsl:otherwise>
            <xsl:message>Must either contain child element IdentificationID with a valid value or have attribute xsi:nil="true" if CTN value is empty or not given by GCIC for new charges not associated with an existing CTN. (string-length(normalize-space(./j:ChargeTrackingIdentification/nc:IdentificationID)) &gt; 0 or //ga:ViolationFilingAugmentation//j:Charge/j:ChargeTrackingIdentification/@xsi:nil = 'true')</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M29"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M29"/>
   <xsl:template match="@*|node()" priority="-2" mode="M29">
      <xsl:apply-templates select="*" mode="M29"/>
   </xsl:template>

   <!--PATTERN CommonFilingChargeClassGCICandWarrant-->


	<!--RULE -->
<xsl:template match="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Accusation' or ecf:RegisterActionDescriptionCode = 'Indictment']"
                 priority="1001"
                 mode="M30">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/ga:ChargeWarrantNumber)) &gt; 0                  or ../../nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/ga:ChargeWarrantNumber/@xsi:nil = 'true'"/>
         <xsl:otherwise>
            <xsl:message>Warrant number must either contain a value or include xsi:nil="true" if no value exists. (string-length(normalize-space(../../nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/ga:ChargeWarrantNumber)) &gt; 0 or ../../nc:Case/j:CaseAugmentation/j:CaseCharge/ga:ChargeAugmentation/ga:ChargeWarrantNumber/@xsi:nil = 'true')</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M30"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="//ga:ViolationFilingAugmentation//j:Charge" priority="1000" mode="M30">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="string-length(normalize-space(./ga:ChargeAugmentation/ga:ChargeWarrantNumber)) &gt; 0                  or ./ga:ChargeAugmentation/ga:ChargeWarrantNumber/@xsi:nil = 'true'"/>
         <xsl:otherwise>
            <xsl:message>Warrant number must either contain a value or include xsi:nil="true" if no value exists. (string-length(normalize-space(./ga:ChargeAugmentation/ga:ChargeWarrantNumber)) &gt; 0 or ./ga:ChargeAugmentation/ga:ChargeWarrantNumber/@xsi:nil = 'true')</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M30"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M30"/>
   <xsl:template match="@*|node()" priority="-2" mode="M30">
      <xsl:apply-templates select="*" mode="M30"/>
   </xsl:template>

   <!--PATTERN ContactInformationAssociation-->


	<!--RULE -->
<xsl:template match="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Accusation' or ecf:RegisterActionDescriptionCode = 'Indictment' or ecf:RegisterActionDescriptionCode = 'Appearance']"
                 priority="1001"
                 mode="M31">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="exists(../../nc:Case/j:CaseAugmentation/j:CaseDefendantParty/nc:EntityPerson/ecf:PersonAugmentation/nc:ContactInformationAssociation)"/>
         <xsl:otherwise>
            <xsl:message>The association PersonContactInformation must be present in Accusations, Indictments, and Appearances. (exists(../../nc:Case/j:CaseAugmentation/j:CaseDefendantParty/nc:EntityPerson/ecf:PersonAugmentation/nc:ContactInformationAssociation))</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M31"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Motion' or ecf:RegisterActionDescriptionCode = 'Petition' or ecf:RegisterActionDescriptionCode = 'Warrant']"
                 priority="1000"
                 mode="M31">

		<!--REPORT -->
<xsl:if test="exists(../../nc:Case/j:CaseAugmentation/j:CaseDefendantParty/nc:EntityPerson/ecf:PersonAugmentation/nc:ContactInformationAssociation)">
         <xsl:message>The association PersonContactInformation can only be present in Accusations, Indictments, and Appearances. (exists(../../nc:Case/j:CaseAugmentation/j:CaseDefendantParty/nc:EntityPerson/ecf:PersonAugmentation/nc:ContactInformationAssociation))</xsl:message>
      </xsl:if>
      <xsl:apply-templates select="*" mode="M31"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M31"/>
   <xsl:template match="@*|node()" priority="-2" mode="M31">
      <xsl:apply-templates select="*" mode="M31"/>
   </xsl:template>

   <!--PATTERN CaseCourt-->


	<!--RULE -->
<xsl:template match="filing:FilingMessage//j:CaseCourt" priority="1000" mode="M32">
      <xsl:variable name="courtCategory" select="./ga:CourtCategoryCode"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="(exists($courtCategory) and string-length(normalize-space($courtCategory)) &gt; 0)"/>
         <xsl:otherwise>
            <xsl:message>Element <xsl:text/>
               <xsl:value-of select="name(.)"/>
               <xsl:text/> must include a value in /ga:CourtCategoryCode for all filings. ((exists($courtCategory) and string-length(normalize-space($courtCategory)) &gt; 0))</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="countyCode"
                    select="./nc:OrganizationIdentification/nc:IdentificationJurisdiction/ga:JurisdictionCountyCode"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="(exists($countyCode) and string-length(normalize-space($countyCode)) &gt; 0)"/>
         <xsl:otherwise>
            <xsl:message>Element <xsl:text/>
               <xsl:value-of select="name(.)"/>
               <xsl:text/> must include a value in /nc:OrganizationIdentification/nc:IdentificationJurisdiction/ga:JurisdictionCountyCode for all filings. ((exists($countyCode) and string-length(normalize-space($countyCode)) &gt; 0))</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M32"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M32"/>
   <xsl:template match="@*|node()" priority="-2" mode="M32">
      <xsl:apply-templates select="*" mode="M32"/>
   </xsl:template>

   <!--PATTERN GeneralRestrictionRules-->


	<!--RULE -->
<xsl:template match="//filing:FilingConnectedDocument" priority="1004" mode="M33">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="./@s:id"/>
         <xsl:otherwise>
            <xsl:message>Connected document must have id. (./@s:id)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="string-length(normalize-space(ecf:DocumentAugmentation/nc:DocumentAssociation/ecf:DocumentAssociationAugmentation/ecf:DocumentRelatedCode)) &gt; 0"/>
         <xsl:otherwise>
            <xsl:message>Must have a valid code value for the association (string-length(normalize-space(ecf:DocumentAugmentation/nc:DocumentAssociation/ecf:DocumentAssociationAugmentation/ecf:DocumentRelatedCode)) &gt; 0)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="parentID" select="../filing:FilingLeadDocument/@s:id"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="ecf:DocumentAugmentation/nc:DocumentAssociation/nc:PrimaryDocument/@s:ref = $parentID"/>
         <xsl:otherwise>
            <xsl:message>Connected document must point back to lead document (ecf:DocumentAugmentation/nc:DocumentAssociation/nc:PrimaryDocument/@s:ref = $parentID)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="ecf:DocumentAugmentation/nc:DocumentAssociation/ecf:DocumentAssociationAugmentation/ecf:DocumentRelatedCode"/>
         <xsl:otherwise>
            <xsl:message> (ecf:DocumentAugmentation/nc:DocumentAssociation/ecf:DocumentAssociationAugmentation/ecf:DocumentRelatedCode)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="docType"
                    select="../filing:FilingLeadDocument/ecf:DocumentAugmentation/ecf:RegisterActionDescriptionCode"/>

		    <!--REPORT -->
<xsl:if test="contains('Appearance,Indictment', $docType)">
         <xsl:message>
            <xsl:text/>
            <xsl:value-of select="$docType"/>
            <xsl:text/> filing cannot contain <xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/> (contains('Appearance,Indictment', $docType))</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="ga:FilingDocumentAugmentation/ga:DocumentStampedDate">
         <xsl:message>
            <xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/> cannot contain ga:FilingDocumentAugmentation/ga:DocumentStampedDate (ga:FilingDocumentAugmentation/ga:DocumentStampedDate)</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="nc:DocumentFileControlID">
         <xsl:message>
            <xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/> cannot contain nc:DocumentFileControlID (nc:DocumentFileControlID)</xsl:message>
      </xsl:if>
      <xsl:apply-templates select="*" mode="M33"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="//filing:FilingLeadDocument" priority="1003" mode="M33">
      <xsl:variable name="docType"
                    select="./ecf:DocumentAugmentation/ecf:RegisterActionDescriptionCode"/>

		    <!--REPORT -->
<xsl:if test="not(contains('Motion,Violation', $docType)) and ./ga:FilingDocumentAugmentation/ga:DocumentSubtype">
         <xsl:message>
            <xsl:text/>
            <xsl:value-of select="$docType"/>
            <xsl:text/> filing cannot contain <xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/>/ga:FilingDocumentAugmentation/ga:DocumentSubtype (not(contains('Motion,Violation', $docType)) and ./ga:FilingDocumentAugmentation/ga:DocumentSubtype)</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="not(contains('Motion,Violation', $docType)) and ./nc:DocumentDescriptionText">
         <xsl:message>
            <xsl:text/>
            <xsl:value-of select="$docType"/>
            <xsl:text/> filing cannot contain <xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/>/nc:DocumentDescriptionText (not(contains('Motion,Violation', $docType)) and ./nc:DocumentDescriptionText)</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="($docType = 'Indictment') and ./ecf:DocumentAugmentation/ecf:SpecialHandlingInstructionsText">
         <xsl:message>
            <xsl:text/>
            <xsl:value-of select="$docType"/>
            <xsl:text/> filing cannot contain <xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/>/ga:FilingDocumentAugmentation/ga:DocumentSealedIndicator (($docType = 'Indictment') and ./ecf:DocumentAugmentation/ecf:SpecialHandlingInstructionsText)</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="($docType = 'Indictment') and ./ga:FilingDocumentAugmentation/ga:DocumentSealedIndicator">
         <xsl:message>
            <xsl:text/>
            <xsl:value-of select="$docType"/>
            <xsl:text/> filing cannot contain <xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/>/ga:FilingDocumentAugmentation/ga:DocumentSealedIndicator (($docType = 'Indictment') and ./ga:FilingDocumentAugmentation/ga:DocumentSealedIndicator)</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="./ga:FilingDocumentAugmentation/ga:DocumentStampedDate">
         <xsl:message>
            <xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/> cannot contain ga:FilingDocumentAugmentation/ga:DocumentStampedDate (./ga:FilingDocumentAugmentation/ga:DocumentStampedDate)</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="./nc:DocumentFileControlID">
         <xsl:message>
            <xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/> cannot contain nc:DocumentFileControlID (./nc:DocumentFileControlID)</xsl:message>
      </xsl:if>
      <xsl:apply-templates select="*" mode="M33"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="//ecf:ReviewedLeadDocument | //ecf:ReviewedConnectedDocument"
                 priority="1002"
                 mode="M33">

		<!--REPORT -->
<xsl:if test="./ecf:DocumentAugmentation/ecf:SpecialHandlingInstructionsText">
         <xsl:message>
            <xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/> cannot contain ecf:DocumentAugmentation/ecf:SpecialHandlingInstructionsText (./ecf:DocumentAugmentation/ecf:SpecialHandlingInstructionsText)</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="./ga:FilingDocumentAugmentation/ga:DocumentSealedIndicator">
         <xsl:message>
            <xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/> cannot contain ga:FilingDocumentAugmentation/ga:DocumentSealedIndicator (./ga:FilingDocumentAugmentation/ga:DocumentSealedIndicator)</xsl:message>
      </xsl:if>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="./ga:FilingDocumentAugmentation/ga:DocumentStampedDate"/>
         <xsl:otherwise>
            <xsl:message>
               <xsl:text/>
               <xsl:value-of select="name(.)"/>
               <xsl:text/> must contain ga:FilingDocumentAugmentation/ga:DocumentStampedDate (./ga:FilingDocumentAugmentation/ga:DocumentStampedDate)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="./nc:DocumentFileControlID"/>
         <xsl:otherwise>
            <xsl:message>
               <xsl:text/>
               <xsl:value-of select="name(.)"/>
               <xsl:text/> must contain nc:DocumentFileControlID (./nc:DocumentFileControlID)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M33"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="//nc:RoleOfPerson[not(@xsi:nil) or @xsi:nil != 'true'] | //nc:EntityPerson[not(@xsi:nil) or @xsi:nil != 'true']"
                 priority="1001"
                 mode="M33">
      <xsl:variable name="parent" select="name(..)"/>
      <xsl:variable name="doc"
                    select="ancestor::filing:FilingMessage/filing:FilingLeadDocument | reviewfilingcallback:NotifyFilingReviewCompleteMessage/ecf:ReviewedLeadDocument"/>
      <xsl:variable name="docType"
                    select="$doc/ecf:DocumentAugmentation/ecf:RegisterActionDescriptionCode"/>

		    <!--REPORT -->
<xsl:if test="$parent != 'j:CaseDefendantParty' and ./nc:PersonTaxIdentification">
         <xsl:message>
            <xsl:text/>
            <xsl:value-of select="$parent"/>
            <xsl:text/>/<xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/> cannot contain nc:PersonTaxIdentification ($parent != 'j:CaseDefendantParty' and ./nc:PersonTaxIdentification)</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="(not(contains('Indictment,Accusation', $docType)) or (contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true')) and $parent = 'j:CaseDefendantParty' and ./nc:PersonTaxIdentification">
         <xsl:message>Any subsequent filing cannot contain <xsl:text/>
            <xsl:value-of select="$parent"/>
            <xsl:text/>/nc:PersonTaxIdentification ((not(contains('Indictment,Accusation', $docType)) or (contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true')) and $parent = 'j:CaseDefendantParty' and ./nc:PersonTaxIdentification)</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="$parent != 'j:CaseDefendantParty' and ./nc:PersonStateIdentification">
         <xsl:message>
            <xsl:text/>
            <xsl:value-of select="$parent"/>
            <xsl:text/>/<xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/> cannot contain nc:PersonStateIdentification ($parent != 'j:CaseDefendantParty' and ./nc:PersonStateIdentification)</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="(not(contains('Indictment,Accusation', $docType)) or (contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true')) and $parent = 'j:CaseDefendantParty' and ./nc:PersonStateIdentification">
         <xsl:message>Any subsequent filing cannot contain <xsl:text/>
            <xsl:value-of select="$parent"/>
            <xsl:text/>/nc:PersonStateIdentification ((not(contains('Indictment,Accusation', $docType)) or (contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true')) and $parent = 'j:CaseDefendantParty' and ./nc:PersonStateIdentification)</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="$parent != 'j:CaseDefendantParty' and (./j:PersonAugmentation or ./j:PersonAugmentation/j:PersonFBIIdentification)">
         <xsl:message>
            <xsl:text/>
            <xsl:value-of select="$parent"/>
            <xsl:text/>/<xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/> cannot contain j:PersonAugmentation/j:PersonFBIIdentification ($parent != 'j:CaseDefendantParty' and (./j:PersonAugmentation or ./j:PersonAugmentation/j:PersonFBIIdentification))</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="(not(contains('Indictment,Accusation', $docType)) or (contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true')) and $parent = 'j:CaseDefendantParty' and (./j:PersonAugmentation or ./j:PersonAugmentation/j:PersonFBIIdentification)">
         <xsl:message>Any subsequent filing cannot contain <xsl:text/>
            <xsl:value-of select="$parent"/>
            <xsl:text/>/j:PersonAugmentation/j:PersonFBIIdentification ((not(contains('Indictment,Accusation', $docType)) or (contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true')) and $parent = 'j:CaseDefendantParty' and (./j:PersonAugmentation or ./j:PersonAugmentation/j:PersonFBIIdentification))</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="$parent != 'j:CaseDefendantParty' and (./ga:PersonAugmentation or ./ga:PersonAugmentation/scr:InmateNumberID)">
         <xsl:message>
            <xsl:text/>
            <xsl:value-of select="$parent"/>
            <xsl:text/>/<xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/> cannot contain ga:PersonAugmentation/scr:InmateNumberID ($parent != 'j:CaseDefendantParty' and (./ga:PersonAugmentation or ./ga:PersonAugmentation/scr:InmateNumberID))</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="(not(contains('Indictment,Accusation', $docType)) or (contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true')) and $parent = 'j:CaseDefendantParty' and (./ga:PersonAugmentation or ./ga:PersonAugmentation/scr:InmateNumberID)">
         <xsl:message>Any subsequent filing cannot contain <xsl:text/>
            <xsl:value-of select="$parent"/>
            <xsl:text/>/ga:PersonAugmentation/scr:InmateNumberID ((not(contains('Indictment,Accusation', $docType)) or (contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true')) and $parent = 'j:CaseDefendantParty' and (./ga:PersonAugmentation or ./ga:PersonAugmentation/scr:InmateNumberID))</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="$parent != 'j:CaseDefendantParty' and ./nc:PersonBirthDate">
         <xsl:message>
            <xsl:text/>
            <xsl:value-of select="$parent"/>
            <xsl:text/>/<xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/> cannot contain nc:PersonBirthDate ($parent != 'j:CaseDefendantParty' and ./nc:PersonBirthDate)</xsl:message>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="(not(contains('Indictment,Accusation', $docType)) or (contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true')) and $parent = 'j:CaseDefendantParty' and ./nc:PersonBirthDate">
         <xsl:message>Any subsequent filing cannot contain <xsl:text/>
            <xsl:value-of select="$parent"/>
            <xsl:text/>/nc:PersonBirthDate ((not(contains('Indictment,Accusation', $docType)) or (contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true')) and $parent = 'j:CaseDefendantParty' and ./nc:PersonBirthDate)</xsl:message>
      </xsl:if>
      <xsl:apply-templates select="*" mode="M33"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="//nc:Case" priority="1000" mode="M33">
      <xsl:variable name="doc" select="../filing:FilingLeadDocument"/>
      <xsl:variable name="docType"
                    select="$doc/ecf:DocumentAugmentation/ecf:RegisterActionDescriptionCode"/>
      <xsl:variable name="part1" select="contains('Indictment,Accusation', $docType)"/>
      <xsl:variable name="part2"
                    select="$doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true'"/>
      <xsl:variable name="isAmendedAccusationOrIndictment"
                    select="contains('Indictment,Accusation', $docType) and $doc/ga:FilingDocumentAugmentation/ga:DocumentAmendedIndicator = 'true'"/>

		    <!--REPORT -->
<xsl:if test="contains('Motion,Appearance,Petition', $docType) and ./nc:CaseTrackingID">
         <xsl:message>$docType filing cannot contain <xsl:text/>
            <xsl:value-of select="name(.)"/>
            <xsl:text/>/nc:ActivityDateRange (contains('Motion,Appearance,Petition', $docType) and ./nc:CaseTrackingID)</xsl:message>
      </xsl:if>
      <xsl:apply-templates select="*" mode="M33"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M33"/>
   <xsl:template match="@*|node()" priority="-2" mode="M33">
      <xsl:apply-templates select="*" mode="M33"/>
   </xsl:template>

   <!--PATTERN CommonFilingDefendantClass-->


	<!--RULE -->
<xsl:template match="/filing:FilingMessage/nc:Case/j:CaseAugmentation/j:CaseCharge/criminal:ChargeAugmentation/j:PersonChargeAssociation/nc:Person"
                 priority="1000"
                 mode="M34">
      <xsl:variable name="reference" select="./@s:ref"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="./@s:ref"/>
         <xsl:otherwise>
            <xsl:message>ECF 5.2 requires the presence of j:PersonChargeAssociation so the offense references back to the defendant to which this charge offense applies. The person is a reference using the ref attribute. (./@s:ref)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="/filing:FilingMessage//j:CaseDefendantParty/nc:EntityPerson[@s:id = $reference]"/>
         <xsl:otherwise>
            <xsl:message>Charge association must point back to a defendant. (/filing:FilingMessage//j:CaseDefendantParty/nc:EntityPerson[@s:id = $reference])</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M34"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M34"/>
   <xsl:template match="@*|node()" priority="-2" mode="M34">
      <xsl:apply-templates select="*" mode="M34"/>
   </xsl:template>

   <!--PATTERN CommonEFilings-->


	<!--RULE -->
<xsl:template match="filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode != 'Accusation' or ecf:RegisterActionDescriptionCode != 'Indictment']"
                 priority="1001"
                 mode="M35">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="../@s:id"/>
         <xsl:otherwise>
            <xsl:message>Lead document must have id. (../@s:id)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M35"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="/filing:FilingMessage/ecf:FilingPartyID" priority="1000" mode="M35">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="string-length(normalize-space(./nc:IdentificationID)) &gt; 0"/>
         <xsl:otherwise>
            <xsl:message>There must be a value for <xsl:text/>
               <xsl:value-of select="name(.)"/>
               <xsl:text/> (string-length(normalize-space(./nc:IdentificationID)) &gt; 0)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="string-length(normalize-space(./nc:IdentificationID)) &gt; 0"/>
         <xsl:otherwise>
            <xsl:message> (string-length(normalize-space(./nc:IdentificationID)) &gt; 0)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M35"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M35"/>
   <xsl:template match="@*|node()" priority="-2" mode="M35">
      <xsl:apply-templates select="*" mode="M35"/>
   </xsl:template>

   <!--PATTERN ViolationAugmentation-->


	<!--RULE -->
<xsl:template match="filing:FilingMessage/filing:FilingLeadDocument[nc:DocumentCategoryText = 'Violation']"
                 priority="1000"
                 mode="M36">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="exists(../ga:ViolationFilingAugmentation)"/>
         <xsl:otherwise>
            <xsl:message>A probation violation petition or motion to modify probation must contain element ga:ViolationFilingAugmentation for the FilingAllegedViolation association. (exists(../ga:ViolationFilingAugmentation))</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M36"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M36"/>
   <xsl:template match="@*|node()" priority="-2" mode="M36">
      <xsl:apply-templates select="*" mode="M36"/>
   </xsl:template>

   <!--PATTERN OrganizationID-->


	<!--RULE -->
<xsl:template match="//nc:Organization" priority="1000" mode="M37">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="./@s:id"/>
         <xsl:otherwise>
            <xsl:message>Every organization element should have attribute structures:id assigned. (./@s:id)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M37"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M37"/>
   <xsl:template match="@*|node()" priority="-2" mode="M37">
      <xsl:apply-templates select="*" mode="M37"/>
   </xsl:template>

   <!--PATTERN CommonFilingsPersonClass-->


	<!--RULE -->
<xsl:template match="//j:CaseDefendantParty/nc:EntityPerson | //j:CaseProsecutionAttorney/nc:RoleOfPerson | //j:CaseDefenseAttorney/nc:RoleOfPerson | j:CaseOtherEntity/nc:EntityPerson"
                 priority="1000"
                 mode="M38">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="./@s:id"/>
         <xsl:otherwise>
            <xsl:message>
               <xsl:text/>
               <xsl:value-of select="name(.)"/>
               <xsl:text/> is missing attribute structures:id to the person element. (./@s:id)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M38"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M38"/>
   <xsl:template match="@*|node()" priority="-2" mode="M38">
      <xsl:apply-templates select="*" mode="M38"/>
   </xsl:template>

   <!--PATTERN CommonFilingPersonFullName-->


	<!--RULE -->
<xsl:template match="//nc:PersonName" priority="1000" mode="M39">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="(./nc:PersonFullName and ((not(./nc:PersonNamePrefixText) or (./nc:PersonNamePrefixText/@xsi:nil = 'true'))                                                   or (not(./nc:PersonGivenName) or (./nc:PersonGivenName/@xsi:nil = 'true'))                                                   or (not(./nc:PersonMiddleName) or (./nc:PersonMiddleName/@xsi:nil = 'true'))                                                   or (not(./nc:PersonSurName) or (./nc:PersonSurName/@xsi:nil = 'true'))                                                   or (not(./nc:PersonNameSuffixText) or (./nc:PersonNameSuffixText/@xsi:nil = 'true'))))                    or (not(./nc:PersonFullName) and ((not(./nc:PersonNamePrefixText/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonNamePrefixText)) &gt; 0)                                                   or (not(./nc:PersonGivenName/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonGivenName)) &gt; 0)                                                   or (not(./nc:PersonMiddleName/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonMiddleName)) &gt; 0)                                                   or (not(./nc:PersonSurName/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonSurName)) &gt; 0)                                                   or (not(./nc:PersonNameSuffixText/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonNameSuffixText)) &gt; 0)))"/>
         <xsl:otherwise>
            <xsl:message>When FullName is present in an instance, then first name, middle name, and last name will not be present and vice versa. ((./nc:PersonFullName and ((not(./nc:PersonNamePrefixText) or (./nc:PersonNamePrefixText/@xsi:nil = 'true')) or (not(./nc:PersonGivenName) or (./nc:PersonGivenName/@xsi:nil = 'true')) or (not(./nc:PersonMiddleName) or (./nc:PersonMiddleName/@xsi:nil = 'true')) or (not(./nc:PersonSurName) or (./nc:PersonSurName/@xsi:nil = 'true')) or (not(./nc:PersonNameSuffixText) or (./nc:PersonNameSuffixText/@xsi:nil = 'true')))) or (not(./nc:PersonFullName) and ((not(./nc:PersonNamePrefixText/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonNamePrefixText)) &gt; 0) or (not(./nc:PersonGivenName/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonGivenName)) &gt; 0) or (not(./nc:PersonMiddleName/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonMiddleName)) &gt; 0) or (not(./nc:PersonSurName/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonSurName)) &gt; 0) or (not(./nc:PersonNameSuffixText/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonNameSuffixText)) &gt; 0))))</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M39"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M39"/>
   <xsl:template match="@*|node()" priority="-2" mode="M39">
      <xsl:apply-templates select="*" mode="M39"/>
   </xsl:template>

   <!--PATTERN PersonFirstandLastName-->


	<!--RULE -->
<xsl:template match="//nc:PersonName[not(nc:PersonFullName)]" priority="1000" mode="M40">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="(not(./nc:PersonGivenName/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonGivenName)) &gt; 0)                 or (not(./nc:PersonSurName/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonSurName)) &gt; 0)"/>
         <xsl:otherwise>
            <xsl:message>At least one of first name, and last name must be present. ((not(./nc:PersonGivenName/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonGivenName)) &gt; 0) or (not(./nc:PersonSurName/@xsi:nil = 'true') and string-length(normalize-space(./nc:PersonSurName)) &gt; 0))</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M40"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M40"/>
   <xsl:template match="@*|node()" priority="-2" mode="M40">
      <xsl:apply-templates select="*" mode="M40"/>
   </xsl:template>

   <!--PATTERN ProbationViolationProbationOfficer-->


	<!--RULE -->
<xsl:template match="//filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode = 'Petition']"
                 priority="1000"
                 mode="M41">
      <xsl:variable name="ProbationOfficer"
                    select="../../nc:Case/j:CaseAugmentation/j:CaseOtherEntity/nc:EntityPerson/ecf:PersonAugmentation/ecf:FilingPartyID/nc:IdentificationID"/>
      <xsl:variable name="FilingAttorneyID"
                    select="//filing:FilingMessage/filing:FilingLeadDocument/ecf:DocumentAugmentation/ecf:FilingAttorneyID/nc:IdentificationID"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="$ProbationOfficer = $FilingAttorneyID"/>
         <xsl:otherwise>
            <xsl:message>The ProbationOfficerID '<xsl:text/>
               <xsl:value-of select="$ProbationOfficer"/>
               <xsl:text/>' must match the FilingAttorneyID '<xsl:text/>
               <xsl:value-of select="$FilingAttorneyID"/>
               <xsl:text/>' in the Document metadata in a Probation Violation Petition. ($ProbationOfficer = $FilingAttorneyID)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M41"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M41"/>
   <xsl:template match="@*|node()" priority="-2" mode="M41">
      <xsl:apply-templates select="*" mode="M41"/>
   </xsl:template>

   <!--PATTERN CaseOtherEntity-->


	<!--RULE -->
<xsl:template match="//j:CaseOtherEntity" priority="1000" mode="M42">

		<!--REPORT -->
<xsl:if test="../../../filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode != 'Petition']">
         <xsl:message>Only probation violation petition filings can contain the CaseOtherEntity element for the Probation Officer. (../../../filing:FilingLeadDocument/ecf:DocumentAugmentation[ecf:RegisterActionDescriptionCode != 'Petition'])</xsl:message>
      </xsl:if>
      <xsl:apply-templates select="*" mode="M42"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M42"/>
   <xsl:template match="@*|node()" priority="-2" mode="M42">
      <xsl:apply-templates select="*" mode="M42"/>
   </xsl:template>

   <!--PATTERN ResponseDocument-->


	<!--RULE -->
<xsl:template match="//ecf:ReviewedLeadDocument | ecf:ReviewedConnectedDocument"
                 priority="1001"
                 mode="M43">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="./nc:DocumentFileControlID and string-length(normalize-space(./nc:DocumentFileControlID)) &gt; 0 and not(./nc:DocumentFileControlID/@xsi:nill='true')"/>
         <xsl:otherwise>
            <xsl:message>Response document must always return court identifier for the filing. (./nc:DocumentFileControlID and string-length(normalize-space(./nc:DocumentFileControlID)) &gt; 0 and not(./nc:DocumentFileControlID/@xsi:nill='true'))</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="./ga:FilingDocumentAugmentation/ga:DocumentStampedDate/nc:Date"/>
         <xsl:otherwise>
            <xsl:message>Response document must always return with a timestamp of the actual filed dote for the document. (./ga:FilingDocumentAugmentation/ga:DocumentStampedDate/nc:Date)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="./ecf:DocumentAugmentation/ecf:DocumentRendition/nc:Attachment/nc:Base64BinaryObject"/>
         <xsl:otherwise>
            <xsl:message>Response document must always return with the stamped document. (./ecf:DocumentAugmentation/ecf:DocumentRendition/nc:Attachment/nc:Base64BinaryObject)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M43"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="/reviewfilingcallback:NotifyFilingReviewCompleteMessage/nc:Case/j:CaseAugmentation//*[contains(name(), 'Case') and ends-with(name(), 'Party')]//nc:EntityPerson"
                 priority="1000"
                 mode="M43">

		<!--ASSERT -->
<xsl:choose>
         <xsl:when test="./ecf:PersonAugmentation/ecf:FilingPartyID and string-length(normalize-space(./ecf:PersonAugmentation/ecf:FilingPartyID)) &gt; 0"/>
         <xsl:otherwise>
            <xsl:message>the response must return the court assigned identifier for the case. (./ecf:PersonAugmentation/ecf:FilingPartyID and string-length(normalize-space(./ecf:PersonAugmentation/ecf:FilingPartyID)) &gt; 0)</xsl:message>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M43"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M43"/>
   <xsl:template match="@*|node()" priority="-2" mode="M43">
      <xsl:apply-templates select="*" mode="M43"/>
   </xsl:template>
</xsl:stylesheet>