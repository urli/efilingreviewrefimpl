<?xml version="1.0" encoding="UTF-8"?>
<!-- Schematron business rules for E-Filing SSP
     Version 1.0 -->
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <title>Filing Review Request Tests</title>

    <ns prefix="hdr" uri="http://cje.georgia.gov/extension/header/1.0"/>
    <ns prefix="cbrn" uri="http://release.niem.gov/niem/domains/cbrn/3.2/"/>
    <ns prefix="ecf" uri="https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/ecf"/>
    <ns prefix="ga" uri="http://cje.georgia.gov/v.0/extension"/>
    <ns prefix="filing" uri="https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/filing"/>
    <ns prefix="x" uri="http://www.w3.org/TR/REC-html40"/>
    <ns prefix="nc" uri="http://release.niem.gov/niem/niem-core/3.0/"/>
    <ns prefix="j" uri="http://release.niem.gov/niem/domains/jxdm/5.2/"/>
    <ns prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
    <ns prefix="structures" uri="http://release.niem.gov/niem/structures/3.0/"/>
    <ns prefix="s" uri="http://release.niem.gov/niem/structures/3.0/"/>
    <ns prefix="criminal" uri="https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/criminal"/>
    <ns prefix="reviewfilingcallback" uri="https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/reviewfilingcallback"/> 
    <ns prefix="scr" uri="http://release.niem.gov/niem/domains/screening/3.2/"/>
    
    <include href="TempCodeLists.sch"/>
    
    <pattern id="ECF">
        <rule context="/filing:FilingMessage">
            <assert test="./nc:DocumentIdentification/nc:IdentificationID and string-length(normalize-space(./nc:DocumentIdentification/nc:IdentificationID)) > 0 and not(./nc:DocumentIdentification/@xsi:nil = 'true')">ECF 6.2.4 Message and Filing Identifiers: DocumentID must be present.</assert>
        </rule>
    </pattern>
    
</schema>