/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gov.georgia.cje.binding.helper;

import cje.efiling.ecf.DocumentAugmentationType;
import cje.efiling.ecf.DocumentRenditionType;
import cje.efiling.niem.core.ObjectFactory;
import com.urlintegration.exchange.xml.BaseXmlBindingHelper;
import com.urlintegration.exchange.xml.BindingException;
import com.urlintegration.exchange.xml.JAXBUtil;
import gov.georgia.cje.binding.NiemConverter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Java XML Binding class helper to simplify application code building EFiling service SOAP messages.
 * Created on 9/22/2017.
 */
public class ECFDocumentAugmentationHelper extends CommonECFBindingHelper {

    private static final Logger LOG = LoggerFactory.getLogger(DocumentRenditionHelper.class.getName());
    private static final cje.efiling.ecf.ObjectFactory ECF_FACTORY = new cje.efiling.ecf.ObjectFactory();
    private static final String SCHEMA_LOCATION = "schemas/ecf/ecf.xsd";
    private static final String NAMESPACE = "https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/ecf";
    private static final String ELEMENT_NAME = "DocumentAugmentation";
    public DocumentAugmentationType ecfDocAugmentation;

    /**
     * Base constructor
     */
    public ECFDocumentAugmentationHelper() {
        super(SCHEMA_LOCATION, NAMESPACE, ELEMENT_NAME);
        this.ecfDocAugmentation = new DocumentAugmentationType();
    }

    /**
     * Returns the {@link javax.xml.bind.JAXBElement} of {@link cje.efiling.ecf.DocumentAugmentationType}.
     *
     * @param obj the {@link cje.efiling.ecf.DocumentAugmentationType} that needs to be generated as a {@link javax.xml.bind.JAXBElement}
     * @return The {@link javax.xml.bind.JAXBElement} of {@link cje.efiling.ecf.DocumentAugmentationType}.
     */
    @Override
    public JAXBElement<?> toJAXBElement(Serializable obj) {
        DocumentAugmentationType r = (DocumentAugmentationType)obj;
        return ECF_FACTORY.createDocumentAugmentation(r);
    }

    /**
     * Private build method for use with the Builder pattern.
     * @param rendition The {@link cje.efiling.ecf.DocumentRenditionType} to be added to the augmentation.
     * @param filingAttorneyID The ID for the attorney to be added to the augmentation.
     * @param documentType The document type to be added to the augmentation
     * @param specialHandlingInstructions The special handling instructions to be added to the augmentation
     * @return {@link cje.efiling.ecf.DocumentAugmentationType}
     */
    private static DocumentAugmentationType buildECFDocumentAugmentation(DocumentRenditionType rendition,
                                                                         String filingAttorneyID,
                                                                         String documentType,
                                                                         String specialHandlingInstructions
                                                                         ) {
        DocumentAugmentationType aug = new DocumentAugmentationType();
        if (rendition == null) {
            throw new IllegalArgumentException(String.format("Parameter rendition of type {} cannot be null", DocumentRenditionType.class.getCanonicalName()));
        }
        if (StringUtils.isBlank(filingAttorneyID)) {
            throw new IllegalArgumentException("Parameter filingAttorneyID cannot be null or empty");
        }
        if (StringUtils.isBlank(documentType)) {
            throw new IllegalArgumentException("Parameter documentType cannot be null or empty");
        }
        if (rendition != null) {
            aug.setDocumentRendition(rendition);
        }
        if (StringUtils.isNotBlank(filingAttorneyID)) {
            aug.setFilingAttorneyID(new IdentificationHelper.IdentificationBuilder().id(filingAttorneyID).build());
        }
        if (StringUtils.isNotBlank(documentType)) {
            aug.setRegisterActionDescriptionCode(NiemConverter.toTextType(documentType));
        }
        if (StringUtils.isNotBlank(specialHandlingInstructions)) {
            aug.setSpecialHandlingInstructionsText(NiemConverter.toTextType(specialHandlingInstructions));
        }
        return aug;
    }

    /**
     * Inner class implementing the builder pattern.
     */
    public static class ECFDocumentationBuilder {
        private DocumentRenditionType rendition;
        private String filingAttorneyID;
        private String documentType;
        private String specialHandlingInstructions;

        /**
         * Builder constructor
         * @param rendition required parameter for building {@link cje.efiling.ecf.DocumentAugmentationType}
         * @param filingAttorneyID required filing attorney ID
         * @param documentType required document type
         */
        public ECFDocumentationBuilder(DocumentRenditionType rendition, String filingAttorneyID, String documentType) {
            this.rendition = rendition;
            this.filingAttorneyID = filingAttorneyID;
            this.documentType = documentType;
        }

        /**
         * Optional build method for adding special handling instructions to the augmentation.
         * @param specialHandlingInstructionsValue the special handling instructions
         * @return {@link gov.georgia.cje.binding.helper.ECFDocumentAugmentationHelper.ECFDocumentationBuilder}
         */
        public ECFDocumentationBuilder specialHandlingInstructions(String specialHandlingInstructionsValue) {
            this.specialHandlingInstructions = specialHandlingInstructionsValue;
            return this;
        }

        /**
         * Returns the {@link cje.efiling.ecf.DocumentAugmentationType} with the required and optional components.
         * @return {@link cje.efiling.ecf.DocumentAugmentationType}
         */
        public DocumentAugmentationType build() {
            return buildECFDocumentAugmentation(this.rendition, this.filingAttorneyID, this.documentType, this.specialHandlingInstructions);
        }
    }
}
