/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gov.georgia.cje.binding.helper;

import cje.efiling.filing.FilingMessageType;
import cje.efiling.niem.core.DocumentType;
import cje.efiling.niem.core.ObjectFactory;
import com.urlintegration.exchange.utilities.ProcessingResult;
import com.urlintegration.exchange.xml.BaseXmlBindingHelper;
import com.urlintegration.exchange.xml.BindingException;
import com.urlintegration.exchange.xml.JAXBUtil;
import com.urlintegration.exchange.xml.ReflectionNilStripper;
import gov.georgia.cje.util.FilingConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Java XML Binding class helper to simplify application code building EFiling service SOAP messages.
 * Created on 9/26/2017.
 */
public class FilingMsgHelper  extends CommonECFBindingHelper {

    private static final Logger LOG = LoggerFactory.getLogger(FilingMsgHelper.class.getName());
    private static final cje.efiling.filing.ObjectFactory FACTORY = new cje.efiling.filing.ObjectFactory();
    private static final String SCHEMA_LOCATION = "schemas/ecf/filing.xsd";
    private static final String NAMESPACE = "https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/filing";
    private static final String ELEMENT_NAME = "FilingMessage";
    public FilingMessageType message;

    /**
     * Base constructor
     */
    public FilingMsgHelper() {

        super(SCHEMA_LOCATION, NAMESPACE, ELEMENT_NAME);
        this.message = new FilingMessageType();
    }

    /**
     * Returns the {@link javax.xml.bind.JAXBElement} of {@link cje.efiling.niem.core.DocumentType}.
     *
     * @param obj the {@link cje.efiling.niem.core.DocumentType} that needs to be generated as a {@link javax.xml.bind.JAXBElement}
     * @return The {@link javax.xml.bind.JAXBElement} of {@link cje.efiling.niem.core.DocumentType}.
     */
    @Override
    public JAXBElement<?> toJAXBElement(Serializable obj) {
        DocumentType r = (DocumentType)obj;
        return FACTORY.createFilingLeadDocument(r);
    }

    /**
     * Returns an unmarshalled object from an input string using the initialized context.
     * @param is is The {@link java.io.InputStream} to be unmarshalled.
     * @return The unmarshalled object
     * @throws JAXBException when marshalling fails.
     * @throws BindingException {@link com.urlintegration.exchange.xml.BindingException} when marshalling or false nil stripping fails.
     */
    public static FilingMessageType toObject(final InputStream is) throws JAXBException, BindingException {
        return toObject(is, new FilingMessageType());
    }
}
