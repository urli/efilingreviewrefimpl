package gov.georgia.cje.util;

/**
 * Created by JMierwa on 3/22/2018.
 */
public class FilingConstants {
    public static final String LEAD_ATTACHMENT = "LeadAttachment";
    public static final String CONNECTED_ATTACHMENT = "ConnectedAttachment";
}
