/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gov.georgia.cje.binding.helper;

import cje.efiling.ecf.DocumentRenditionType;
import cje.efiling.niem.core.BinaryType;
import cje.efiling.niem.core.ObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.xml.bind.JAXBElement;
import java.io.Serializable;

/**Java XML Binding class helper to simplify application code building EFiling service SOAP messages.
 *
 * Created on 9/21/2017.
 */
public class DocumentRenditionHelper extends CommonECFBindingHelper {

    private static final Logger LOG = LoggerFactory.getLogger(DocumentRenditionHelper.class.getName());
    private static final cje.efiling.ecf.ObjectFactory ECF_FACTORY = new cje.efiling.ecf.ObjectFactory();
    private static final String SCHEMA_LOCATION = "schemas/ecf/ecf.xsd";
    private static final String NAMESPACE = "https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/ecf";
    private static final String ELEMENT_NAME = "ReviewFilingRequest";
    public DocumentRenditionType rendition;

    /**
     * Base constructor
     */
    public DocumentRenditionHelper() {
        super(SCHEMA_LOCATION, NAMESPACE, ELEMENT_NAME);
        this.rendition = new DocumentRenditionType();
    }

    /**
     * Returns the {@link javax.xml.bind.JAXBElement} of {@link cje.efiling.ecf.DocumentRenditionType}.
     *
     * @param obj the {@link cje.efiling.ecf.DocumentRenditionType} that needs to be generated as a {@link javax.xml.bind.JAXBElement}
     * @return The {@link javax.xml.bind.JAXBElement} of {@link cje.efiling.ecf.DocumentRenditionType}.
     */
    @Override
    public JAXBElement<?> toJAXBElement(Serializable obj) {
        DocumentRenditionType r = (DocumentRenditionType)obj;
        return ECF_FACTORY.createDocumentRendition(r);
    }

    /**
     * Returns an instance of {@link cje.efiling.ecf.DocumentRenditionType}
     * <p>
     *     This was not coded using a builder pattern when it should have been for consistency sake. It was left as is
     *     because of time constraints and also because not enough of the underlying type needed to be built.
     * </p>
     * @param binary The {@link cje.efiling.niem.core.BinaryType} to be added as an attachment to the rendition.
     * @return {@link cje.efiling.ecf.DocumentRenditionType}
     */
    public static DocumentRenditionType buildDocumentRendition(BinaryType binary) {
        DocumentRenditionType docRendition = new DocumentRenditionType();
        docRendition.setAttachment(binary);
        return docRendition;
    }

    /**
     * Returns the {@link cje.efiling.niem.core.BinaryType} from the rendition.
     * @param rendition The {@link cje.efiling.ecf.DocumentRenditionType} from which the attachment is wanted.
     * @return {@link cje.efiling.niem.core.BinaryType}
     */
    public static BinaryType getAttachment(DocumentRenditionType rendition) {
        return rendition.getAttachment();
    }
}
