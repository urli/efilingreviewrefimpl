package gov.georgia.cje.binding.helper;

import cje.efiling.ecf.DocumentRenditionType;
import cje.efiling.niem.core.DocumentType;

import javax.xml.bind.JAXBElement;

/**
 * Created by JMierwa on 3/19/2018.
 */
public abstract class DocumentTypeHelper extends CommonECFBindingHelper {

    /**
     * Base constructor
     * It is used for setting up the JAXBContext when marshaling.
     * @param schemaLocation The location of the XML schema containing the XML Element to which this helper is related.
     * @param namespace The {@link javax.xml.namespace.QName} namespace URI to which the XML element belongs.
     * @param elementName The XML element name to which the input binding object is related.
     */
    public DocumentTypeHelper(String schemaLocation, String namespace, String elementName) {
        super(schemaLocation, namespace, elementName);
    }

    /**
     * Returns the {@link gov.georgia.cje.binding.helper.BinaryHelper} from the document to be able to work with the attachment.
     * @param doc the lead {@link cje.efiling.niem.core.DocumentType}
     * @return {@link gov.georgia.cje.binding.helper.BinaryHelper}
     */
    public static BinaryHelper getBinaryHelper(DocumentType doc) {
        BinaryHelper bh = null;
        for(JAXBElement<?> element : doc.getDocumentAugmentationPoint()) {
            String ns = element.getName().getNamespaceURI();
            if ("https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/ecf".equals(element.getName().getNamespaceURI())) {
                DocumentRenditionType rendition = ((cje.efiling.ecf.DocumentAugmentationType)element.getValue()).getDocumentRendition();
                bh = new BinaryHelper(DocumentRenditionHelper.getAttachment(rendition));
            }
        }
        return bh;
    }
}
