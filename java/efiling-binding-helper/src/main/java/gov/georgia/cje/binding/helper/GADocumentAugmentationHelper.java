/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gov.georgia.cje.binding.helper;

import cje.efiling.niem.core.DocumentType;
import cje.efiling.niem.core.ObjectFactory;
import com.urlintegration.exchange.xml.BaseXmlBindingHelper;
import com.urlintegration.exchange.xml.BindingException;
import com.urlintegration.exchange.xml.JAXBUtil;
import gov.georgia.cje.binding.NiemConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;

/**
 * Java XML Binding class helper to simplify application code building EFiling service SOAP messages.
 * Created on 9/22/2017.
 */
public class GADocumentAugmentationHelper extends CommonECFBindingHelper {

    private static final Logger LOG = LoggerFactory.getLogger(GADocumentAugmentationHelper.class.getName());
    private static final ObjectFactory NC_FACTORY = new ObjectFactory();
    private static final cje.efiling.ga.extension.ObjectFactory GA_FACTORY = new cje.efiling.ga.extension.ObjectFactory();
    private static final String SCHEMA_LOCATION = "schemas/extensions/ga.xsd";
    private static final String NAMESPACE = "http://cje.georgia.gov/v.0/extension";
    private static final String ELEMENT_NAME = "DocumentAugmentation";
    private static final String BINDING_ERROR_MSG = "Failed to strip false NILs from Marshalled data.";
    public cje.efiling.ga.extension.FilingDocumentAugmentationType gaDocAugmentation;

    /**
     * Base constructor
     */
    public GADocumentAugmentationHelper() {
        super(SCHEMA_LOCATION, NAMESPACE, ELEMENT_NAME);
    }

    /**
     * Returns the {@link javax.xml.bind.JAXBElement} of {@link cje.efiling.ga.extension.FilingDocumentAugmentationType}.
     *
     * @param obj the {@link cje.efiling.ga.extension.FilingDocumentAugmentationType} that needs to be generated as a {@link javax.xml.bind.JAXBElement}
     * @return The {@link javax.xml.bind.JAXBElement} of {@link cje.efiling.ga.extension.FilingDocumentAugmentationType}.
     */
    @Override
    public JAXBElement<?> toJAXBElement(Serializable obj) {
        cje.efiling.ga.extension.FilingDocumentAugmentationType r = (cje.efiling.ga.extension.FilingDocumentAugmentationType)obj;
        return GA_FACTORY.createFilingDocumentAugmentation(r);
    }

    /**
     * Private constructor for implementing the builder pattern
     * @param amendedIndicator A {@link java.lang.Boolean} indicating whether the docuemnt is amended or not.
     * @return {@link cje.efiling.ga.extension.DocumentAugmentationType}
     */
    private static cje.efiling.ga.extension.FilingDocumentAugmentationType buildGADocumentAugmentation(boolean amendedIndicator) {
        cje.efiling.ga.extension.FilingDocumentAugmentationType aug = new cje.efiling.ga.extension.FilingDocumentAugmentationType();
        aug.setDocumentAmendedIndicator(NiemConverter.toNiemBool(amendedIndicator));
        return aug;
    }

    /**
     * Inner class implementing the builder pattern
     */
    public static class GADocumentAugmentationBuilder {
        private boolean amendedIndicator;

        /**
         * Constructor for the builder with required parameters. This augmentation applies to the filing message itself
         * and not the FilingLead or FilingConnected documents.
         * @param amendedIndicator True when the filing is being amended.
         */
        public GADocumentAugmentationBuilder(boolean amendedIndicator) {
            this.amendedIndicator = amendedIndicator;
        }

        /**
         * Returns the {@link cje.efiling.ga.extension.DocumentAugmentationType} with the required and optional components.
         * @return {@link cje.efiling.ga.extension.DocumentAugmentationType}
         */
        public cje.efiling.ga.extension.FilingDocumentAugmentationType build(){
            return buildGADocumentAugmentation(this.amendedIndicator);
        }
    }
}
