/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package gov.georgia.cje.binding.helper;

import cje.efiling.niem.core.BinaryType;
import cje.efiling.niem.core.ObjectFactory;
import com.urlintegration.exchange.utilities.ProcessingResult;
import com.urlintegration.exchange.xml.BaseXmlBindingHelper;
import com.urlintegration.exchange.xml.BindingException;
import com.urlintegration.exchange.xml.JAXBUtil;
import com.urlintegration.exchange.xml.ValidationErrorEnum;
import com.urlintegration.io.FileHelper;
import gov.georgia.cje.binding.NiemConverter;
import gov.georgia.cje.util.MTOMException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.cxf.attachment.DelegatingInputStream;
import org.apache.cxf.attachment.LazyDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.*;
import java.math.BigDecimal;
import java.util.Dictionary;

/**
 * Java XML Binding class helper to simplify application code building EFiling service SOAP messages.
 * Created on 9/21/2017.
 */
public class BinaryHelper extends CommonECFBindingHelper {

    private static final Logger LOG = LoggerFactory.getLogger(BinaryHelper.class.getName());
    private static final ObjectFactory NC_FACTORY = new ObjectFactory();
    private static final String SCHEMA_LOCATION = "schemas/ecf/niem/niem-core/3.0/niem-core.xsd";
    private static final String NAMESPACE = "http://release.niem.gov/niem/niem-core/3.0/";
    private static final String ELEMENT_NAME = "Attachment";
//    private static final String JAXB_CONTEXT_CLASSLIST
//            = "cje.efiling.niem.codes.aamva_d20:"
//            + "cje.efiling.niem.codes.cbrncl:"
//            + "cje.efiling.niem.codes.census_uscounty:"
//            + "cje.efiling.niem.codes.fbi_ncic:"
//            + "cje.efiling.niem.codes.fbi_ucr:"
//            + "cje.efiling.niem.codes.usps_states:"
//            + "cje.efiling.niem.core:"
//            + "cje.efiling.niem.domains.cbrn:"
//            + "cje.efiling.niem.domains.cyfs:"
//            + "cje.efiling.niem.domains.jxdm:"
//            + "cje.efiling.niem.domains.screening:"
//            + "cje.efiling.niem.proxy.xsd:"
//            + "cje.niem.structures";
    private static final String BINDING_ERROR_MSG = "Failed to strip false NILs from Marshalled data.";
    private BinaryType binary;

    /**
     * Base constructor
     */
    public BinaryHelper() {
        super(SCHEMA_LOCATION, NAMESPACE, ELEMENT_NAME);
        this.binary = new BinaryType();
    }

    /**
     * Constructor for a pre-existing instance of
     * {@link cje.efiling.niem.core.BinaryType}.
     *
     * @param binary The {@link cje.efiling.niem.core.BinaryType} that this
     * helper will interact with.
     */
    public BinaryHelper(BinaryType binary) {
        super(SCHEMA_LOCATION, NAMESPACE, ELEMENT_NAME);
        this.binary = binary;
    }

    /**
     * Returns the {@link javax.xml.bind.JAXBElement} of the element to which the helper pertains.
     * @param obj the {@link cje.efiling.niem.core.BinaryType} that needs to be generated as a {@link javax.xml.bind.JAXBElement}
     * @return The {@link javax.xml.bind.JAXBElement}
     */
    @Override
    public JAXBElement<?> toJAXBElement(Serializable obj) {
        return NC_FACTORY.createAttachment((BinaryType)obj);
    }


    public static InputStream getBinaryObject(BinaryType bt) throws MTOMException {
        InputStream is = null;
        try {
            LOG.debug("Data Handler is {}", ReflectionToStringBuilder.toString(bt.getBase64BinaryObject()));
            is = bt.getBase64BinaryObject().getInputStream();
            LOG.debug("Resulting Input Stream is {}", ReflectionToStringBuilder.toString(is));
        } catch (IOException e) {
            throw new MTOMException("Failed accessing binary data from message", e);
        }
        return is;
    }

    public  InputStream getBinaryObject() throws MTOMException {
        InputStream is = null;
        try {
            //binary.getBase64BinaryObject().
            LOG.debug("Data Handler is {}", ReflectionToStringBuilder.toString(this.binary.getBase64BinaryObject()));
            LOG.debug("Data Source is {}", ReflectionToStringBuilder.toString(this.binary.getBase64BinaryObject().getDataSource()));
            DataSource ds = this.binary.getBase64BinaryObject().getDataSource();
            if (ds instanceof LazyDataSource) {
                LazyDataSource lds = (LazyDataSource)ds;
                if (lds.getDataSource() != null) {
                    LOG.debug("Lazy Data Source Inner Data Source is {}", ReflectionToStringBuilder.toString(lds.getDataSource()));
                }
            }
            LOG.debug("Data Handler is {}", ReflectionToStringBuilder.toString(this.binary.getBase64BinaryObject().getDataSource()));

            is = this.binary.getBase64BinaryObject().getInputStream();
            LOG.debug("Resulting Input Stream is {}", ReflectionToStringBuilder.toString(is));
//            is.reset();
        } catch (IOException e) {
            throw new MTOMException("Failed accessing binary data from message", e);
        }
        return is;
    }

    public void setBinaryObject(DataSource binaryObjectSource) {
            // create a data handler to manage the MTOM for the binary attachment
            this.binary.setBase64BinaryObject(new DataHandler(binaryObjectSource));
    }

    public void setBinaryObjectFromFile(String fileName) {
        DataSource source = new FileDataSource(fileName);
        // create a data handler to manage the MTOM for the binary attachment
        this.binary.setBase64BinaryObject(new DataHandler(new FileDataSource(fileName)));
    }

    public ProcessingResult saveAttachmentToFile(String absoluteFilePath) {
        ProcessingResult result;
        InputStream is = null;
        try {
            is = this.getBinaryObject();
            if (is != null) {
                LOG.debug("Successfully retrieved Attachment. Now writing to {}", absoluteFilePath);
                //FileHelper.inputStreamToFile(is, absoluteFilePath);
                FileUtils.copyInputStreamToFile(is, new File(absoluteFilePath ));
                result = ProcessingResult.createSuccessResult();
            } else {
                LOG.error("Failed to get stream from datasource for MTOM attachment");
                result = ProcessingResult.createFailResult("Failed to get stream from datasource for MTOM attachment.", ValidationErrorEnum.MTOM_ERROR.toString());
            }
        } catch (MTOMException e) {
            result = ProcessingResult.createFailResult(e.getMessage(),"", ValidationErrorEnum.MTOM_ERROR.toString(), e);
        } catch (IOException e) {
            result = ProcessingResult.createFailResult(e.getMessage(), "", ValidationErrorEnum.UNSPECIFIED.toString(), e);
        } catch (IllegalStateException e) {
            result = ProcessingResult.createFailResult(String.format("Could not find Attachment. If the Attachment was physically sent, make sure the cid in the XML matches the part; %s", e.getMessage()),"", ValidationErrorEnum.UNSPECIFIED.toString(), e);
        } catch (Exception e) {
            result = ProcessingResult.createFailResult(e.getMessage(),"", ValidationErrorEnum.UNSPECIFIED.toString(), e);
        }
        return result;
    }

    public String getID() {
        return this.binary.getId();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("BinaryType{");
        if (this.binary != null) {
            sb.append("binaryID=");
            if (this.binary.getBinaryID() != null) {
                sb.append((StringUtils.isNotBlank(this.binary.getBinaryID().getValue())) ? this.binary.getBinaryID().getValue() : "NULL");
            }
            sb.append("binaryFormatText=");
            if (this.binary.getBinaryFormatText() != null) {
                sb.append((StringUtils.isNotBlank(this.binary.getBinaryFormatText().getValue())) ? this.binary.getBinaryFormatText().getValue() : "NULL");
            }
            sb.append("binaryURI=");
            if (this.binary.getBinaryURI() != null) {
                sb.append((StringUtils.isNotBlank(this.binary.getBinaryURI().getValue())) ? this.binary.getBinaryURI().getValue() : "NULL");
            }
            sb.append("binarySizeValue=");
            if (this.binary.getBinarySizeValue() != null) {
                sb.append(this.binary.getBinarySizeValue().getValue());
            }
            sb.append("id=");
            if (this.binary.getId() != null) {
                sb.append((StringUtils.isNotBlank(this.binary.getId())) ? this.binary.getId() : "NULL");
            }
        } else {
            sb.append("NULL");
        }
        sb.append("}");
        return sb.toString();
    }

    private static BinaryType buildBinary(String binaryID,
                                          DataSource binaryObjectSource,
                                          String binaryFormatText,
                                          String binaryURI,
                                          BigDecimal binarySizeValue) {

        BinaryType bt = new BinaryType();
        bt.setBinaryID(NiemConverter.toNiemString(binaryID));
        bt.setBinaryFormatText(NiemConverter.toTextType(binaryFormatText));
        bt.setBinaryURI(NiemConverter.toNiemAnyURI(binaryURI));
        bt.setBinarySizeValue(NiemConverter.toNiemNonNegativeInt(binarySizeValue));

        // create a data handler to manage the MTOM for the binary attachment
        bt.setBase64BinaryObject(new DataHandler(binaryObjectSource));

        return bt;
    }

    public static class BinaryBuilder {
        private String identifier;
        private DataSource binaryObject;
        private String binaryFormat;
        private String uri;
        private BigDecimal binarySize;

        public BinaryBuilder(final String binaryID,
                             final String binaryFormatText) {
            this.identifier = binaryID;
            this.binaryFormat = binaryFormatText;
        }

        public BinaryBuilder binaryURI(String binaryURI) {
            this.uri = binaryURI;
            return this;
        }

        public BinaryBuilder binarySize(BigDecimal binarySizeValue) {
            this.binarySize = binarySizeValue;
            return this;
        }

        public BinaryBuilder binaryObject(DataSource binaryObject) {
            this.binaryObject = binaryObject;
            return this;
        }

        public BinaryType build() {
            return buildBinary(identifier,
                     binaryObject,
                     binaryFormat,
                     uri,
                     binarySize);
        }
    }
}
