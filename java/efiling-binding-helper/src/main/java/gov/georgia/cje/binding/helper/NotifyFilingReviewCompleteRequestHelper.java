package gov.georgia.cje.binding.helper;

import cje.efiling.wrapper.NotifyFilingReviewCompleteRequestType;
import com.urlintegration.exchange.xml.BindingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.InputStream;
import java.io.Serializable;

/**
 * Created by JMierwa on 3/21/2018.
 */
public class NotifyFilingReviewCompleteRequestHelper extends CommonECFBindingHelper {

    private static final Logger LOG = LoggerFactory.getLogger(NotifyFilingReviewCompleteRequestHelper.class.getName());
    private static final cje.efiling.wrapper.ObjectFactory REQ_FACTORY = new cje.efiling.wrapper.ObjectFactory();
    private static final String SCHEMA_LOCATION = "schemas/ecf/MessageWrappers.xsd";
    private static final String NAMESPACE = "https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/MessageWrappers";
    public static final String ELEMENT_NAME = "NotifyFilingReviewCompleteRequest";

    /**
     * Base constructor
     */

    public NotifyFilingReviewCompleteRequestHelper() {
        super(SCHEMA_LOCATION, NAMESPACE, ELEMENT_NAME);
    }

    /**
     * Returns the {@link JAXBElement} of the element to which the helper pertains.
     *
     * @param obj the Java binding object for which a {@link JAXBElement} is required.
     * @return The {@link JAXBElement}
     */
    @Override
    public JAXBElement<?> toJAXBElement(Serializable obj) {
        NotifyFilingReviewCompleteRequestType r = (NotifyFilingReviewCompleteRequestType)obj;
        return REQ_FACTORY.createNotifyFilingReviewCompleteRequest(r);
    }

    /**
     * Returns an unmarshalled object from an input string using the initialized context.
     * @param is is The {@link java.io.InputStream} to be unmarshalled.
     * @return The unmarshalled object
     * @throws JAXBException when marshalling fails.
     * @throws BindingException {@link com.urlintegration.exchange.xml.BindingException} when marshalling or false nil stripping fails.
     */
    public static NotifyFilingReviewCompleteRequestType toObject(final InputStream is) throws JAXBException, BindingException {
        return toObject(is, new NotifyFilingReviewCompleteRequestType());
    }
}
