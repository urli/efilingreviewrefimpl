/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gov.georgia.cje.binding.helper;

import cje.efiling.niem.core.IdentificationType;
import cje.efiling.niem.core.JurisdictionType;
import cje.efiling.niem.core.ObjectFactory;
import gov.georgia.cje.binding.NiemConverter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.io.Serializable;

/**
 * Java XML Binding class helper to simplify application code building EFiling service SOAP messages.
 * Created on 9/22/2017.
 */
public class IdentificationHelper extends CommonECFBindingHelper {

    private static final Logger LOG = LoggerFactory.getLogger(IdentificationHelper.class.getName());
    private static final ObjectFactory NC_FACTORY = new ObjectFactory();
    private static final cje.efiling.ga.extension.ObjectFactory GA_FACTORY = new cje.efiling.ga.extension.ObjectFactory();
    private static final String SCHEMA_LOCATION = "schemas/ecf/niem/niem-core/3.0/niem-core.xsd";
    private static final String NAMESPACE = "http://release.niem.gov/niem/niem-core/3.0/";
    private static final String ELEMENT_NAME = "DocumentIdentification";
    private static final QName  IDENTIFICATION_QNAME = new QName("http://release.niem.gov/niem/niem-core/3.0/", "Identification");
    private static final QName  JurisdictionCountyCode_QNAME = new QName("http://release.niem.gov/niem/niem-core/3.0/", "JurisdictionText");
    private IdentificationType id;

    /**
     * Base constructor
     */
    public IdentificationHelper() {
        super(SCHEMA_LOCATION, NAMESPACE, ELEMENT_NAME);
        this.id = new IdentificationType();
    }

    /**
     * Constructor for a pre-existing instance of
     * {@link cje.efiling.niem.core.BinaryType}.
     *
     * @param id The {@link cje.efiling.niem.core.IdentificationType} that this
     * helper will interact with.
     */
    public IdentificationHelper(IdentificationType id) {
        super(SCHEMA_LOCATION, NAMESPACE, ELEMENT_NAME);
        this.id = id;
    }

    /**
     * Returns the {@link javax.xml.bind.JAXBElement} of the element to which the helper pertains.
     * @param obj the {@link cje.efiling.niem.core.IdentificationType} that needs to be generated as a {@link javax.xml.bind.JAXBElement}
     * @return The {@link javax.xml.bind.JAXBElement} of {@link cje.efiling.niem.core.IdentificationType}
     */
    @Override
    public JAXBElement<?> toJAXBElement(Serializable obj) {
        return new JAXBElement<IdentificationType>(IDENTIFICATION_QNAME, IdentificationType.class, null, (IdentificationType) obj);
    }

    /**
     * Private constructor implementing the builder pattern.
     * @param idValue The ID number
     * @param countyJurisdiction The jurisdiction to which the ID is applicable.
     * @return {@link cje.efiling.niem.core.IdentificationType}
     */
    private static IdentificationType buildIdentification(String idValue, String countyJurisdiction) {
        IdentificationType id = new IdentificationType();

        if (StringUtils.isBlank(idValue) && StringUtils.isBlank(countyJurisdiction)) {
            throw new IllegalArgumentException("IdentificationType must be instantiated with ID, countyJurisdiction, or both");
        }
        if (StringUtils.isNotBlank(idValue)) {
            id.setIdentificationID(NiemConverter.toNiemString(idValue));
        }

        if (StringUtils.isNotBlank(countyJurisdiction)) {
            JurisdictionType jurisdiction = new JurisdictionType();
            cje.efiling.ga.extension.ObjectFactory gaof = new cje.efiling.ga.extension.ObjectFactory();
            jurisdiction.setJurisdictionAbstract(gaof.createJurisdictionCountyCode(NiemConverter.toTextType(countyJurisdiction)));
            id.setIdentificationJurisdiction(jurisdiction);
        }
        return id;
    }

    /**
     * Inner builder class implementing the builder pattern
     */
    public static class IdentificationBuilder {
        private String id;
        private String countyCode;

        /**
         * Constructor accepting the required parameter. There is specific business logic that applies to this class. An
         * instance must have either or both, the ID  and the county code.
         */
        public IdentificationBuilder() {
        }

        /**
         * Adds the id number to the identification.
         * @param id The jurisdiction to which the ID is applicable.
         * @return {@link gov.georgia.cje.binding.helper.IdentificationHelper.IdentificationBuilder}
         */
        public IdentificationBuilder id(String id) {
            this.id = id;
            return this;
        }

        /**
         * Adds the county code to the identification.
         * @param countyCode The jurisdiction to which the ID is applicable.
         * @return {@link gov.georgia.cje.binding.helper.IdentificationHelper.IdentificationBuilder}
         */
        public IdentificationBuilder countyCode(String countyCode) {
            this.countyCode = countyCode;
            return this;
        }

        /**
         * Returns the {@link cje.efiling.niem.core.IdentificationType} with the required and optional components.
         * @return {@link cje.efiling.niem.core.IdentificationType}
         */
        public IdentificationType build() {
            return buildIdentification(this.id, this.countyCode);
        }
    }
}
