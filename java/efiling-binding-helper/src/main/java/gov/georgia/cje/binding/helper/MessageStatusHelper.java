/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gov.georgia.cje.binding.helper;

import cje.efiling.niem.codes.cbrncl.MessageStatusCodeSimpleType;
import cje.efiling.niem.codes.cbrncl.MessageStatusCodeType;
import cje.efiling.niem.domains.cbrn.MessageContentErrorType;
import cje.efiling.niem.domains.cbrn.MessageErrorType;
import cje.efiling.niem.domains.cbrn.MessageStatusType;
import cje.efiling.niem.domains.cbrn.ObjectFactory;
import com.urlintegration.exchange.utilities.ProcessingResult;
import com.urlintegration.exchange.xml.ValidationError;
import com.urlintegration.exchange.xml.ValidationErrorEnum;
import gov.georgia.cje.binding.NiemConverter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Java XML Binding class helper to simplify application code building EFiling service SOAP messages.
 * Created on 9/8/2017.
 */
public class MessageStatusHelper extends CommonECFBindingHelper {

    private static final Logger LOG = LoggerFactory.getLogger(MessageStatusHelper.class.getName());
    private static final ObjectFactory FACTORY = new ObjectFactory();
    private static final String SCHEMA_LOCATION = "schemas/ecf/niem/domains/cbrn/3.2/cbrn.xsd";
    private static final String NAMESPACE = "http://release.niem.gov/niem/domains/cbrn/3.2/";
    private static final String ELEMENT_NAME = "MessageStatus";

    private MessageStatusType responseMessage;

    /**
     * Base constructor
     */
    public MessageStatusHelper() {
        super(SCHEMA_LOCATION, NAMESPACE, ELEMENT_NAME);
        this.responseMessage = new MessageStatusType();
    }

    /**
     * Constructor for a pre-existing instance of
     * {@link cje.efiling.niem.domains.cbrn.MessageStatusType}.
     *
     * @param responseMessage The {@link cje.efiling.niem.domains.cbrn.MessageStatusType} that this
     * helper will interact with.
     */
    public MessageStatusHelper(MessageStatusType responseMessage) {
        super(SCHEMA_LOCATION, NAMESPACE, ELEMENT_NAME);
        this.responseMessage = responseMessage;
    }

    /**
     * Returns the {@link javax.xml.bind.JAXBElement} of the element to which the helper pertains.
     * @param obj The object that needs to be rendered as a {@link javax.xml.bind.JAXBElement}.
     * @return The {@link javax.xml.bind.JAXBElement}
     */
    @Override
    public JAXBElement<?> toJAXBElement(Serializable obj) {
        return FACTORY.createMessageStatus((MessageStatusType)obj);
    }

    /**
     * Returns a success instance of {@link cje.efiling.niem.domains.cbrn.MessageStatusType}
     * <p>
     *     This was not coded using a builder pattern when it should have been for consistency sake. It was left as is
     *     because of time constraints.
     * </p>
     * @param messageID The message ID from the CJE header of the received message.
     * @return {@link cje.efiling.niem.domains.cbrn.MessageStatusType}
     */
    public static MessageStatusType createSuccessMessageStatusHelper(String messageID) {
        LOG.debug("Create response message for filing message ID {}", messageID);
        MessageStatusType ms = new MessageStatusType();
        MessageStatusCodeType status = new MessageStatusCodeType();
        status.setValue(MessageStatusCodeSimpleType.SUCCESS);
        ms.setMessageStatusCode(status);
        ms.setSystemEventDateTime(NiemConverter.toNiemDateTimeType(new Date()));
        return ms;
    }

    /**
     * Returns an error instance of {@link cje.efiling.niem.domains.cbrn.MessageStatusType}
     * <p>
     *     This was not coded using a builder pattern when it should have been for consistency sake. It was left as is
     *     because of time constraints.
     * </p>
     * @param messageID The message ID from the CJE header of the received message.
     * @param result The {@link com.urlintegration.exchange.utilities.ProcessingResult} that contains the related error
     *               information to be included in the response message.
     * @return {@link cje.efiling.niem.domains.cbrn.MessageStatusType}
     */
    public static MessageStatusType createErrorMessageStatusHelper(String messageID, ProcessingResult result) {
        LOG.debug("Creating error response message for filing message ID {}; result message is '{}'", messageID, result.getlongResponseMsg());
        MessageStatusType ms = new MessageStatusType();
        MessageStatusCodeType status = new MessageStatusCodeType();
        status.setValue(MessageStatusCodeSimpleType.DATA_ERROR);
        ms.setMessageStatusCode(status);
        ms.setSystemEventDateTime(NiemConverter.toNiemDateTimeType(new Date()));

        MessageErrorType met = new MessageErrorType();
        met.setErrorCodeDescriptionText(NiemConverter.toTextType(result.getlongResponseMsg()));
        met.setErrorCodeText(NiemConverter.toTextType(result.getApplicationErrorCode()));
        ms.setMessageHandlingError(met);
        return ms;
    }

    /**
     * Returns an error instance of {@link cje.efiling.niem.domains.cbrn.MessageStatusType}
     * <p>
     *     This was not coded using a builder pattern when it should have been for consistency sake. It was left as is
     *     because of time constraints.
     * </p>
     * @param messageID The message ID from the CJE header of the received message.
     * @param errors A {@link java.util.List} of {@link com.urlintegration.exchange.xml.ValidationError}
     * @return {@link cje.efiling.niem.domains.cbrn.MessageStatusType}
     */
    public static MessageStatusType createXmlErrorMessageStatusHelper(String messageID, List<ValidationError> errors) {
        LOG.debug("Creating error response message for filing message ID {}; result message is '{}'", messageID);
        MessageStatusType ms = new MessageStatusType();
        MessageStatusCodeType status = new MessageStatusCodeType();
        status.setValue(MessageStatusCodeSimpleType.DATA_ERROR);
        ms.setMessageStatusCode(status);
        ms.setSystemEventDateTime(NiemConverter.toNiemDateTimeType(new Date()));
        addContentErrors(ms, errors);
        return ms;
    }
    
    public static void addContentErrors (MessageStatusType ms, List<ValidationError> errors) {
        if (ms.getMessageContentError() == null && errors.size() > 0) {
            ms.getMessageContentError().addAll(new ArrayList<MessageContentErrorType>());
        }
        for (ValidationError error : errors) {
            MessageContentErrorType mcet = new MessageContentErrorType();
            MessageErrorType met = new MessageErrorType();
            met.setErrorCodeDescriptionText(NiemConverter.toTextType(error.getError()));
            met.setErrorCodeText(NiemConverter.toTextType(error.getErrorType().ecfErrorCode()));
            mcet.setErrorDescription(met);
            ms.getMessageContentError().add(mcet);
        }
    }
    
    public void addHandlingError(String errorMessage, String errorCode) {
        MessageErrorType met = new MessageErrorType();
        met.setErrorCodeDescriptionText(NiemConverter.toTextType(errorMessage));
        met.setErrorCodeText(NiemConverter.toTextType(errorCode));
        this.responseMessage.setMessageHandlingError(met);
    }

    public void buildMessageError(String errorMessage, String errorCode) {
        MessageErrorType met = new MessageErrorType();
        met.setErrorCodeDescriptionText(NiemConverter.toTextType(errorMessage));
        met.setErrorCodeText(NiemConverter.toTextType(errorCode));
        this.responseMessage.setMessageHandlingError(met);
    }

    public static MessageErrorType createMessageError(String errorMessage, String errorCode) {
        MessageErrorType met = new MessageErrorType();
        met.setErrorCodeDescriptionText(NiemConverter.toTextType(errorMessage));
        met.setErrorCodeText(NiemConverter.toTextType(errorCode));
        return met;
    }

    public static MessageStatusType createMessageStatus(String messageID, ProcessingResult result) {
        MessageStatusType ms;
        if (result.successful()) {
            return createSuccessMessageStatusHelper(messageID);
        } else {
            ms = new MessageStatusType();
            ms.setSystemEventDateTime(NiemConverter.toNiemDateTimeType(new Date()));
            MessageStatusCodeType status = new MessageStatusCodeType();

            if (StringUtils.isNotBlank(result.getApplicationErrorCode())) {
                try {
                    ValidationErrorEnum resultCode = ValidationErrorEnum.valueOf(result.getApplicationErrorCode());
                    switch (resultCode) {
                        case UNSPECIFIED:
                            status.setValue(MessageStatusCodeSimpleType.SYSTEM_ERROR);
                            String rc = resultCode.ecfErrorCode();
                            String err = result.buildErrorMessage();
                            ms.setMessageHandlingError(createMessageError(err, rc));
                            break;
                        case MTOM_ERROR:
                            status.setValue(MessageStatusCodeSimpleType.MESSAGE_ERROR);
                            ms.setMessageHandlingError(createMessageError(result.buildErrorMessage(), resultCode.ecfErrorCode()));
                            break;
                        case SCHEMA:
                            status.setValue(MessageStatusCodeSimpleType.DATA_ERROR);
                            break;
                        case SCHEMATRON:
                            status.setValue(MessageStatusCodeSimpleType.DATA_ERROR);
                            break;
                    }
                } catch (IllegalArgumentException iaex) {
                    status.setValue(MessageStatusCodeSimpleType.SYSTEM_ERROR);
                    ms.setMessageHandlingError(createMessageError(result.buildErrorMessage(), ValidationErrorEnum.UNSPECIFIED.ecfErrorCode()));

                } catch (NullPointerException npe) {
                    status.setValue(MessageStatusCodeSimpleType.SYSTEM_ERROR);
                    ms.setMessageHandlingError(createMessageError(result.buildErrorMessage(), ValidationErrorEnum.UNSPECIFIED.ecfErrorCode()));
                }
            } else {
                // No Application code. Assign unexpected message status code
                status.setValue(MessageStatusCodeSimpleType.UNKNOWN_ERROR);
                ms.setMessageHandlingError(createMessageError(result.buildErrorMessage(), ValidationErrorEnum.UNSPECIFIED.ecfErrorCode()));
            }
            ms.setMessageStatusCode(status);
        }
        return ms;
    }
}