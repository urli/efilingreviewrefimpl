/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gov.georgia.cje.binding.helper;

import cje.efiling.filing.FilingMessageType;
import cje.efiling.niem.core.ObjectFactory;
import com.urlintegration.exchange.xml.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.lang.reflect.InvocationTargetException;

/**
 * Created on 10/18/2017.
 */
public abstract class CommonECFBindingHelper extends BaseXmlBindingHelper {

    private static final Logger LOG = LoggerFactory.getLogger(BinaryHelper.class.getName());
    private static final ObjectFactory NC_FACTORY = new ObjectFactory();
    protected final String namespace;
    protected final String elementName;
    protected static final String JAXB_CONTEXT_CLASSLIST
            = "cje.efiling.wrapper:"
            + "cje.efiling.criminal:"
            + "cje.efiling.ecf:"
            + "cje.efiling.filing:"
            + "cje.efiling.ga.extension:"
            + "cje.efiling.niem.domains.cbrn:"
            + "cje.efiling.niem.codes.cbrncl:"
            + "cje.efiling.niem.domains.cyfs:"
            + "cje.efiling.niem.codes.fbi_ncic:"
            + "cje.efiling.niem.codes.usps_states:"
            + "cje.efiling.niem.domains.jxdm:"
            + "cje.efiling.niem.domains.screening:"
            + "cje.efiling.niem.core:"
            + "cje.efiling.niem.proxy.xsd:"
            + "cje.niem.structures:"
            + "cje.common.header";
    private static final String BINDING_ERROR_MSG = "Failed to strip false NILs from Marshalled data.";

    /**
     * Base constructor
     * It is used for setting up the JAXBContext when marshaling.
     * @param schemaLocation The location of the XML schema containing the XML Element to which this helper is related.
     * @param namespace The {@link javax.xml.namespace.QName} namespace URI to which the XML element belongs.
     * @param elementName The XML element name to which the input binding object is related.
     */
    public CommonECFBindingHelper(String schemaLocation, String namespace, String elementName) {
        super(JAXB_CONTEXT_CLASSLIST, schemaLocation);
        this.namespace = namespace;
        this.elementName = elementName;
    }

    /**
     * Returns a {@link java.lang.String} of the XML fragment to which this XML Binding object is related.
     * @param message The java object that is the XML binding object. It is not type checked prior to attempting the marshalling.
     * @return{@link java.lang.String} of the XML fragment to which this XML Binding object is related
     * @throws JAXBException {@link javax.xml.bind.JAXBException} when a fundamental error occurs with the context, marshaller creation, or processing.
     * @throws BindingException {@link com.urlintegration.exchange.xml.BindingException} when the marshalling of the object encounters errors.
     * @throws UnsupportedEncodingException {@link java.io.UnsupportedEncodingException} when conversion to string fails with encoding type.
     */
    public String toXMLString(Object message) throws JAXBException, BindingException, UnsupportedEncodingException {
        return JAXBUtil.toString(this.namespace, this.elementName, JAXB_CONTEXT_CLASSLIST, message);
    }
    
    /** Returns a {@link java.io.InputStream} of the XML fragment to which this XML Binding object is related.
     * @param message The java object that is the XML binding object. It is not type checked prior to attempting the marshalling.
     * @return{@link java.io.InputStream} of the XML fragment to which this XML Binding object is related
     * @throws JAXBException {@link javax.xml.bind.JAXBException} when a fundamental error occurs with the context, marshaller creation, or processing.
     * @throws BindingException {@link com.urlintegration.exchange.xml.BindingException} when the marshalling of the object encounters errors.
     */
    public InputStream toInputStream(Object message) throws JAXBException, BindingException {
        return JAXBUtil.toInputStream(this.namespace, this.elementName, JAXB_CONTEXT_CLASSLIST, message);
    }

    /**
     * Validates an unmarshalled XML Binding object against the schema. The referenced schema must be on the class path.
     * @param message The java object that is the XML binding object. It is not type checked prior to attempting the marshalling.
     * @return the marshalled XML binding object as a {@link java.lang.String}.
     * @throws BindingException {@link com.urlintegration.exchange.xml.BindingException} when any unmarshalling or validation error occurs.
     */
    public String validate(Object message) throws BindingException {
        return this.validate(this.namespace, this.elementName, message);
    }

    /**
     * Returns an unmarshalled object from an input string using the initialized context.
     * @param is The {@link java.io.InputStream} to be unmarshalled.
     * @param classObj The seed class instance of the object to be returned.
     * <p>
     * This will typically be passed in as <code>new myClass()</code>.
     * </p>
     * @return The unmarshalled object.
     * @throws JAXBException when marshalling fails.
     * @throws BindingException {@link com.urlintegration.exchange.xml.BindingException} when marshalling or false nil stripping fails.
     */
    public static <T> T toObject(final InputStream is, final T classObj) throws JAXBException, BindingException {
        Object dm = JAXBUtil.toObject(JAXB_CONTEXT_CLASSLIST, classObj, is);
        T newDm;
        try {
            newDm = (T)ReflectionNilStripper.strip(dm);
        } catch (ClassNotFoundException e) {
            throw new BindingException(BINDING_ERROR_MSG, e);
        } catch (IllegalArgumentException e) {
            throw new BindingException(BINDING_ERROR_MSG, e);
        } catch (IllegalAccessException e) {
            throw new BindingException(BINDING_ERROR_MSG, e);
        } catch (InvocationTargetException e) {
            throw new BindingException(BINDING_ERROR_MSG, e);
        } catch (SecurityException e) {
            throw new BindingException(BINDING_ERROR_MSG, e);
        } catch (NoSuchMethodException e) {
            throw new BindingException(BINDING_ERROR_MSG, e);
        }
        return newDm;
    }
}
