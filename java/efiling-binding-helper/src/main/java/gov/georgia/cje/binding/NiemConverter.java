/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gov.georgia.cje.binding;

import cje.efiling.niem.core.DateType;
import cje.efiling.niem.core.NonNegativeDecimalType;
import cje.efiling.niem.core.ObjectFactory;
import cje.efiling.niem.core.PersonNameTextType;
import cje.efiling.niem.core.TextType;
import cje.efiling.niem.proxy.xsd.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * A simple converter to convert java based types into NIEM based types.
 * Created on 9/18/2017.
 */
public class NiemConverter {

    public static final Logger LOG = LoggerFactory.getLogger(NiemConverter.class.getName());


    private static final ObjectFactory NC_OF = new ObjectFactory();
    private static DatatypeFactory DTF;

    static { // A static block to initialize DatatypeFactory needed to catch exceptions from the factory method
        try {
            DTF = DatatypeFactory.newInstance();
        } catch (Exception e) {
            LOG.error("failed to initialize DatatypeFactory", e);
        }
    }

    /**
     * Private constructor to prevent instantiation since this is a static class.
     */
    private NiemConverter() {
    } // prevent instantiation

    /**
     * Returns an XMLGregorianCalendar object based on the provided input.
     *
     * @param date
     *            a Java date representation
     * @return XMLGregorianCalendar object based on the provided input or NULL if there was a problem during
     *         setup.
     */
    public static XMLGregorianCalendar toXMLGregCalDateTime(Date date) {
        if (date == null) // this will prevent null pointers
            return null;

        // create a regular GregorianCalendar
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date); // set the time

        // create an XMLGregorianCalendar from the GregorianCalendar for return
        return DTF.newXMLGregorianCalendar(cal);
    }

    /**
     * Returns a NIEM DateType based on the provided Java util Date.
     *
     * @param date Date value to be used for the NIEM date
     * @return NIEM DateType based on the provided value
     */
    public static DateType toDateType(Date date) {
        ObjectFactory oc = new ObjectFactory();
        DateType dt = new DateType();
        NiemDate nd = new NiemDate();
        nd.setValue(toXMLGregCalDateTime(date));
        dt.setDateRepresentation(oc.createDate(nd));
        return dt;
    }

    /**
     * Returns a NIEM DateType containing the date and time based on the provided
     * {@link java.util.Date}
     *
     * @param date
     *            Date value to be used for the NIEM date
     * @return NIEM DateType based on the provided value
     */
    public static DateType toDateTimeType(Date date) {
        DateType dt = new DateType();
        NiemDateTime nd = new NiemDateTime();
        nd.setValue(toXMLGregCalDateTime(date));
        dt.setDateRepresentation(NC_OF.createDateTime(nd));
        return dt;
    }
    /**
     * Returns a NIEM DateType containing the date and time based on the provided
     * {@link java.util.Date}
     *
     * @param date
     *            Date value to be used for the NIEM date
     * @return NIEM DateType based on the provided value
     */
    public static NiemDateTime toNiemDateTimeType(Date date) {
        NiemDateTime nd = new NiemDateTime();
        nd.setValue(toXMLGregCalDateTime(date));
        return nd;
    }


    /**
     * Returns a NIEM JAXB ActivityDateType based on the provided Java util Date.
     *
     * @param date
     *            Date value to be used for the NIEM date
     * @return NIEM DateType based on the provided value
     */
    public static JAXBElement<DateType> toActivityDateType(Date date) {
        if (date == null)
            return null;
        ObjectFactory oc = new ObjectFactory();
        DateType dt = new DateType();
        NiemDate nd = new NiemDate();
        nd.setValue(toXMLGregCalDateTime(date));
        dt.setDateRepresentation(oc.createDate(nd));
        JAXBElement<DateType> foo = oc.createActivityDate(dt);
        if (foo.isNil())
            foo.setNil(false);
        return foo;
    }

    /**
     * Returns a NIEM Core TextType based on the provided String.
     *
     * @param value String to convert to TextType
     * @return TextType object based on provided String
     */
    public static TextType toTextType(String value) {
        TextType tt = new TextType();
        tt.setValue(value);
        return tt;
    }

    /**
     * Returns a NIEM Core PersonNameTextType based on the provided String.
     *
     * @param value String to convert to TextType
     * @return PersonNameTextType object based on provided String
     */
    public static PersonNameTextType toPersonNameTT(String value) {
        PersonNameTextType pntt = new PersonNameTextType();
        pntt.setValue(value);
        return pntt;
    }

    /**
     * Returns a Proxy XSD NiemString based on the provided String.
     *
     * @param value String to convert to a NiemString
     * @return NiemString object based on provided String
     */
    public static NiemString toNiemString(String value) {
        NiemString ns = new NiemString();
        ns.setValue(value);
        return ns;
    }

    /**
     * Returns a NIEM non negative integer based on the provided value.
     *
     * @param value
     *            the integer string to be converted to the non negative integer
     * @return NiemBoolean based on the provided input
     */
    public static NonNegativeDecimalType toNiemNonNegativeInt(BigDecimal value) {
        NonNegativeDecimalType result = new NonNegativeDecimalType();
        result.setValue(value);
        return result;
    }

    /**
     * Returns a NIEM non negative integer based on the provided value.
     *
     * @param value
     *            the integer string to be converted to the non negative integer
     * @return NiemBoolean based on the provided input
     */
    public static NiemBoolean toNiemBool(boolean value) {
        NiemBoolean result = new NiemBoolean();
        result.setValue(value);
        return result;
    }

    /**
     * Returns a NIEM Duration based on the provided inputs.
     *
     * @param yrs
     *            Integer value for number of years
     * @param mos
     *            Integer value for number of months
     * @param days
     *            Integer value for number of days
     * @param hrs
     *            Integer value for number of hours.
     * @return NiemDuration based on the provided int inputs
     */
    public static NiemDuration toNiemDuration(int yrs, int mos, int days, int hrs) {
        NiemDuration niemDur = new NiemDuration();
        niemDur.setValue(DTF.newDuration(true, yrs, mos, days, hrs, 0, 0));
        return niemDur;
    }

    public static NiemAnyURI toNiemAnyURI(String uri) {
        NiemAnyURI niemURI = new NiemAnyURI();
        niemURI.setValue(uri);
        return niemURI;
    }
}
