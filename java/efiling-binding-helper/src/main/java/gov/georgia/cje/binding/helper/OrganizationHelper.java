/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gov.georgia.cje.binding.helper;

import cje.efiling.niem.core.ObjectFactory;
import cje.efiling.niem.core.OrganizationType;
import gov.georgia.cje.binding.NiemConverter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;
import java.io.Serializable;

/**
 * Java XML Binding class helper to simplify application code building EFiling service SOAP messages.
 * Created on 10/2/2017.
 */
public class OrganizationHelper extends CommonECFBindingHelper {

    private static final Logger LOG = LoggerFactory.getLogger(DocumentRenditionHelper.class.getName());
    private static final ObjectFactory NC_FACTORY = new ObjectFactory();
    private static final cje.efiling.ga.extension.ObjectFactory GA_FACTORY = new cje.efiling.ga.extension.ObjectFactory();
    private static final String SCHEMA_LOCATION = "schemas/ecf/niem/niem-core/3.0/niem-core.xsd";
    private static final String NAMESPACE = "http://release.niem.gov/niem/niem-core/3.0/";
    private static final String ELEMENT_NAME = "Organization";
    private OrganizationType org;

    /**
     * Base constructor
     */
    public OrganizationHelper() {

        super(SCHEMA_LOCATION, NAMESPACE, ELEMENT_NAME);
        this.org = new OrganizationType();
    }

    /**
     * Returns the {@link javax.xml.bind.JAXBElement} of {@link cje.efiling.niem.core.OrganizationType}.
     *
     * @param obj the {@link cje.efiling.niem.core.OrganizationType} that needs to be generated as a {@link javax.xml.bind.JAXBElement}
     * @return The {@link javax.xml.bind.JAXBElement} of {@link cje.efiling.niem.core.OrganizationType}.
     */
    @Override
    public JAXBElement<?> toJAXBElement(Serializable obj) {
        OrganizationType r = (OrganizationType)obj;
        return NC_FACTORY.createOrganization(r);
    }

    /**
     * Private build method for use with the Builder pattern.
     * @param name the name of the organization
     * @param category The type of organization
     * @param county the county to which the organization is a part of.
     * @param id the identification number of the organization.
     * @return {@link cje.efiling.niem.core.OrganizationType}
     */
    private static OrganizationType buildOrganization(String name, String category, String county, String id) {
        OrganizationType org = new OrganizationType();

        if (StringUtils.isNotBlank(name)) {
            org.setOrganizationName(NiemConverter.toTextType(name));
        }
        if (StringUtils.isNotBlank(category)) {
            org.setOrganizationCategory(GA_FACTORY.createOrganizationCategoryCode(NiemConverter.toTextType(category)));
        }
        if (StringUtils.isNotBlank(id) || StringUtils.isNotBlank(county)) {
            org.setOrganizationIdentification(new IdentificationHelper.IdentificationBuilder()
                    .id(id)
                    .countyCode(county)
                    .build());
        }
        return org;
    }

    /**
     * Inner class implementing the builder pattern
     */
    public static class OrganizationBuilder {
        private String name;
        private String category;
        private String county;
        private String id;

        /**
         * Base constructor
         */
        public OrganizationBuilder() {}

        /**
         * Sets the organization name
         * @param name the name of the organization
         * @return {@link gov.georgia.cje.binding.helper.OrganizationHelper.OrganizationBuilder}
         */
        public OrganizationBuilder name(String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the category of the organization
         * @param category The type of organization
         * @return {@link gov.georgia.cje.binding.helper.OrganizationHelper.OrganizationBuilder}
         */
        public OrganizationBuilder category(String category) {
            this.category = category;
            return this;
        }

        /**
         * Sets the county that the organization is in.
         * @param county the county to which the organization is a part of.
         * @return {@link gov.georgia.cje.binding.helper.OrganizationHelper.OrganizationBuilder}
         */
        public OrganizationBuilder county(String county) {
            this.county = county;
            return this;
        }

        /**
         * Sets an identifying number for an organization.
         * @param id the identification number of the organization.
         * @return {@link gov.georgia.cje.binding.helper.OrganizationHelper.OrganizationBuilder}
         */
        public OrganizationBuilder id(String id) {
            this.id = id;
            return this;
        }

        /**
         * Returns an instance of an Organization.
         * @return {@link cje.efiling.niem.core.OrganizationType}
         */
        public OrganizationType build() {
            return buildOrganization(this.name, this.category, this.id, this.county);
        }
    }
}
