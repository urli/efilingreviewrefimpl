/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gov.georgia.cje.binding.helper;

import cje.efiling.niem.core.DocumentType;
import cje.efiling.niem.core.IdentificationType;
import cje.efiling.niem.core.ObjectFactory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Java XML Binding class helper to simplify application code building EFiling service SOAP messages.
 * Created on 9/21/2017.
 */
public class FilingLeadDocumentHelper extends DocumentTypeHelper {

    private static final Logger LOG = LoggerFactory.getLogger(DocumentRenditionHelper.class.getName());
    private static final ObjectFactory NC_FACTORY = new ObjectFactory();
    private static final cje.efiling.filing.ObjectFactory FILING_FACTORY = new cje.efiling.filing.ObjectFactory();
    private static final cje.efiling.ecf.ObjectFactory ECF_FACTORY = new cje.efiling.ecf.ObjectFactory();
    private static final String SCHEMA_LOCATION = "genericode.xsd";
    private static final String NAMESPACE = "https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/ecf";
    private static final String ELEMENT_NAME = "FilingLeadDocument";
    private static final String BINDING_ERROR_MSG = "Failed to strip false NILs from Marshalled data.";
    public DocumentType leadDocument;

    /**
     * Base constructor
     */
    public FilingLeadDocumentHelper() {
        super(SCHEMA_LOCATION, NAMESPACE, ELEMENT_NAME);
    }

    /**
     * Returns the {@link javax.xml.bind.JAXBElement} of {@link cje.efiling.niem.core.DocumentType}.
     *
     * @param obj the {@link cje.efiling.niem.core.DocumentType} that needs to be generated as a {@link javax.xml.bind.JAXBElement}
     * @return The {@link javax.xml.bind.JAXBElement} of {@link cje.efiling.niem.core.DocumentType}.
     */
    @Override
    public JAXBElement<?> toJAXBElement(Serializable obj) {
        DocumentType r = (DocumentType)obj;
        return FILING_FACTORY.createFilingLeadDocument(r);
    }

    /**
     * Private build method for use with the Builder pattern.
     * @param filingCategoryCode the category for the filing document
     * @param documentID the ID for the document
     * @param gaAug the {@link cje.efiling.ga.extension.FilingDocumentAugmentationType} extension to the base niem document.
     * @param ecfAug the {@link cje.efiling.ecf.DocumentAugmentationType} extension to the base niem document.
     * @return {@link cje.efiling.niem.core.DocumentType}
     */
    private static DocumentType buildLeadDocument(String filingCategoryCode,
                                                  String documentID,
                                                  cje.efiling.ga.extension.FilingDocumentAugmentationType gaAug,
                                                  cje.efiling.ecf.DocumentAugmentationType ecfAug) {
        if (StringUtils.isBlank(filingCategoryCode)) {
            throw new IllegalArgumentException("Parameter filingCategoryCode cannot be null");
        }
        if (StringUtils.isBlank(documentID)) {
            throw new IllegalArgumentException("Parameter documentID cannot be null");
        }
        if (ecfAug == null) {
            throw new IllegalArgumentException(String.format("Parameter rendition of type {} cannot be null", cje.efiling.ga.extension.FilingDocumentAugmentationType.class.getCanonicalName()));
        }
        DocumentType doc = new DocumentType();

        List<IdentificationType> ids = new ArrayList<IdentificationType>();
        ids.add(new IdentificationHelper.IdentificationBuilder().id(documentID).build());

        List<JAXBElement<?>> augs = new ArrayList<JAXBElement<?>>();
        if (gaAug != null) {
            cje.efiling.ga.extension.ObjectFactory gof = new cje.efiling.ga.extension.ObjectFactory();
            augs.add(gof.createFilingDocumentAugmentation(gaAug));
        }
        augs.add(ECF_FACTORY.createDocumentAugmentation(ecfAug));
        doc.getDocumentAugmentationPoint().addAll(augs);
        return doc;
    }

    /**
     * Inner class implementing the builder pattern
     */
    public static class FilingLeadDocumentBuilder {
        private String filingCategoryCode;
        private String documentID;
        private cje.efiling.ecf.DocumentAugmentationType ecfAug;
        private cje.efiling.ga.extension.FilingDocumentAugmentationType gaAug;

        /**
         * Base constructor for required components.
         * @param filingCategoryCode the filing category code of the document.
         * @param documentID the ID of the document.
         * @param ecfAug the {@link cje.efiling.ecf.DocumentAugmentationType} extension to the base niem document.
         */
        public FilingLeadDocumentBuilder(String filingCategoryCode,
                                         String documentID,
                                         cje.efiling.ecf.DocumentAugmentationType ecfAug) {
            this.documentID = documentID;
            this.filingCategoryCode = filingCategoryCode;
            this.ecfAug = ecfAug;
        }

        /**
         * Optional add of the
         * @param gaAug {@link cje.efiling.ga.extension.FilingDocumentAugmentationType} extension to the base niem document.
         * @return {@link gov.georgia.cje.binding.helper.FilingLeadDocumentHelper.FilingLeadDocumentBuilder}
         */
        public FilingLeadDocumentBuilder gaDocumentationAugmentation(cje.efiling.ga.extension.FilingDocumentAugmentationType gaAug) {
            this.gaAug = gaAug;
            return this;
        }

        /**
         * Returns the {@link cje.efiling.niem.core.DocumentType} with the required and optional components.
         * @return {@link cje.efiling.niem.core.DocumentType}
         */
        public DocumentType build() {
            return buildLeadDocument(this.filingCategoryCode, this.documentID, this.gaAug, this.ecfAug);
        }
    }
}
