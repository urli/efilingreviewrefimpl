/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gov.georgia.cje.binding.helper;

import cje.efiling.niem.core.EntityType;
import cje.efiling.niem.core.ObjectFactory;
import cje.efiling.niem.core.OrganizationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;
import java.io.Serializable;

/**Java XML Binding class helper to simplify application code building EFiling service SOAP messages.
 * Created on 10/2/2017.
 */
public class EntityHelper extends CommonECFBindingHelper {

    private static final Logger LOG = LoggerFactory.getLogger(DocumentRenditionHelper.class.getName());
    private static final ObjectFactory NC_FACTORY = new ObjectFactory();
    private static final String SCHEMA_LOCATION = "schemas/ecf/niem/niem-core/3.0/niem-core.xsd";
    private static final String NAMESPACE = "http://release.niem.gov/niem/niem-core/3.0/";
    private static final String ELEMENT_NAME = "Organization";
    private EntityType entity;

    /**
     * Base constructor
     */
    public EntityHelper() {
        super(SCHEMA_LOCATION, NAMESPACE, ELEMENT_NAME);
        this.entity = new EntityType();
    }

    /**
     * Returns the {@link javax.xml.bind.JAXBElement} of {@link cje.efiling.niem.core.EntityType}.
     *
     * @param obj the {@link cje.efiling.niem.core.EntityType} that needs to be generated as a {@link javax.xml.bind.JAXBElement}
     * @return The {@link javax.xml.bind.JAXBElement} of {@link cje.efiling.niem.core.EntityType}.
     */
    @Override
    public JAXBElement<?> toJAXBElement(Serializable obj) {
        OrganizationType r = (OrganizationType)obj;
        return NC_FACTORY.createOrganization(r);
    }

    /**
     * Returns an instance of {@link cje.efiling.niem.core.EntityType}
     * <p>
     *     This was not coded using a builder pattern when it should have been for consistency sake. It was left as is
     *     because of time constraints and also because not enough of the underlying type needed to be built for this example.
     * </p>
     * @param org The {@link cje.efiling.niem.core.OrganizationType} to be added as an entity.
     * @return {@link cje.efiling.niem.core.EntityType}
     */
    public static EntityType buildEntityOrganization(OrganizationType org) {
        LOG.debug("Adding organization to Entity");
        EntityType entity = new EntityType();
        entity.setEntityRepresentation(NC_FACTORY.createEntityOrganization(org));
        return entity;
    }
}
