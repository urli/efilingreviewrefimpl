/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gov.georgia.cje.binding.helper;

import cje.common.header.CJEExchangeHeaderType;
import cje.common.header.ObjectFactory;
import cje.efiling.niem.core.DocumentType;
import cje.efiling.niem.core.EntityType;
import com.urlintegration.exchange.xml.BindingException;
import gov.georgia.cje.binding.NiemConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;

/**
 * Java XML Binding class helper to simplify application code building EFiling service SOAP messages.
 * Created on 10/2/2017.
 */
public class CJEHeaderHelper  extends CommonECFBindingHelper {

    private static final Logger LOG = LoggerFactory.getLogger(CJEHeaderHelper.class.getName());
    private static final ObjectFactory FACTORY = new ObjectFactory();
    private static final cje.efiling.niem.core.ObjectFactory NC_FACTORY = new cje.efiling.niem.core.ObjectFactory();
    private static final String SCHEMA_LOCATION = "schemas/extensions/CJEExchangeHeader.xsd";
    private static final String NAMESPACE = "http://cje.georgia.gov/extension/header/1.0";
    public static final String ELEMENT_NAME = "CJEExchangeHeader";
    private static final String BINDING_ERROR_MSG = "Failed to strip false NILs from Marshalled data.";
    public CJEExchangeHeaderType header;

    /**
     * Base constructor
     */
    public CJEHeaderHelper(CJEExchangeHeaderType header) {
        super(SCHEMA_LOCATION, NAMESPACE, ELEMENT_NAME);
        this.header = header;
    }

    /**
     * Returns the {@link javax.xml.bind.JAXBElement} of {@link cje.efiling.wrapper.ReviewFilingRequestType}.
     *
     * @param obj
     * @return The {@link javax.xml.bind.JAXBElement} of {@link cje.efiling.wrapper.ReviewFilingRequestType}.
     */
    @Override
    public JAXBElement<?> toJAXBElement(Serializable obj) {
        CJEExchangeHeaderType header = (CJEExchangeHeaderType)obj;
        return FACTORY.createCJEExchangeHeader(header);
    }

    /**
     * Returns an unmarshalled object from an input string using the initialized context.
     * @param is is The {@link java.io.InputStream} to be unmarshalled.
     * @return The unmarshalled object
     * @throws JAXBException when marshalling fails.
     * @throws BindingException {@link com.urlintegration.exchange.xml.BindingException} when marshalling or false nil stripping fails.
     */
    public static CJEExchangeHeaderType toObject(final InputStream is) throws JAXBException, BindingException {
        return toObject(is, new CJEExchangeHeaderType());
    }

    /**
     * Returns a success instance of {@link cje.common.header.CJEExchangeHeaderType}
     * <p>
     *     This was not coded using a builder pattern when it should have been for consistency sake. It was left as is
     *     because of time constraints.
     * </p>
     * @param messageID The message ID from the CJE header of the received message.
     * @return {@link cje.common.header.CJEExchangeHeaderType}
     */
    public static CJEExchangeHeaderType buildHeader(String transactionID, String messageID, Date sendDate, EntityType sender, EntityType receiver) {
        CJEExchangeHeaderType header = new CJEExchangeHeaderType();
        header.setTransactionID(NiemConverter.toNiemString(transactionID));
        header.setMessageID(NiemConverter.toNiemString(messageID));
        header.setMessageSentDate(NiemConverter.toDateTimeType(sendDate));
        header.setMessageSender(NC_FACTORY.createMessageSenderEntity(sender));
        header.setMessageSender(NC_FACTORY.createMessageRecipientEntity(receiver));
        return header;
    }
}
