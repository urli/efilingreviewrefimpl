/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.georgia.cje.binding.helper;

import cje.efiling.criminal.CaseAugmentationType;
import cje.efiling.niem.domains.jxdm.OrganizationAugmentationType;
import java.io.InputStream;
import javax.xml.bind.JAXBElement;
import org.junit.Test;
import static org.junit.Assert.*;
import cje.efiling.wrapper.ReviewFilingRequestType;
import com.urlintegration.exchange.xml.BindingException;
import javax.xml.bind.JAXBException;

/**
 *
 * @author JMierwa
 */
public class ReviewFilingRequestHelperTest {
    
    public ReviewFilingRequestHelperTest() {
    }

    /**
     * Test of toJAXBElement method, of class ReviewFilingRequestHelper.
     * @throws javax.xml.bind.JAXBException
     * @throws com.urlintegration.exchange.xml.BindingException
     */
    @Test
    public void testToJAXBElement() throws JAXBException, BindingException {
        System.out.println("toJAXBElement");
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("TestRFR-AccusationInitial.xml");
        ReviewFilingRequestType result = ReviewFilingRequestHelper.toObject(is);
        assertNotNull(result);
        assertNotNull(result.getFilingMessage());
        assertNotNull(result.getFilingMessage().getCase());
        for (JAXBElement<?> jElement : result.getFilingMessage().getCase().getCaseAugmentationPoint()) {
            if ("https://docs.oasis-open.org/legalxml-courtfiling/ns/v5.0/criminal".equals(jElement.getName().getNamespaceURI())) {
                CaseAugmentationType ca = (CaseAugmentationType) (jElement.getValue());
                for (JAXBElement<?> jElement1 : ca.getArrest().getArrestAgency().getOrganizationAugmentationPoint()) {
                    if ("http://release.niem.gov/niem/domains/jxdm/5.2/".equals(jElement1.getName().getNamespaceURI())) {
                        OrganizationAugmentationType oa = (OrganizationAugmentationType) (jElement1.getValue());
                        assertNotNull(oa.getOrganizationIdentification());
                    }
                }
            }
        }
    }
}
