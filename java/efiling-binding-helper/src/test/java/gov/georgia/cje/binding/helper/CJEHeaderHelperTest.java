package gov.georgia.cje.binding.helper;

import cje.common.header.CJEExchangeHeaderType;
import org.junit.Test;

import java.io.InputStream;

import static org.junit.Assert.*;

/**
 * Created by JMierwa on 3/23/2018.
 */
public class CJEHeaderHelperTest {
    @Test
    public void toObjectTest() throws Exception {
        System.out.println("toObject");
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("header.xml");
        CJEExchangeHeaderType result = CJEHeaderHelper.toObject(is);
        assertNotNull(result);
    }

}