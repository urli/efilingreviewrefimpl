<?xml version="1.0" encoding="UTF-8"?>
<xs:schema targetNamespace="http://release.niem.gov/niem/domains/cbrn/3.2/" version="1" xsi:schemaLocation="http://release.niem.gov/niem/appinfo/3.0/ ../../../appinfo/3.0/appinfo.xsd http://release.niem.gov/niem/conformanceTargets/3.0/ ../../../conformanceTargets/3.0/conformanceTargets.xsd" ct:conformanceTargets="http://reference.niem.gov/niem/specification/naming-and-design-rules/4.0/#ReferenceSchemaDocument" xmlns:niem-xs="http://release.niem.gov/niem/proxy/xsd/3.0/" xmlns:ct="http://release.niem.gov/niem/conformanceTargets/3.0/" xmlns:cbrncl="http://release.niem.gov/niem/codes/cbrncl/3.0/" xmlns:structures="http://release.niem.gov/niem/structures/3.0/" xmlns:appinfo="http://release.niem.gov/niem/appinfo/3.0/" xmlns:nc="http://release.niem.gov/niem/niem-core/3.0/" xmlns:cbrn="http://release.niem.gov/niem/domains/cbrn/3.2/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:annotation>
    <xs:documentation>Chemical, Biological, Radiological, and Nuclear Domain</xs:documentation>
  </xs:annotation>
  <xs:import schemaLocation="../../../codes/cbrncl/3.0/cbrncl.xsd" namespace="http://release.niem.gov/niem/codes/cbrncl/3.0/"/>
  <xs:import schemaLocation="../../../niem-core/3.0/niem-core.xsd" namespace="http://release.niem.gov/niem/niem-core/3.0/"/>
  <xs:import schemaLocation="../../../proxy/xsd/3.0/xs.xsd" namespace="http://release.niem.gov/niem/proxy/xsd/3.0/"/>
  <xs:import schemaLocation="../../../structures/3.0/structures.xsd" namespace="http://release.niem.gov/niem/structures/3.0/"/>
  <xs:complexType name="MessageContentErrorType">
    <xs:annotation>
      <xs:documentation>A data type that provides information about the point in the xml payload content of a message where an error occurred in processing the message.</xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="structures:ObjectType">
        <xs:sequence>
          <xs:element ref="cbrn:ErrorNodeName" minOccurs="1" maxOccurs="1"/>
          <xs:element ref="cbrn:ErrorDescription" minOccurs="1" maxOccurs="1"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="MessageErrorType">
    <xs:annotation>
      <xs:documentation>A data type that describes a message error.</xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="structures:ObjectType">
        <xs:sequence>
          <xs:element ref="cbrn:ErrorCodeText" minOccurs="1" maxOccurs="1"/>
          <xs:element ref="cbrn:ErrorCodeDescriptionText" minOccurs="0" maxOccurs="1"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="MessageStatusType">
    <xs:annotation>
      <xs:documentation>A data type to provide success or error feedback on a message that has been received.</xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="cbrn:SystemEventType">
        <xs:sequence>
          <xs:element ref="cbrn:MessageStatusCode" minOccurs="1" maxOccurs="1"/>
          <xs:element ref="cbrn:MessageContentError" minOccurs="0" maxOccurs="unbounded"/>
          <xs:element ref="cbrn:MessageHandlingError" minOccurs="0" maxOccurs="1"/>
          <xs:element ref="cbrn:MessageStatusAugmentationPoint" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="RemarksComplexObjectType">
    <xs:annotation>
      <xs:documentation>A data type providing a Remark via inheritance to applicable Types.</xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="structures:ObjectType"/>
    </xs:complexContent>
  </xs:complexType>
  <xs:complexType name="SystemEventType">
    <xs:annotation>
      <xs:documentation>A data type for a system event.</xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="cbrn:RemarksComplexObjectType">
        <xs:sequence>
          <xs:element ref="cbrn:SystemEventDateTime" minOccurs="1" maxOccurs="1"/>
        </xs:sequence>
        <xs:attribute ref="cbrn:systemSimulatedIndicator" use="required"/>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  <xs:attribute name="systemSimulatedIndicator" type="xs:boolean">
    <xs:annotation>
      <xs:documentation>True if the system is simulated; false otherwise.  If the attribute is not present, the value is false.</xs:documentation>
    </xs:annotation>
  </xs:attribute>
  <xs:element name="ErrorCodeDescriptionText" type="nc:TextType" >
    <xs:annotation>
      <xs:documentation>A description of an error code in free form text.</xs:documentation>
    </xs:annotation>
  </xs:element>
  <xs:element name="ErrorCodeText" type="nc:TextType" >
    <xs:annotation>
      <xs:documentation>An error code.</xs:documentation>
    </xs:annotation>
  </xs:element>
  <xs:element name="ErrorDescription" type="cbrn:MessageErrorType" >
    <xs:annotation>
      <xs:documentation>A text description of an error that occurred at a specific XML tag while processing an XML message.</xs:documentation>
    </xs:annotation>
  </xs:element>
  <xs:element name="ErrorNodeName" type="nc:TextType" nillable="true">
    <xs:annotation>
      <xs:documentation>A name of the XML tag at which an error occurred.</xs:documentation>
    </xs:annotation>
  </xs:element>
  <xs:element name="MessageContentError" type="cbrn:MessageContentErrorType" >
    <xs:annotation>
      <xs:documentation>A set of information about the point in the xml payload content of a message where an error occurred in processing the message.</xs:documentation>
    </xs:annotation>
  </xs:element>
  <xs:element name="MessageHandlingError" type="cbrn:MessageErrorType" >
    <xs:annotation>
      <xs:documentation>A description of a  message error encountered by an infrastructure component in the process of message handling and transmission.</xs:documentation>
    </xs:annotation>
  </xs:element>
  <xs:element name="MessageID" type="niem-xs:string" >
    <xs:annotation>
      <xs:documentation>An identifier associated with a message content.  There is no required format for the ID value.</xs:documentation>
    </xs:annotation>
  </xs:element>
  <xs:element name="MessageStatus" type="cbrn:MessageStatusType">
    <xs:annotation>
      <xs:documentation>A status of the message.</xs:documentation>
    </xs:annotation>
  </xs:element>
  <xs:element name="MessageStatusAugmentationPoint" abstract="true">
    <xs:annotation>
      <xs:documentation>An augmentation point for cbrn:MessageStatusType.</xs:documentation>
    </xs:annotation>
  </xs:element>
  <xs:element name="MessageStatusCode" type="cbrncl:MessageStatusCodeType" >
    <xs:annotation>
      <xs:documentation>A code for the receiving status of a message.</xs:documentation>
    </xs:annotation>
  </xs:element>
  <xs:element name="SystemEventDateTime" type="niem-xs:dateTime" >
    <xs:annotation>
      <xs:documentation>A date and time of a system event.</xs:documentation>
    </xs:annotation>
  </xs:element>
</xs:schema>
